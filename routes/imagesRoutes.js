import express from 'express';
import { getImage } from '../controllers/imagesController.js';

const imageRoutes= express.Router();

imageRoutes.get("/imageDownload/:imName", getImage);
export default imageRoutes;