//const express = require( 'express' );
//const {c}require 'userControllers.js';

import express from 'express'
import { verifyUserExistence, registerFunction, loginFunction, changePasswordFunction, secondStepRegistration, getUser,refresh,deconnexion,verify } from '../controllers/usersControllers.js'

// Path avec ES module
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename);

const router = express.Router();


router.post('/api/check-mail', verifyUserExistence);

router.post('/api/register', registerFunction);

router.post('/api/refreshtoken', refresh);

router.post('/api/login', loginFunction);

router.post('/api/change-password', changePasswordFunction);

router.post('/api/register/second-step', secondStepRegistration);

router.get('/api/user/:id', getUser);

router.post('/api/deconnect', verify,deconnexion);
router.post('/api/verification', verify);
/**
 * A décommenter lors du build (et du déploiement)
 */
// router.get('/*', (_, res) => {
//     res.sendFile(path.join(__dirname, '../dacmi-react/build/index.html'))
// });
//module.exports = router;

export default router