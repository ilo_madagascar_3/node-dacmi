import express from 'express'
import {
 addConversation,
 getConversation
} from '../controllers/conversationController.js'
const conversationRoutes = express.Router()

conversationRoutes.post('/ajout_conversation', addConversation);
conversationRoutes.get('/affich_conversation/:userId', getConversation);

export default conversationRoutes