import express from 'express'
import {
    getStripeid,
    postStripe
 
} from '../controllers/stripeController.js'
const stripeRoutes = express.Router()

stripeRoutes.post('/payment', postStripe);
stripeRoutes.get('/', getStripeid);

export default stripeRoutes