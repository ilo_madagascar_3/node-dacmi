import express from 'express'
import {
 addAchatDacmi,
 getAchatDacmi,
 getADacmi_id,
 updateDacmi,
 getAllUser,
 updateUsers,
 getUser_id
} from '../controllers/achatDacmiController.js';
import {verify} from '../controllers/usersControllers.js';

const achatDacmiRoutes = express.Router()

achatDacmiRoutes.post('/achatDacmi', addAchatDacmi);
achatDacmiRoutes.get('/afficheDacmi', getAchatDacmi);
achatDacmiRoutes.get('/affiche/:id', getADacmi_id);
achatDacmiRoutes.patch('/modiffier/:id', updateDacmi);
achatDacmiRoutes.get('/afficheAlluser', getAllUser);
achatDacmiRoutes.patch('/modiffieruser/:id',verify, updateUsers);
achatDacmiRoutes.get('/afficheuser/:id', getUser_id);

// achatDacmiRoutes.get('/affichetest', getTest);

export default achatDacmiRoutes