import express from 'express'
import {
 addMessage,
 getMessage,
 updatestatuMP,
 getLimitMp,
 updatestatuMultiple
} from '../controllers/messageController.js'
const messageRoutes = express.Router()

messageRoutes.post('/ajout_message', addMessage);
messageRoutes.get('/affich_messagee/:conversationId', getMessage);
messageRoutes.get('/affich_limiteMP/:conversationId', getLimitMp);
messageRoutes.patch('/modiffier_MP', updatestatuMP);
messageRoutes.patch('/modiffier_Multiple', updatestatuMultiple);

export default messageRoutes