/* const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const routes = require('./routes/routes.js') */
// import  socketio  from 'socket.io'
import express from 'express';
// import  createServer  from 'http'
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import routes from './routes/routes.js';
import achatDacmiRoutes from './routes/achatDacmiRoutes.js';
import upload from 'express-fileupload';
import dotenv from 'dotenv';
import cors from 'cors';
import http from 'http';
import * as _io from "socket.io";

import format from './format.js';
import { findUser, userJoin, userLeaves, getRoomUsers } from './users.js';
import conversationRoutes from './routes/conversationRoutes.js';
import messageRoutes from './routes/messageRoutes.js';
import stripeRoutes from './routes/stripeRoutes.js';
import moment from 'moment';
// import { SocketAddress } from 'net';
// import imageRoutes from './routes/imagesRoutes.js';

dotenv.config();

mongoose.connect(process.env.MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex:true
});
const app = express();

//app.use('/', express.static(path.join(__dirname, 'static')));
app.use(upload());
app.use(express.json());

/**
 * A décommenter lors du build (et du déploiement)
 */
app.use(express.static('dacmi-react/build'));
app.use(express.static('dacmi-react/build/uploads'));
app.use(express.static('uploads'));

app.use(achatDacmiRoutes);
app.use(conversationRoutes);
app.use(messageRoutes);
app.use(stripeRoutes);
// app.use(imageRoutes);
app.get('/imageDownload/:imName', function(req, res, n){
    // console.log(decodeURIComponent(req.params.imName));
    res.download('uploads/' + decodeURIComponent(req.params.imName));
    // res.download('uploads/photo-by-face-generator_61925098be0cb9000fc20600.jpg');
});
app.use(routes);//
app.use(cors());


const server= http.createServer(app);
const io= new _io.Server(server);
io.on('connection', socket => {
    // console.log('new connection..');
    
    socket.on('joinRoom', ({username, room, id}) => {
        // console.log(username + ' has joined. room= ' + room);
        const user= userJoin(id, username, room, socket.id);

        io.to(user.room).emit('roomW', {msg: 'he joined', room: room});

        socket.join(user.room);
        // console.log('yes');
        socket.emit('welcome', 'you connected successfully');

        // socket.emit('message', 'hey hey');//to the user only

        // socket.broadcast.to(user.room).emit('message', user.username + " is there..");//to all but not the current user

        // socket.on('chatMessage', ({user_, body}) => {
        socket.on('chatMessage', (obj) => {
            // socket.to(user.room).broadcast.emit('chatMessage', format(user, body));
            // console.log(user_);
            socket.to(user.room).emit('chatMessage', format(obj.user, obj.body));
            // console.log(format(obj.user, obj.body));
            // socket.emit('chatMessage', format(user_, body));
        });

        //request to know if an user is connected ---attempt---
        socket.on('isOnline', (id => {//is is a string representing the user dacmi id
            console.log('request : ' + id);
            const user_= findUser(id);

            if(user_){
                io.to(user.room).emit(
                    'online',
                    id
                );
                console.log('response : ' + id + " is online");
                return;
            }
            console.log('response : ' + id + " is offline");
        }));

        // io.to(user.room).emit('roomUsers', {
        //     room: user.room,
        //     users: getRoomUsers(user.room)
        // });
    });


    //io.emit()//to everybody including current user

    // socket.on('chatMessage', (msg => {
    //     const user= findUser(socket.id);
        // io.to(user.room).emit('message', format(user.username, msg));
    // }));

    socket.on('disconnect', () => {
        const user= userLeaves(socket.id);
        if(user){
            const user_= findUser(user.id);
            if(!user_){
                socket.to(user.room).emit(
                    'offline',
                    user.id
                );
                console.log(user.id + ' is not one of us anymore');
            }
        }
        // if(user){
        //     io.to(user.room).emit('message', user.username + ' left');
        // }

        // io.to(user.room).emit('roomUsers', {
        //     room: user.room,
        //     users: getRoomUsers(user.room)
        // });
    });
});


server.listen(process.env.PORT || 4000, () => {
    console.log('Server up at 4000');
});