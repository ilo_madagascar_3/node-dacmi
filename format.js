import moment from "moment";

export default function format(user_, body){
    return{
        user: user_,
        body,
        date: moment().format('DD-MM-YYYY HH:mm')
    };
}