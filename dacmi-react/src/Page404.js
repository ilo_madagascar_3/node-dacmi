import React from 'react';

export default function Page404() {
    return (
        <>
        <div className="fourZeroFour">
            La page où vous tentez d'aller n'existe pas.
        </div>
        </>
    )
}
