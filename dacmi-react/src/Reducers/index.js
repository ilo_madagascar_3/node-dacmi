import { combineReducers } from "redux";
import modalsReducer from "./modalsReducer";
import signInReducer from "./signInReducer";
import MetamaskReducer from "./MetamaskReducer";
import logReducer from "./logReducer";
import loginModalReducer from './loginModalReducer';

const allReducers = combineReducers({modalsReducer, signInReducer, MetamaskReducer, logReducer, loginModalReducer});

export default allReducers;