const initialState={
    shown: false
};

export default function loginModalReducer(state= initialState, action){
    switch(action.type){
        case 'DISP':
            return({
                ...state,
                shown: true
            });
        case 'HIDE':
            return({
                ...state,
                shown: false
            });
        default:
            return(state);
    }
};