const initialState= {
    showSignIn: false,
    id: ""
}

export default function modalsReducer(state= initialState, action){

    switch(action.type){
        case "TOGGLEIN":
            if(state.showSignIn){
                return{
                    ...state,
                    showSignIn: false
                }
            } else {
                return{
                    ...state,
                    showSignIn: true,
                    id: action.id
                }
            }
        case "CLOSEMODAL":
            return{
                ...state,
                showSignIn: false
            }
        default:
            return(state);
    }
}