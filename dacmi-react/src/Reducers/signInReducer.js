const initialState= {
    isSignedIn: false
}

export default function signInReducer(state= initialState, action){

    switch(action.type){
        case "LOGIN":
            return{
                ...state,
                isSignedIn: true
            }
        case "LOGOUT":
            return{
                ...state,
                isSignedIn: false
            }
        default:
            return(state);
    }
}