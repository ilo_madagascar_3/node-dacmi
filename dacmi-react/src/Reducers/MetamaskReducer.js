const initialState={
    metamaskAccounts: [],
    hasMetamask: false
};

export default function MetamaskReducer(state= initialState, action){
    switch(action.type){
        case 'SETMETA':
            return {
                ...state,
                metamaskAccounts: action.metamaskAccounts,
                hasMetamask: true
            };
        default:
            return state;
    }
};