import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import pie from '../../media/pie-white.png';
import piecol from '../../media/pie-col.png';
import user from '../../media/username-white.png';
import usercol from '../../media/username-col.png';
import compass from '../../media/compass-white.png';
import compasscol from '../../media/compass-col.png';
import assist from '../../media/assist-white.png';
import assistcol from '../../media/assist-col.png';
import env from '../../media/envellope-w.svg';
import envG from '../../media/envellope-g.svg';

export default function Sidemenu(props) {

    const [special, setSpecial]= useState(false);
    function toggleSpecial(){
        setSpecial(!special);
        props.expandIt();
    }
    return (
        <div className={"side-menu" + " " + props.classes}>
            <div className="menu-commands">
                <div className="hamburgers" onClick={ toggleSpecial }>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                </div>
                <div className="crosses" onClick={ props.toggleMenu }>
                    <div className="cross"></div>
                    <div className="cross"></div>
                </div>
            </div>
            <ul className="menu-list">
                <li className={ props.active === 'dsh' ? "active" : "" }>
                    <Link to="/dashboard" className="menu-link">
                        <div className="superposed">
                            <img src={ piecol } className='thing1'/>
                            <img src={ pie } className='thing2'/>
                        </div>
                        <div className="text-link">
                            Dashboard
                        </div>
                    </Link>
                </li>
                <li className={ props.active === 'acc' ? "active" : "" }>
                    <Link to="/account" className="menu-link">
                        <div className="superposed">
                            <img src={ usercol } className='thing1'/>
                            <img src={ user }/>
                        </div>
                        <div className="text-link">
                            Account
                        </div>
                    </Link>
                </li>
                <li className={ props.active === 'gui' ? "active" : "" }>
                    <Link to="/guide" className="menu-link">
                        <div className="superposed">
                            <img src={ compasscol } className='thing1'/>
                            <img src={ compass }/>
                        </div>
                        <div className="text-link">
                            Our guide
                        </div>
                    </Link>
                </li>
                {/* <li className={ props.active === 'cal' ? "active" : "" }>
                    <a href="calendar" className="menu-link">
                        <div className="superposed">
                            <img src={ calendarcol } className='thing1'/>
                            <img src={ calendar }/>
                        </div>
                        <div className="text-link">
                            Calendar
                        </div>
                    </a>
                </li> */}
                <li className={ props.active === 'ass' ? "active" : "" }>
                    <a href="assist" className="menu-link">
                        <div className="superposed">
                            <img src={ assistcol } className='thing1'/>
                            <img src={ assist }/>
                        </div>
                        <div className="text-link">
                            Assistance
                        </div>
                    </a>
                </li>
                <li className={ props.active === 'msg' ? "active" : "" }>
                    <a href="/messages" className="menu-link">
                        <div className="superposed">
                            <img src={ envG } className='thing1'/>
                            <img src={ env }/>
                        </div>
                        <div className="text-link">
                            Messages
                        </div>
                    </a>
                </li>
                {/* <li className={ props.active === 'set' ? "active" : "" }>
                    <a href="settings" className="menu-link">
                        <div className="superposed">
                            <img src={ settingscol } className='thing1'/>
                            <img src={ settings }/>
                        </div>
                        <div className="text-link">
                            Settings
                        </div>
                    </a>
                </li> */}
            </ul>

            <div className="dacmi-logo">
                <img src={ process.env.PUBLIC_URL + 'coin-1.png' }/>
            </div>
        </div>
    )
}
