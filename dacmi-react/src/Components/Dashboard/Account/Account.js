import React, { useState, useRef, useEffect } from 'react';
import './Account.css';

import Header from '../../Header/Header';
import Stats from '../Stats';
import Sidemenu from '../Sidemenu';
// import { getAccounts } from '../../dmi';
import Bar from '../../Loading/Bar';

import Upload from '../../../media/upload-gold.png';
import Download from '../../../media/download.png';
import jpeg from '../../../media/jpeg-des.png';
import jpegcol from '../../../media/jpeg-col.png';
import pdf from '../../../media/pdf-des.png';
import pdfcol from '../../../media/pdf-col.png';
import save from '../../../media/save-wh.png';
import edit from '../../../media/edit-wh.png';
import copy from '../../../media/copy-w.svg';
import user from '../../../media/user.svg';

import { Link } from 'react-router-dom';

import axios from 'axios';
import FileDownload from 'js-file-download';
import CloseButton from '../../CloseButton/CloseButton';
import { useSelector, useDispatch } from 'react-redux';
import MetamaskHandler from '../../MetamaskHandler/MetamaskHandler';

import isJpegFile from '../../fileExtension';
import LoginModal from '../../LoginModal/LoginModal';

// import Loading from '../../Loading/Loading';

// import OnboardingButton from '../Onboarding';

export default function Account() {
    //some Metamask stuff..
    const metaRedux= useSelector(state=> state.MetamaskReducer);
    // useEffect(() => {
    //     console.log(metaRedux.metamaskAccounts);
    //     console.log(metaRedux);
    // }, [metaRedux]);
    //---END---
    let result_UserInfo=JSON.parse(localStorage.getItem('userInfo')).data;

    const [valeur, setValeur]= useState(result_UserInfo);
    // console.log("otran zao",valeur);
    const [metamask, setMetamask]= useState("");

    const [ultimate__initial__first__state__which__doesnt__change, setUltimate__state__init] = useState("");
    //yep, it doesn't change unless the one in the DB changes :D !

    const uploadLogo= useRef(null);
    const [saveEdit, setSaveEdit]= useState(false);//is on read mode when false
    // const [userparid, setUserparid]= useState([]);
    const [profile__current__state, setProfile__current__state] = useState("");

    //loading thing..
    const [loading, setLoading] = useState(true)
    //loading thing.. END---

    useEffect(() => {//getting the user info and updating metamask adress if there is none in the DB
        const affichUserid= async () =>{
           // let valeur=JSON.parse(localStorage.getItem('userInfo')).data;
            const userid = await window.fetch(`/afficheuser/${valeur._id}`);
            const resultuserid= await userid.json();

            if((resultuserid[0].adressMetamask!= "")&&(resultuserid[0].adressMetamask!="aucune")&&(resultuserid[0].adressMetamask!= undefined)){
                setMetamask(resultuserid[0].adressMetamask);
            } else {
                if(metaRedux.hasMetamask){
                    // console.log(resultuserid[0]);
                    setMetamask(metaRedux.metamaskAccounts[0]);
                    resultuserid[0].adressMetamask= metaRedux.metamaskAccounts[0];
                    const usInf= localStorage.getItem('userInfo');
                    const p_usInf= JSON.parse(usInf);
                    await window.fetch(`/modiffieruser/${resultuserid[0]._id}`,{
                        headers:{
                            "Accept":'application/json',
                            "Content-Type":'application/json',
                            "authorization": "Bearer " + p_usInf.data.acsessToken
                        },
                        method:'PATCH',
                        body: JSON.stringify(resultuserid[0])
                    });
                }
            }

            setUltimate__state__init(resultuserid[0]);//the original kept in a safe location, in case the user aborts the edition..
            setProfile__current__state(resultuserid[0]);
            setLoading(false);
        };
        affichUserid();
    },[]);

    // console.log("modif",profile__current__state);
    //modal for login
    // const [loginShown, showLogin] = useState(false);
    const loginShown= useSelector(state=> state.loginModalReducer);
    const dispatch= useDispatch();

    function switchto(){
        // console.log("faharoa",valeur.accessToken);
        if(saveEdit){
            (async ()=> {
                let item={
                    firstname:profile__current__state.firstname,
                    lastname:profile__current__state.lastname,
                    country:profile__current__state.country,
                    adressMetamask:profile__current__state.adressMetamask,
                    phone:profile__current__state.phone,
                    email:profile__current__state.email
                };
                const usInf= localStorage.getItem('userInfo');
                const p_usInf= JSON.parse(usInf);
                // console.log("alefa",item,profile__current__state._id);
                window.fetch(`/modiffieruser/${profile__current__state._id}`,{
                    headers:{
                        "Accept":'application/json',
                        "Content-Type":'application/json',
                        "Authorization": "Bearer " + p_usInf.data.acsessToken
                        }, 
                    method:'PATCH',
                    body: JSON.stringify(item)
                }).then(stuff=> stuff.json()).then(res=> {
                    console.log("yyy",res);
                    if(res.status===403){
                        //showModal..

                        // showLogin(true);

                        dispatch({
                            type: 'DISP',
                        });

                        //do login here..
                        //..
                        //..
                        //when login is done
                        //showLogin(false);
                        //redo the modify thing :
                        //I'll do it in useEffect.. :D
                    }
                    return res;
                });
                // .then(row=> {
                //     // console.log(row);
                //     if(row.status!== 403){
                //         window.fetch('/afficheuser/' + row._id)
                //         .then(thing=> thing.json())
                //         .then(res=> {
                //             let newInfo= {
                //                 status: "ok",
                //                 data: res[0]
                //             };
                //             localStorage.removeItem('userInfo');
                //             localStorage.setItem("userInfo", JSON.stringify(newInfo));
                //         });
                //     }
                // });
            })();
        }
        setSaveEdit(!saveEdit);
    }

    const have_changes_been_made= function(){
        if(profile__current__state!==""){
            if(profile__current__state.firstname!== ultimate__initial__first__state__which__doesnt__change.firstname){
                return true;
            }
            if(profile__current__state.lastname!== ultimate__initial__first__state__which__doesnt__change.lastname){
                return true;
            }
            if(profile__current__state.country!== ultimate__initial__first__state__which__doesnt__change.country){
                return true;
            }
            if(profile__current__state.adressMetamask!== ultimate__initial__first__state__which__doesnt__change.adressMetamask){
                return true;
            }
            if(profile__current__state.phone!== ultimate__initial__first__state__which__doesnt__change.phone){
                return true;
            }
            if(profile__current__state.email!== ultimate__initial__first__state__which__doesnt__change.email){
                return true;
            }
            //if file input not empty..
            //some code for that..
            //return true too!
        }
        return false;
    };

    useEffect(()=>{
        console.log("modal state changed.");
        if(loginShown.shown===false){
            // console.log("login is hidden");
            // console.log(profile__current__state);
            // if(JSON.stringify(profile__current__state)=== JSON.stringify(ultimate__initial__first__state__which__doesnt__change) && profile__current__state!== ""){//if the user did some modifs..
            if(have_changes_been_made()){//if the user did some modifs..
                //..it would still be present in profile__current__state
                // console.log("unsaved modifications found, saving..");
                // console.log(profile__current__state);
                // console.log(ultimate__initial__first__state__which__doesnt__change);

                let item={
                    firstname:profile__current__state.firstname,
                    lastname:profile__current__state.lastname,
                    country:profile__current__state.country,
                    adressMetamask:profile__current__state.adressMetamask,
                    phone:profile__current__state.phone,
                    email:profile__current__state.email
                };
                let value= JSON.parse(localStorage.getItem('userInfo')).data;
                window.fetch(`/modiffieruser/${profile__current__state._id}`,{
                    headers:{
                        "Accept":'application/json',
                        "Content-Type":'application/json',
                        "Authorization": "Bearer " + value.acsessToken
                        }, 
                    method:'PATCH',
                    body: JSON.stringify(item)
                })
                .then(row=> {//then update localStorage
                    console.log('trying to update locastorage..');
                    // console.log(row);
                    // if(row.status!== 403){
                    window.fetch('/afficheuser/' + profile__current__state._id)
                    .then(thing=> thing.json())
                    .then(res=> {
                        let newInfo= {
                            status: "ok",
                            data: {...res[0], acsessToken: value.acsessToken}
                        };
                        localStorage.removeItem('userInfo');
                        localStorage.setItem("userInfo", JSON.stringify(newInfo));
                        setValeur(newInfo.data);
                        console.log("local storage updated :");
                        console.log(JSON.parse(localStorage.getItem('userInfo')));
                    });
                    // }
                });
            }
        }
    },[loginShown]);

    // const {email, firstname, lastname} = JSON.parse(localStorage.getItem('userInfo')).data;

    function update__input(e){
        let new__state= {...profile__current__state};
        new__state[e.target.getAttribute('name')]= e.target.value;
        setProfile__current__state(new__state);
    }
    
    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };

    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    function abort(){
        //Abort the edition
        setSaveEdit(false);
        setProfile__current__state(ultimate__initial__first__state__which__doesnt__change);
    }
    
    const pop= useRef(null);
    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Fallback: Copying text command was ' + msg);
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
          console.log('Async: Copying to clipboard was successful!');
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }
    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }

    const jpegLink= useRef(null);
    const pdfLink= useRef(null);

    function jpegdown(){
        jpegLink.current.click();
    }
    function pdfdown(){
        pdfLink.current.click();
    }

    //image---

    const [imageMod, setImageMod] = useState(false);
    const showImage= ()=>{
        setImageMod(true);
    };
    const closeImage= ()=>{
        setImageMod(false);
    };
    const imageDown= function(event){
        // event.preventDefault();
        if(isJpegFile(profile__current__state.KYC)){
            axios.get('/imageDownload/' + encodeURIComponent(profile__current__state.KYC), {
                responseType: "blob"
            })
            .then(response=> {
                // console.log(response);
                // console.log("fuck you");
                FileDownload(response.data, "image.jpg");
                // console.log("downloaded :", encodeURIComponent(profile__current__state.KYC));
            });
        }
    };

    function imageDown2(){
        console.log("explain me!");
    }

    return (
        <>
            <div className="my-account">
                <Header title="account" accountType="user" user="John Smith"></Header>
                <div className={ expanded ? "content expanded" : "content" }>

                    <Sidemenu active='acc' expandIt={ expander } classes= { menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <Stats></Stats>
                    <div className="profile-form">
                        {
                            loading?
                            <>
                                <div className="fullrows">
                                    <div className="one_">
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                    </div>
                                    <div className="two_">
                                        <div className="two_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="two_one"><Bar></Bar><Bar></Bar></div>
                                    </div>
                                    <div className="thr_">
                                        <Bar></Bar>
                                        <Bar></Bar>
                                    </div>
                                    <div className="fou_">
                                        <Bar></Bar>
                                        <Bar></Bar>
                                    </div>
                                </div>
                            </>
                            :
                            <>
                                <div className="name-country">
                                    <div className="label__and__input">
                                        <label htmlFor="firstname">First name</label>
                                        { saveEdit ? <input type='text' className="" name="firstname" id="firstname"  value={ profile__current__state.firstname } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="firstname" id="firstname" value={ profile__current__state.firstname } onChange={ update__input } />
                                        }
                                        {/* <input type="text" className={ saveEdit ? "" : "disabled" } disabled={ !saveEdit } name="firstname" id='firstname' value="John"/> */}
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="lastname">Last name</label>
                                        { saveEdit ? <input type='text' className="" name="lastname" id="lastname"  value={ profile__current__state.lastname } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="lastname" id="lastname" value={ profile__current__state.lastname } onChange={ update__input } />
                                        }
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="country">Country</label>
                                        { saveEdit ? <input type='text' className="" name="country" id="country"  value={ profile__current__state.country } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="country" id="country" value={ profile__current__state.country } onChange={ update__input } />
                                        }
                                    </div>
                                </div>
                                <div className="email__phone">
                                    <div className="label__and__input">
                                        <label htmlFor="email">Email</label>
                                        { saveEdit ? <input type='text' className="" name="email" id="email"  value={ profile__current__state.email } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="email" id="email" value={ profile__current__state.email } onChange={ update__input } />
                                        }
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="phone">Phone number</label>
                                        { saveEdit ? <input type='text' className="" name="phone" id="phone"  value={ profile__current__state.phone } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="phone" id="phone" value={ profile__current__state.phone } onChange={ update__input } />
                                        }
                                    </div>
                                </div>
                                <div className="meta__key">
                                    <div className="label__and__input">
                                        <label htmlFor="metakey" className="label_copy">
                                            Metamask key
                                            <img
                                            src={ copy }
                                            alt="copy"
                                            title="Copy to clipboard"
                                            text-content={ metamask }
                                            onClick={ copyText__to__ }
                                            />
                                        </label>
                                        <span className="meta-key-disp">
                                            {/* <OnboardingButton></OnboardingButton> */}
                                            {(metamask!=="" && metamask!= "aucune") ? metamask : 
                                                metaRedux.hasMetamask? metaRedux.metamaskAccounts[0] : "Non disponible"
                                            }
                                        </span>
                                        {/* { saveEdit ? <input type='text' className="" name="metakey" id="metakey"  value={ profile__current__state.metakey } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="metakey" id="metakey" value={ profile__current__state.metakey } onChange={ update__input } />
                                        } */}
                                    </div>
                                </div>
                                <div className="save-edit">
                                    <label htmlFor="">Documents</label>
                                    { saveEdit ?
                                        <div className="upload" id='upload'>
                                            <div className="input">
                                                <label htmlFor="uploader">
                                                    <img src={ Upload } alt="" id='up-im' />
                                                    {/* <div className="hoverableImg" id="jpeg-up" onClick={ jpegdown }> */}
                                                    <div className="hoverableImg" id="jpeg-up" onClick={ imageDown2 }>
                                                        <img src={ jpeg } alt="" />
                                                        <img src={ jpegcol } alt="" className='floatty' />
                                                    </div>
                                                    {/* <Link className="invisible" to={ "/" + ultimate__initial__first__state__which__doesnt__change.KYC } ref={ jpegLink } target="_blank">dafadafa</Link> */}
                                                    <div className="hoverableImg" id="pdf-up" onClick={ imageDown2 }>
                                                        <img src={ pdf } alt="" />
                                                        <img src={ pdfcol } alt="" className='floatty' />
                                                    </div>
                                                </label>
                                                <input type="file" name="file" id="uploader"/>
                                            </div>
                                        </div> :
                                        <div className="download" id='download'>
                                            <div className="output">
                                                {/* replace it with a link to the files */}
                                                <label htmlFor="">
                                                    <img src={ Download } alt="" id='up-im' />
                                                    <div className="hoverableImg" id="jpeg-down" onClick={ showImage }>
                                                        <img src={ jpeg } alt="" />
                                                        <img src={ jpegcol } alt="" className='floatty' />
                                                    </div>
                                                    <Link className="invisible" to={ "./" + ultimate__initial__first__state__which__doesnt__change.KYC } ref={ jpegLink } target="_blank" download="something.jpg"></Link>
                                                    <div className="hoverableImg" id="pdf-down" onClick={ pdfdown }>
                                                        <img src={ pdf } alt="" />
                                                        <img src={ pdfcol } alt="" className='floatty' />
                                                    </div>
                                                    {/* <img src={ Download } alt="" id='down-im' />
                                                    <div className="hoverableImg" id="jpeg-down">
                                                        <img src={ jpeg } alt="" />
                                                        <img src={ jpegcol } alt="" className='floatty' />
                                                    </div>
                                                    <div className="hoverableImg" id="pdf-down">
                                                        <img src={ pdf } alt="" />
                                                        <img src={ pdfcol } alt="" className='floatty'/>
                                                    </div> */}
                                                </label>
                                            </div>
                                        </div>
                                    }
                                    
                                </div>
                                <div className="save__or__edit__button" onClick={switchto} title={ saveEdit ? "Save changes" : "Edit profile info" }>
                                    <div className="button-inner">
                                        <div className="text-sliding">{ saveEdit ? <>Sauvegarder</> : <>Editer</>}</div>
                                        { saveEdit ? <img src={ save } alt="" /> :
                                            <img src={ edit } alt="" />
                                        }
                                    </div>
                                </div>
                                <div className="pop-up-text invisible" ref={ pop }>
                                    Texte copié dans le presse-papiers.
                                </div>
                                { saveEdit && 
                                    <div className="abort-save__" onClick={ abort }>
                                        <div className="abort-inner">
                                            <div className="abort-crosses">
                                                <div className="abort"></div>
                                                <div className="abort"></div>
                                            </div>
                                            <div className="text-sliding">Annuler</div>
                                        </div>
                                    </div>
                                }
                            </>
                        }
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                    <div className={ imageMod? "imageModal" : "imageModal invisible"}>
                        <div className="im-mod-inner">
                            {
                                !loading && <>{isJpegFile(profile__current__state.KYC)?
                                <img src={ profile__current__state.KYC } alt="" />
                                :
                                <img src={ user } alt="" />}</>
                            }
                            <button onClick={ imageDown } style={ {cursor: "pointer"} }>
                                <img src={ Download } alt="" />
                                <span>Download Image</span>
                            </button>
                            <CloseButton right="0" transform="translate(calc(100% + .5em))" width="2em" onClick={ closeImage }/>
                        </div>
                    </div>
                    <MetamaskHandler class="invisible"></MetamaskHandler>
                    <div className={ loginShown.shown? "loginModalContainer" : "loginModalContainer invisible" }>
                        <LoginModal></LoginModal>
                    </div>
                </div>
            </div>
        </>
    );
}
