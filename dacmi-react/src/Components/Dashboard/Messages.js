import React, { useState, useEffect } from 'react';
import './Messages.css';
import Header from '../Header/Header';
import Sidemenu from './Sidemenu';
import Values from './Values';

import user from '../../media/user.svg';
import Loading from '../Loading/Loading';

import {io} from 'socket.io-client';
const socket= io();//default to window.location

const userInfo = localStorage.getItem("userInfo");
let resultat = JSON.parse(userInfo);
if(resultat!==null){
    // console.log(resultat.data._id);

    socket.emit(
        'joinRoom',
        {
            username: resultat.data.firstname + " " + resultat.data.lastname,
            room: resultat.data._id,
            id: resultat.data._id
        }
    );
}

// socket.on('welcome', (msg) => console.log(msg));

export default function Messages() {

    
    const [activeTab, setActiveTab] = useState(0); //'buy' and 'wallet'
    function setTab(event){
        let data_index= event.target.getAttribute('data-tab');
        setActiveTab(parseInt(data_index));
    }

    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    //script for the chat

    const [textToSend, setTextToSend] = useState("");
    
    const [realTime, setRealTime]= useState([]);

    const changeText= (event) => {
        setTextToSend(event.currentTarget.value);
    };
    function scrollDown(){
        let view= document.querySelector('.the-messaging-app');
        view.scrollTo({
            top: view.scrollHeight,
            left: 0,
            behavior: 'smooth'
        });
    }

    function gimme(event){
        event.preventDefault();
        if(textToSend===""){
            return;
        }
        // console.log(textToSend);
        socket.emit('chatMessage', {
            user: {username: resultat.data.firstname + " " + resultat.data.lastname, admin: false},
            body: textToSend
        });
        let now= new Date();
        let newM= {
            type: 'OUT',
            date: now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes(),
            content: textToSend
        };
        let arr= [...realTime];
        arr.push(newM);
        setRealTime(arr);
        setTextToSend('');
        let the_to__send= {
            conversationId: resultat.data._id,
            sender: resultat.data._id,
            text: textToSend
        };
        console.log(resultat.data._id);
        window.fetch('/ajout_message', {
            method: 'POST',
            body:JSON.stringify(the_to__send),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }
        })
        .then(thing => thing.json());
        // .then(res=> console.log(res));
        scrollDown();
    }

    useEffect(() => {
        socket.on('chatMessage', (thing) => {
            // console.log(thing.user + ' says : ' + thing.body);
            // console.log(thing.date);
            let newM= {
                type: 'IN',
                date: thing.date,
                content: thing.body
            };
            let arr= [...realTime];
            arr.push(newM);
            setRealTime(arr);
            scrollDown();
        });
        return () => {
            socket.off('chatMessage');
        }
    }, [realTime]);

    useEffect(() => {
        socket.on('roomW', (thing) => {
            console.log(thing.msg + " => " + thing.room);
        });
        return () => {
            socket.off('roomW');
        }
    }, []);

    //let's fill the inbox !!
    const [inboxContent, setInboxContent]= useState([]);//empty first..
    const [inboxGathered, setInboxGathered]= useState(false);//if the inbox content is ready
    // const [senders, setSenders] = useState([]);
    useEffect(()=> {//if there is a change on the converser state
        if(true){//do something only when converser is not null
            console.log(resultat.data._id);
            let sdrs= [];
            // console.log(converser._id);
            window.fetch('/affich_messagee/' + resultat.data._id, {
                method: 'GET',
                headers:{
                    "Content-Type":'application/json',
                    "Accept":'application/json'
                }
            }).then((result) => result.json())
            .then(output=> {
                let messages= [];
                output.forEach((element) => {
                    let single= {
                        type: element.sender!==resultat.data._id? 'IN' : 'OUT',
                        date: element.createdAt,
                        content: element.text,
                        key: element._id,
                        sender: element.sender,
                        read: element.lu
                    }
                    if((sdrs.find(el=> el===element.sender)=== undefined)&&(single.type=== 'IN')){
                        sdrs.push(element.sender);
                    }
                    messages.push(single);
                });
                setInboxContent(messages);
                setInboxGathered(true);
                scrollDown();

                if((inboxContent.find(elem=> elem.type=== 'IN' && elem.read=== false))!== undefined){
                    let req_body={
                        conversationId: resultat.data._id,
                        sender: sdrs
                    }
                    window.fetch('/modiffier_Multiple',{
                        method: 'PATCH',
                        headers:{
                            "Content-Type":'application/json',
                            "Accept":'application/json'
                        },
                        body: JSON.stringify(req_body)
                    }).then(i=> {
                        // console.log("success  ", i);
                        return i.json();
                    }).then(c=> console.log(c));
                }
            });
        }
    },[]);


    return (
        <>
        <div className="my-account">
            <Header title="Messages" accountType="user"></Header>
            <div className={ expanded ? "content expanded" : "content" }>
                <Sidemenu active='msg' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                <Values></Values>

                <div className="messages">
                    <div className="messages-head">
                        <img src={ user } alt="userlogo" />
                        <span className="user-name">
                            Admin
                            <span className="green-dot"></span>
                        </span>
                    </div>
                    <div className="the-messaging-app">
                        <div className="the-inner">
                            <div className="message-logs">
                                {
                                    !inboxGathered ?
                                    <>
                                    <div className="center-box">
                                        Chargement des anciens messages...<Loading/>
                                    </div>
                                    </>
                                    :
                                    (inboxContent.length<=0 && realTime.length<= 0) &&
                                    <>
                                    <div className="center-box">
                                        Vous n'avez pas encore commencé la conversation.
                                    </div>
                                    </>
                                }
                                {
                                    inboxContent.map(msg => (
                                    <>
                                        <div className={ msg.type=== "IN" ? "incoming" : msg.type=== 'OUT' ? "outcoming" : "invisible"} key={ msg.key }>
                                            <div className="single-body">{msg.content}</div>
                                            <div className="single-date">{msg.date}</div>
                                        </div>
                                    </>
                                    ))
                                }
                                {
                                    realTime.map((msg, index) => (
                                    <>
                                        <div className={ msg.type=== "IN" ? "incoming" : msg.type=== 'OUT' ? "outcoming" : "invisible"} key={ 'msg-single-' + index }>
                                            <div className="single-body">{msg.content}</div>
                                            <div className="single-date">{msg.date}</div>
                                        </div>
                                    </>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                        <div className="text-form">
                            <form className="text-to-send" onSubmit={ gimme }>
                                <input type="text" value={ textToSend } onChange={ changeText } autoFocus/>
                                <button type="submit">Envoyer</button>
                            </form>
                        </div>
                </div>
                <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                </div>
            </div>
        </div>
        </>
    )
}
