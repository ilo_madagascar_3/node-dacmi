import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
import './Dashboard.css';
import Header from '../Header/Header';
import Current from './Current';
import Wallet from './Wallet';
import Buy from './Buy';
import Values from './Values';
import Stats from './Stats';
import Sidemenu from './Sidemenu';



export default function Dashboard() {

    const userInfo = localStorage.getItem("userInfo");
    let resultat = JSON.parse(userInfo)
    
    const [activeTab, setActiveTab] = useState(0); //'buy' and 'wallet'
    function setTab(event){
        let data_index= event.target.getAttribute('data-tab');
        setActiveTab(parseInt(data_index));
    }

    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }
    return (
        <>
            <div className="my-account">
                <Header title="dashboard" accountType="user" user="John Smith"></Header>
                <div className={ expanded ? "content expanded" : "content" }>
                    <Sidemenu active='dsh' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    { activeTab === 0 ? <Stats></Stats> : <Values></Values>}

                    <div className="tabulable">
                        <div className="tabs">
                            {/* <div className="tab">CURRENT</div> */}
                            <div className={ activeTab === 0 ? 'tab active' : 'tab'} onClick={setTab} data-tab='0'>CURRENT VALUE</div>
                            <div className={ activeTab === 1 ? 'tab active' : 'tab'} onClick={setTab} data-tab='1'>WALLET</div>
                            <div className={ activeTab === 2 ? 'tab active' : 'tab'} onClick={setTab} data-tab='2'>BUY DACMI</div>
                        </div>
                        { activeTab === 0 && <Current></Current> }
                        { activeTab === 1 && <Wallet></Wallet> }
                        { activeTab === 2 && <Buy></Buy> }
                        {/* <Wallet></Wallet> */}
                        {/* <Buy></Buy> */}
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                </div>
            </div>
        </>
    );
}
