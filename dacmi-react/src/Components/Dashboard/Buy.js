import React from 'react';
import OnboardingButton from './Onboarding';
// import PaypalPage from "../../pages/PaypalPage";
import { Link } from 'react-router-dom';
import MetamaskHandler from '../MetamaskHandler/MetamaskHandler';
import { useSelector } from 'react-redux';

import './Buy.css';

export default function Buy() {
    let metaRedux= useSelector(state=> state.MetamaskReducer);
    return (
        <div className="tabulable-content">
            <div className="my-key">
                <p className="key">
                    Votre&nbsp;adresse&nbsp;Metamask&nbsp;:
                    {/* <span>
                        ox2bdfrg2rg5rg2163z32gr65z2gfe3r5g213s5gf3rg135erg435rg3er5g3er5
                    </span> */}
                    <OnboardingButton></OnboardingButton>
                </p>
            </div>
            <div className="methods">
                <div className="images">
                    <img src={ process.env.PUBLIC_URL + "coin-2.png" }/>
                    <img src={ process.env.PUBLIC_URL + "coin-3.png" }/>
                </div>
                <div className="cards">
                    {
                        metaRedux.hasMetamask?
                        <>
                        <div className="payment-line">
                            <p>Payment via Paypal</p>
                            <Link to={ "/paypal" } className="linkto_buy">
                                Acheter DACMI
                                <img src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_74x46.jpg" border="0" alt="PayPal Logo" className="PPlogo" style={ {width: "4em"} } />
                            </Link>
                        </div>
                        <div className="payment-line">
                            <p>Payment via Stripe</p>
                            <Link to={ "/stripe" } className="linkto_buy">
                                Acheter DACMI
                                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Stripe_Logo%2C_revised_2016.svg/512px-Stripe_Logo%2C_revised_2016.svg.png" border="0" alt="PayPal Logo" className="PPlogo" style={ {width: "4em"} } />
                            </Link>
                        </div>
                        {/* <div className="payment-lines">
                            <p>Payment by bank transfer</p>
                            <img src={ process.env.PUBLIC_URL + "rib.png" }/>
                        </div> */}
                        </>
                        :
                        <>
                        <div className="doesnt-have">
                            Pour pouvoir acheter DACMI, activez l'<Link to={{pathname: "https://metamask.io/download"}} target="_blank" style={{display: "inline"}}>extension&nbsp;Metamask</Link> sur votre navigateur et connectez-vous à votre compte.<br/>
                            Pour utiliser ce site, nous vous conseillons d'utiliser un navigateur qui supporte Metamask.
                        </div>
                        </>
                    }
                    {/* <div className="payment-line">
                        <p>Payment by Card</p>
                        <img src={ process.env.PUBLIC_URL + "card.png" }/>
                    </div>
                    <div className="payment-lines">
                        <p>Payment by bank transfer</p>
                        <img src={ process.env.PUBLIC_URL + "rib.png" }/>
                    </div> */}
                </div>
            </div>
            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
            <MetamaskHandler className="invisible"></MetamaskHandler>
        </div>
    );
}
