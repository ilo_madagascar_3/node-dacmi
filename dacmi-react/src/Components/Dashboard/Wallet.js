import React, { useRef, useEffect, useState } from 'react';
// import { getAccounts } from '../dmi';
import './Wallet.css';
import copy from '../../media/copy-w.svg';

export default function Wallet() {
    const [listachat, setListachat]= useState([]);
    const [ready, setReady]= useState(false);

    useEffect(async () => {
        // let key_state= await getAccounts();
        // let adressMetamask=key_state[0];
        // console.log("meta__key", key_state);
        let usercon=JSON.parse(localStorage.getItem('userInfo')).data;
        let id=usercon._id;
        // console.log("____kkkk___",id);
        // const affichAchat= () =>{
            const result = await window.fetch(`/affiche/${id}`);
            const val= await result.json();
            // console.log("hita",val);
            setListachat(val);
            setReady(true);
        // }
        // affichAchat()
    },[]);
    // let history=[
    //     {
    //         date: "10-12-21",
    //         adresse: "0x455e54548f54854654665454a465466",
    //         statut: "en attente"
    //     }
    // ];

    const pop= useRef(null);

    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Fallback: Copying text command was ' + msg);
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }

    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
          console.log('Async: Copying to clipboard was successful!');
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }

    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }

    return (
        <div className="wallet-content">

            <h3 className="wallet-h3">Historique de vos transactions</h3>
            <div className="table-holder">
                <table className="stuff">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Adresse</th>
                            <th>Etat</th>
                        </tr>
                    </thead>
                    <tbody>
                        { listachat.map((element, index) => (
                            <tr key= { "transaction_" + index}>
                                <td>
                                    { element.date }
                                </td>
                                <td>
                                    <span className="meta">
                                        { element.adressMetamask }<img src={ copy } alt="copy" title="Copy to clipboard" text-content={ element.adressMetamask } onClick={ copyText__to__ } />
                                    </span>
                                </td>
                                <td className={ element.statu ? "termine" : "at" } >
                                    { element.statu ? <>Terminé</> : <>En attente</>}
                                </td>
                            </tr>
                        )) }
                        { ( listachat.length == 0 && ready )? <tr><td colSpan="3">Votre historique de transactions est vide.</td></tr> : !ready ? <tr><td colSpan="3">Chargement...</td></tr> : <></> }
                    </tbody>
                </table>
                <div className="pop-up-text invisible" ref={ pop }>
                    Texte copié dans le presse-papiers.
                </div>
            </div>

            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
