import React, { useState, useRef } from 'react';
import './Guide.css';

import Header from '../../Header/Header';
import Values from '../Values';
import Sidemenu from '../Sidemenu';

import Download from '../../../media/download.png';
import pdf from '../../../media/pdf-des.png';
import pdfcol from '../../../media/pdf-col.png';

export default function Guide() {
    
    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };

    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    return (
        <>
            <div className="my-account">
                <Header title="our guide" accountType="user" user="John Smith"></Header>
                <div className={ expanded ? "content expanded" : "content" }>

                    <Sidemenu active='gui' expandIt={ expander } classes= { menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <Values></Values>
                    <div className="guides">
                        <div className="guide-line">
                            <div className="guide-title">How to buy DACMI</div>
                            <div className="download-container">
                                <img src={ Download } alt="" />
                                <div className="hoverableImg">
                                    <img src={ pdf } alt="" />
                                    <img src={ pdfcol } alt="" className="floatty" />
                                </div>
                            </div>
                        </div>
                        <div className="guide-line">
                            <div className="guide-title">Portefeuille de fonctionement interne à DACMI</div>
                            <div className="download-container">
                                <img src={ Download } alt="" />
                                <div className="hoverableImg">
                                    <img src={ pdf } alt="" />
                                    <img src={ pdfcol } alt="" className="floatty" />
                                </div>
                            </div>
                        </div>
                        <div className="guide-line">
                            <div className="guide-title">Metamask</div>
                            <div className="download-container">
                                <img src={ Download } alt="" />
                                <div className="hoverableImg">
                                    <img src={ pdf } alt="" />
                                    <img src={ pdfcol } alt="" className="floatty" />
                                </div>
                            </div>
                        </div>
                        <div className="guide-line">
                            <div className="guide-title">DACMI, usage and functionalities</div>
                            <div className="download-container">
                                <img src={ Download } alt="" />
                                <div className="hoverableImg">
                                    <img src={ pdf } alt="" />
                                    <img src={ pdfcol } alt="" className="floatty" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                </div>
            </div>
        </>
    );
}
