import React, { useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import './Dashboard.css';
import './Assistance.css';
import Header from '../Header/Header';
import Stats from './Stats';
import Sidemenu from './Sidemenu';
import { json } from 'body-parser';


let raw= localStorage.getItem('userInfo');
let not_raw= JSON.parse(raw);

export default function Assistance() {

    const userInfo = localStorage.getItem("userInfo");
    let resultat = JSON.parse(userInfo)
    
    const [activeTab, setActiveTab] = useState(0); //'buy' and 'wallet'
    function setTab(event){
        let data_index= event.target.getAttribute('data-tab');
        setActiveTab(parseInt(data_index));
    }

    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    const [helpMessage, setHelpMessage] = useState("");
    function changeHelpMessage(event){
        setHelpMessage(event.currentTarget.value);
    }
    
    let the_to__send= {
        conversationId: resultat.data._id,
        sender: resultat.data._id,
        text: helpMessage
    };

    const someRef= useRef(null);

    function sendMessage(event){
        event.preventDefault();
        window.fetch('/ajout_message', {
            method: 'POST',
            body:JSON.stringify(the_to__send),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }
        });
        // console.log(helpMessage);
        if(someRef){
            someRef.current.classList.remove('invisible');
        }
        setTimeout(() => {
            if(someRef){
                someRef.current.classList.add('fadeaway');
            }
        }, 4000);
        setTimeout(() => {
            if(someRef){
                someRef.current.classList.add('invisible');
            }
            if(someRef){
                someRef.current.classList.remove('fadeaway');
            }
        }, 5000);
        setHelpMessage('');
    }

    if(resultat!==null){
        if(resultat.status==="ok"){

            return (
                <>
                    <div className="my-account">
                        <Header title="Assistance" accountType="user" user="John Smith"></Header>
                        <div className={ expanded ? "content expanded" : "content" }>
                            <Sidemenu active='ass' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                            <Stats></Stats>

                            <div className="assistance">
                                <div className="dodo">
                                    Vous avez besion d'une assistance?<br/>
                                    Contactez le support par
                                    <Link className="underlined" to="/messages">
                                        Chat.
                                    </Link>
                                </div>
                                <form onSubmit={sendMessage}>
                                    <input type="text" className="yellow" placeholder="Posez votre question" value={helpMessage} onChange={changeHelpMessage}/>
                                </form>
                                <div className="dodo">
                                    Vous pouvez également nous contacter :
                                </div>
                                <div className="whitewhite">
                                    Par mail à
                                    <Link to="#" onClick={ (e) => { window.location="mailto: supportdacmi@digitalcoin-mineral.com"; e.preventDefault(); } } className="underlined">
                                        supportdacmi@digitalcoin-mineral.com
                                    </Link><br/>
                                    Par téléphone au: +212 707 7109 27.
                                </div>
                                <div className="pop_up__ invisible" ref={someRef}>
                                    Votre message a été envoyé à nos administrateur, vous pouvez voir votre conversation avec eux dans la section <Link className="underlined" to="/messages">chat.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
    }else{
        return(
            <div className="not-connected">
                Vous devez être connecté(e) avant d'aller sur cette page.
                <Link to="/login" className="centerL">Page de connexion</Link>
            </div>
        );
    }
}
