import React, { useRef, useState, useEffect } from 'react';
import './Header.css';
import axios from 'axios';
import { ReactComponent as User} from '../../media/user.svg';
import { Link } from 'react-router-dom';

import acc from '../../media/account-w.svg';
import accG from '../../media/account-g.svg';
import env from '../../media/envellope-w.svg';
import envG from '../../media/envellope-g.svg';
import pow from '../../media/power-w.svg';
import powG from '../../media/power-g.svg';
import { useSelector } from 'react-redux';

import isJpegFile from '../fileExtension';


export default function Header(props) {
	let linkToLogin= useRef(null);
	let linkToAccount= useRef(null);
	let linkToMessages= useRef(null);
	const [menuShown, setMenuShown] = useState(false);//is the menu shown??
	const {firstname, lastname} = JSON.parse(localStorage.getItem('userInfo')).data;
	const [date, setDate] = useState("");
	const today= new Date();
	const months=['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'];
	useEffect(() => {
		setDate( today.getDate() + " " + months[today.getMonth()] + " " + today.getFullYear() );
	}, []);
	
	let borderer= useRef(null);
	let hiddenMenu= useRef(null);

	 const showToken= useSelector(state => state.logReducer);
	useEffect(() => {
		document.addEventListener('click', hideMenu);
		return () => {
			document.removeEventListener('click', hideMenu);
		}
	}, [borderer, hiddenMenu]);

	function hideMenu(event){
		// console.log(event.target.classList);
		if(event.target.classList.contains("menu-link") || event.target.classList.contains("text-link")){
			return;
		}
		
		if(!event.target.classList.contains("hidden-menu")){
			setMenuShown(false);
			if(borderer.current){
				borderer.current.classList.add('invisible');
			}
			if(hiddenMenu.current){
				hiddenMenu.current.classList.add('invisible');
			}
		}
	}

	function toggleMenu(event){
		event.stopPropagation();
		setMenuShown(!menuShown);
		if(borderer.current){
			borderer.current.classList.toggle('invisible');
		}
		if(hiddenMenu.current){
			hiddenMenu.current.classList.toggle('invisible');
		}
	}
    async function Logout(event){
        
		event.preventDefault();
        //  console.log("ooooo",showToken);
		//  let token=showToken.data.refreshToken;
        //  let res  = await axios.post(`/api/deconnect`,{
        //     method:'POST',
        //     body:JSON.stringify(token),
        //     headers:{
        //         authorization: "Bearer " + showToken.data.accessToken,
		// 		"Content-Type": "application/json",
		// 		"Accept":'application/json'
        //     }
        //     });
		localStorage.removeItem('userInfo');
		linkToLogin.current.click();
	}

	function account(){
		linkToAccount.current.click();
	}

	function messages(){
		linkToMessages.current.click();
	}

	const [userData, setUserData] = useState(null);
	const [gotUserData, setGotUserData] = useState(false);

	useEffect(() => {
		window.fetch(`/afficheuser/${JSON.parse(localStorage.getItem('userInfo')).data._id}`)
		.then(data => data.json())
		.then((res)=>{
			setUserData(res[0]);
			// console.log(userData);
			// console.log(res);
			setGotUserData(true);
		});
	}, []);

	// function lastThree(someStr){
	// 	// console.log((someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]));
	// 	return(someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]);
	// }

    return (
        <div className="header">
			<img className="header-banner" src={ process.env.PUBLIC_URL + '/myaccount.jpg' }  id="logo_"/>
			{/* <img className="header-banner" src={ '/photo-by-face-generator_61924f535d2207000fe6d544.jpg' }  id="logo_"/> */}
			<div className="header-bar">
				<div className="left">
					<div className="date" style={{ wordSpacing: '.25em' }}>
						{ date }
					</div>
					<div className="crumb">
						<h2>{ props.title }</h2>
					</div>
				</div>

				<div className="right">
					<div className="user-name">
						<span className="green-dot"></span>
						{firstname + ' ' +lastname}
					</div>

					{
						(gotUserData && isJpegFile(userData.KYC))?
						<img src={ userData.KYC } alt="profile"  width={ '70%' } height={ '70%' } className="avatar" onClick={ account }/>
						:
						<div className="avatar" onClick={ account }>
						<User width={ '70%' } height={ '70%' }></User>
						</div>
					}

					{/* <div className="avatar" onClick={ account }>
						<User width={ '70%' } height={ '70%' }></User>
					</div> */}
					{/* <img src={ userData.KYC } alt="profile"  width={ '70%' } height={ '70%' } className="avatar"/> */}

					<div className={ menuShown? "vertical-dots act" : "vertical-dots" } onClick={ toggleMenu }>
						<div className="dot"></div>
						<div className="dot"></div>
						<div className="dot"></div>
						<div className="borderer invisible" ref={ borderer }></div>
						<div className="hidden-menu invisible" ref={ hiddenMenu }>
							<Link className='invisible' to='/login' ref={ linkToLogin }></Link>
							<Link className='invisible' to='/account' ref={ linkToAccount }></Link>
							<Link className='invisible' to='/messages' ref={ linkToMessages }></Link>
							<ul className="menu-list">
								<li className="menu-item" onClick={Logout}>
									<div className="supperpos">
										<img src={pow} alt="" />
										<img src={powG} alt="" floatter="true"/>
									</div>
									<div className="text">
										Logout
									</div>
								</li>
								<li className="menu-item" onClick={account}>
									<div className="supperpos">
										<img src={acc} alt="" />
										<img src={accG} alt="" floatter="true"/>
									</div>
									<div className="text">
										Account
									</div>
								</li>
								<li className="menu-item" onClick={messages}>
									<div className="supperpos">
										<img src={env} alt="" />
										<img src={envG} alt="" floatter="true"/>
										<div className="new-message-indic"></div>
									</div>
									<div className="text">
										Messages
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
        </div>
    );
}
