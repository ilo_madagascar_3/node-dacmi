import React from 'react';
import './Bill.css';
import Header from '../Header/Header';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';
import pdf from '../../media/pdf-des.png';
import pdfcol from '../../media/pdf-col.png';

export default function Bill() {
    let data={
        t_id: "22ea22f21d2",
        c_id: "15er5151s54",
        firstname: "Mahay",
        lastname: "Rakoto",
        dacmi: 5.5,
        one_dacmi: 3,// one DACMI costs 3€
        frais: 0.5,
        date: "22-12-21"
    }

    async function printDocument(){

        const input = document.querySelector('#logo_');
        const pdf = new jsPDF();
        
        let canvas= await html2canvas(input);
        const imgData = canvas.toDataURL('image/png');
        pdf.addImage(imgData, 'JPEG', 20, 10, 46, 20);

        pdf.autoTable({//client and bill info
            margin: { left: 140 },
            theme: 'grid',
            headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
            bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
            styles: { font: 'monospace' },
            columnStyles: {
                2:{ halign: 'right' }
            },
            html: '#the'
        });
        
        pdf.autoTable({//DACMI info
            startY: 30,
            margin: { right: 120, left: 20 },
            theme: 'grid',
            headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
            bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
            styles: { font: 'monospace' },
            columnStyles: {
                2:{ halign: 'right' }
            },
            body: [
                ["NEGOCE international UA"],
                ["a.gerno@negoce-int-ua.com"],
                ["+212 707 7109 27"]
            ],
            bodyStyles: {
                cellPadding: 0,
                lineWidth: 0,
                textColor: [0,0,0]
            }
        });
        // pdf.text(20, 20, "NEGOCE international UA");
        // pdf.text(20, 30, "a.gerno@negoce-int-ua.com");
        // pdf.text(20, 40, "+212 707 7109 27");

        pdf.autoTable({//the actual bill
            // startY: 40,
            theme: 'grid',
            headStyles: { lineWidth: 0.1, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap', halign: 'center' },
            bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 'wrap', halign: "right" },
            styles: { font: 'monospace' },
            html: '.bill'
        });

        pdf.autoTable({//bill status
            theme: 'grid',
            headStyles: {
                lineWidth: 0.1,
                lineColor: [0,0,0],
                fillColor: [255,255,255],
                textColor: [0,0,0],
                cellWidth: 40
            },
            bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 40, textColor: [0,0,0] },
            columnStyles: {
                0:{ fontStyle: 'bold' }
            },
            styles: { font: 'monospace' },//doesn't work :(
            html: '.status'
        });


        pdf.save("download.pdf");
    }

    return (
        <>
            <Header title="facture"></Header>
            <div className="inner-bill">
                <h1 className="bill-title">Les détails de votre dernière transaction</h1>
                <div className="bill-content">
                    <div className="bill-head">
                        <img className="header-banner" src={ process.env.PUBLIC_URL + '/logo_.jpg' } id="logo_" />
                        <div className="bill-space"></div>
                        <div className="bill-details">
                            <div className="details-line">
                                <div className="details-label">Transaction ID</div>
                                <div className="details-value">{ data.t_id }</div>
                            {/* </div>
                            <div className="details-line"> */}
                                <div className="details-label">Client ID</div>
                                <div className="details-value">{ data.c_id }</div>
                            {/* </div>
                            <div className="details-line"> */}
                                <div className="details-label">Nom client</div>
                                <div className="details-value">{ data.firstname + " " + data.lastname }</div>
                            </div>
                        </div>
                        <div className="dacmi-info">
                            <div className="info">
                                NEGOCE international UA<br/>
                                a.gerno@negoce-int-ua.com <br/>
                                +212 707 7109 27
                            </div>
                        </div>
                    </div>
                    <div className="bill-body">
                        <table className="bill">
                            <thead>
                                <tr>
                                    <th>Montant (€)</th>
                                    <th>Valeur DACMI (€)</th>
                                    <th>DACMI demandé</th>
                                    <th>Montant transféré (€)</th>
                                    <th>Frais(?) (€)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{ data.one_dacmi * data.dacmi + data.frais }</td>
                                    <td>{ data.one_dacmi }</td>
                                    <td>{ data.dacmi }</td>
                                    <td>{ data.one_dacmi * data.dacmi }</td>
                                    <td>{ data.frais }</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="state-and-stuff">
                        <table className="status">
                            <tbody>
                                <tr>
                                    <td>Etat du paiement</td>
                                    <td>Effectué</td>
                                </tr>
                                <tr>
                                    <td>Mode de paiement</td>
                                    <td>Paypal</td>
                                </tr>
                                <tr>
                                    <td>Date de paiement</td>
                                    <td>{ data.date }</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div className="invisible">
                <table id="the">
                    <tr>
                        <td>Transaction ID</td>
                        <td>:</td>
                        <td>{ data.t_id }</td>
                    </tr>
                    <tr>
                        <td>Client ID</td>
                        <td>:</td>
                        <td>{ data.c_id }</td>
                    </tr>
                    <tr>
                        <td>Nom client</td>
                        <td>:</td>
                        <td>{ data.firstname + " " + data.lastname }</td>
                    </tr>
                </table>
            </div>
            <div className="bill-actions">
                <button className="one" onClick={ printDocument }>
                    <div className="inner-one">
                        <div className="hoverable-img">
                            <img src={ pdf } alt="" />
                            <img src={ pdfcol } alt="" className='floater' />
                        </div>
                        <div className="text-one">
                            Télecharger PDF
                        </div>
                    </div>
                </button>
                {/* <button className="two">Action two</button> */}
            </div>
        </>
    )
}
