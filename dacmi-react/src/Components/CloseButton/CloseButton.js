import React from 'react';
import './CloseButton.css';

export default function CloseButton(props) {
    return (
        // <div className="closer-c" style={{ width: '3em', height: '3em', top: '2em', right: '2em' }}>
        <div className="closer-c" style={{ width: props.width || '3em', height: props.height || '3em', top: props.top || '2em', right: props.right || '2em', transform: props.transform || "none" }} onClick={ props.onClick }>
            <div className="cross"></div>
            <div className="cross"></div>
        </div>
    )
}
