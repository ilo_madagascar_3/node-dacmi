import React, {useState} from 'react';
import './LoginModal.css';
import {useDispatch} from 'react-redux';
import Loading from '../Loading/Loading';

const sessionDuration= 100;
const sessionUnity= "secondes";

export default function LoginModal() {
    const [inputStates, setinputStates] = useState({
        email: "",
        pass: ""
    });
    const [buttonState, setButtonState] = useState(0);
    //0 is normal, not pressed, waiting for action
    //1 is loading, processing stuff
    //2 is success, everything is done and OK
    //3 is error

    const dispatch= useDispatch();

    const submiter_= (event)=> {
        event.preventDefault();
        // console.log(inputStates);
    };

    const userTypes_= (event)=> {
        setButtonState(0);
        let temp= {...inputStates};
        temp[event.currentTarget.name]= event.currentTarget.value;
        setinputStates(temp);
        // console.log(inputStates);
    };

    async function CloginModal(event){
        setButtonState(1);
        event.preventDefault();
        
        let item={
            email:inputStates.email,
            password:inputStates.pass
        }
        
        let res  = await window.fetch(`/api/login`,{
            method:'POST',
            body:JSON.stringify(item),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            },
        });
        const resulta = await res.json();
        console.log(resulta);
        if(resulta.status=== "ok"){
            setButtonState(2);
            localStorage.removeItem('userInfo');
            localStorage.setItem("userInfo", JSON.stringify(resulta));
            dispatch({
                type: 'HIDE'
            });
            setButtonState(0);
        } else {
            setButtonState(3);
        }
    }
    return (
        <>
        <div className="loginForm">
            <h2>
                Votre session a expiré
            </h2>
            <p className="explainer">
                Pour des raisons de sécurité, le site vous gardera connecté(é) pendant { sessionDuration + " " + sessionUnity }. Après ce delai, il vous sera demandé de vous reconnecter pour vérifier votre identité.
            </p>
            <form action="" className="inModalForm" onSubmit={ submiter_ }>
                <div className="line_">
                    <label htmlFor="email">Email</label>
                    <input type="text" name="email" id="email" value= { inputStates.email } onChange= {userTypes_} />
                </div>
                <div className="line_">
                    <label htmlFor="password">Mot de passe</label>
                    <input type="password" name="pass" id="password" value= { inputStates.pass }  onChange= {userTypes_} />
                </div>
                <div className="line_">
                    <button type= "submit" onClick={ CloginModal } className={ "button-" + buttonState }>
                        {
                            (()=>{
                                switch (buttonState) {
                                    case 0:
                                        return <>Continuer</>;
                                    case 1:
                                        return <Loading/>;
                                    case 2:
                                        return <>Succes</>;
                                    case 3:
                                        return <>Mot de passe ou email erronné</>;
                                    default:
                                        break;
                                }
                            })()
                            // <Loading/>
                        }
                    </button>
                </div>
            </form>
        </div>
        </>
    )
}
