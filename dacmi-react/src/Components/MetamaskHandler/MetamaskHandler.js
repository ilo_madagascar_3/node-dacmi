import React from 'react';
import { useState, useEffect } from 'react';
import MetaMaskOnboarding from '@metamask/onboarding';
import { useDispatch } from 'react-redux';

export default function MetamaskHandler() {
    const [accounts, setAccounts]= useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        if(MetaMaskOnboarding.isMetaMaskInstalled()){
            window.ethereum.request({ method: "eth_requestAccounts" })
            .then((response)=> setAccounts(response));
            window.ethereum.on('accountsChanged', (change)=> setAccounts(change));
        }
        // return(()=> { window.ethereum.off('accountsChanged') });
    }, []);

    useEffect(() => {
        if(accounts.length!= 0){
            dispatch({
                type: 'SETMETA',
                metamaskAccounts: accounts
            });
        }
    }, [accounts]);

    return (
        <></>
    )
}
