export const GWEI = 1000000000;
export const WEI = 1000000000000000000;
export const DACMI = 3936.08;

function numberToHexString(value){
    return '0x' + parseInt(value).toString(16);
}

function hexStringToNumber(value){
    return parseInt(value, 16);
}

function showEthNumber(balance, n = 2){
    return (balance / WEI).toFixed(n);
}

function showDcmiNumber(euro, n=2){
    return (euro/DACMI).toFixed(n);
}

export {numberToHexString,hexStringToNumber,showEthNumber,showDcmiNumber};