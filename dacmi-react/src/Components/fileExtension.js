function lastThree(params){
    return(params[params.length - 3] + params[params.length - 2] + params[params.length - 1]);
}

function lastFour(params){
    return(params[params.length - 4] + params[params.length - 3] + params[params.length - 2] + params[params.length - 1]);
}

//checks if the string input ends with either 	'.jpg' , '.jpeg' , '.jpe' , '.jif' or '.jfif'
//whic are the extensions for jpeg files..
export default function isJpegFile(stringParams){
    if(lastThree(stringParams).match(/[Jj][Pp][Gg]/)){
        return true;
    }
    if(lastThree(stringParams).match(/[Jj][Pp][Ee]/)){
        return true;
    }
    if(lastThree(stringParams).match(/[Jj][Ii][Ff]/)){
        return true;
    }
    if(lastFour(stringParams).match(/[Jj][Pp][Ee][Gg]/)){
        return true;
    }
    if(lastFour(stringParams).match(/[Jj][Ff][Ii][Ff]/)){
        return true;
    }
    return false;
};