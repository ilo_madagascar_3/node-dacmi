import React, { useState, useRef, useEffect } from 'react';
import './Account.css';

import Header from '../Header';
import Stats from '../Stats';
import Sidemenu from '../Sidemenu';
// import { getAccounts } from '../../dmi';
import Bar from '../../Loading/Bar';

import Upload from '../../../media/upload-gold.png';
import Download from '../../../media/download.png';
import jpeg from '../../../media/jpeg-des.png';
import jpegcol from '../../../media/jpeg-col.png';
import pdf from '../../../media/pdf-des.png';
import pdfcol from '../../../media/pdf-col.png';
import save from '../../../media/save-wh.png';
import edit from '../../../media/edit-wh.png';
import copy from '../../../media/copy-w.svg';
import user from '../../../media/user.svg';

import axios from 'axios';
import FileDownload from 'js-file-download';
import CloseButton from '../../CloseButton/CloseButton';
import { useSelector } from 'react-redux';
import MetamaskHandler from '../../MetamaskHandler/MetamaskHandler';

import isJpegFile from '../../fileExtension';

// import Loading from '../../Loading/Loading';

export default function Account() {
    //some Metamask stuff..
    const metaRedux= useSelector(state=> state.MetamaskReducer);
    // useEffect(() => {
    //     console.log(metaRedux.metamaskAccounts);
    //     console.log(metaRedux);
    // }, [metaRedux]);
    //---END---


    const [metamask, setMetamask]= useState("");

    const [ultimate__initial__first__state__which__doesnt__change, setUltimate__state__init] = useState("");
    //yep, it doesn't change unless the one in the DB changes :D !

    const uploadLogo= useRef(null);
    const [saveEdit, setSaveEdit]= useState(false);//is on read mode when false
    // const [userparid, setUserparid]= useState([]);
    const [profile__current__state, setProfile__current__state] = useState("");
    
    //loading thing..
    const [loading, setLoading] = useState(true)
    //loading thing.. END---

    useEffect(() => {
        const affichUserid= async () =>{
            let valeur=JSON.parse(localStorage.getItem('userInfo')).data;
            const userid = await window.fetch(`/afficheuser/${valeur._id}`);
            const resultuserid= await userid.json();

            // let key_state= await getAccounts();
            // let adressMetamask=key_state[0];
            // setMetamask(adressMetamask);
            // // console.log(resultuserid[0]);
            // if(resultuserid[0].adressMetamask=== undefined){
            //     resultuserid[0].adressMetamask= [adressMetamask];
            //     console.log("undefinded it is : ", resultuserid[0].adressMetamask);
            // } else {
            //     console.log("ELSE it is :");
            //     if(typeof(resultuserid[0].adressMetamask)== "object"){
            //         console.log("OBJECT it is :");
            //         if(!resultuserid[0].adressMetamask.includes(adressMetamask)){
            //             resultuserid[0].adressMetamask.append(adressMetamask);
            //             console.log("not included it is : ", resultuserid[0].adressMetamask);
            //         }
            //     } else {
            //         console.log("PRIMITIVE it is :");
            //         if(resultuserid[0].adressMetamask!== adressMetamask){
            //             resultuserid[0].adressMetamask= [resultuserid[0].adressMetamask, adressMetamask];
            //             console.log("different it is : ", resultuserid[0].adressMetamask);
            //         } else {
            //             resultuserid[0].adressMetamask= [resultuserid[0].adressMetamask];
            //             console.log("equal they are : ", resultuserid[0].adressMetamask);
            //         }
            //     }
            // }//after this resultuserid[0].adressMetamask becomes an array !!
             //but not the corresponding value in the database :( !
             
            if((resultuserid[0].adressMetamask!= "")&&(resultuserid[0].adressMetamask!="aucune")&&(resultuserid[0].adressMetamask!= undefined)){
                // console.log("doenst have", resultuserid[0].adressMetamask);
                setMetamask(resultuserid[0].adressMetamask);
            } else {
                if(metaRedux.hasMetamask){
                    // console.log("has meta", resultuserid[0]);
                    setMetamask(metaRedux.metamaskAccounts[0]);
                    resultuserid[0].adressMetamask= metaRedux.metamaskAccounts[0];
                    await window.fetch(`/modiffieruser/${resultuserid[0]._id}`,{
                        headers:{
                            "Accept":'application/json',
                            "Content-Type":'application/json'
                        },
                        method:'PATCH',
                        body: JSON.stringify(resultuserid[0])
                    });
                }
            }

            // setMetamask(resultuserid[0].adressMetamask);

            // resultuserid[0].adressMetamask= resultuserid[0].adressMetamask[0];//it's a primitive again !!
            setUltimate__state__init(resultuserid[0]);//the original kept in a safe location, in case the user aborts the edition..

            await window.fetch(`/modiffieruser/${resultuserid[0]._id}`,{
                headers:{
                    "Accept":'application/json',
                    "Content-Type":'application/json'
                },
                method:'PATCH',
                body: JSON.stringify(resultuserid[0])
            });
            // thing= thing.json();
            // console.log(resultuserid[0]);
            // console.log(thing);
            setProfile__current__state(resultuserid[0]);
            setLoading(false);
        };
        affichUserid();
    },[]);

    // console.log("modif",profile__current__state);
    //,adressMetamask:profile__current__state.adressMetamask
    function switchto(){
        if(saveEdit){
            (async ()=> {
            let item={
                firstname:profile__current__state.firstname,
                lastname:profile__current__state.lastname,
                country:profile__current__state.country,
                adressMetamask:profile__current__state.adressMetamask,
                phone:profile__current__state.phone,
                email:profile__current__state.email
                // adressMetamask: (profile__current__state.adressMetamask!=undefined? (profile__current__state.adressMetamask!= metamask ? [profile__current__state.adressMetamask, metamask] : [profile__current__state.adressMetamask] ) : [metamask])
                // adressMetamask:metamask
            };
            // console.log("alefa",item,profile__current__state._id);
            await window.fetch(`/modiffieruser/${profile__current__state._id}`,{
                headers:{
                        "Accept":'application/json',
                        "Content-Type":'application/json'
                        }, 
                method:'PATCH',
                body: JSON.stringify(item)
            }).then(stuff=> stuff.json())
            .then(row=> {
                // console.log(row);
                window.fetch('/afficheuser/' + row._id)
                .then(thing=> thing.json())
                .then(res=> {
                    let newInfo= {
                        status: "ok",
                        data: res[0]
                    };
                    localStorage.removeItem('userInfo');
                    localStorage.setItem("userInfo", JSON.stringify(newInfo));
                });
            });
        })(); 
        }
        setSaveEdit(!saveEdit);
    }

    //initial mock state, to be fetched from DB later
    const {email, firstname, lastname} = JSON.parse(localStorage.getItem('userInfo')).data;
    // let affichUser=userparid[0];
    // console.log("userparid",affichUser);
    // const [profile__current__state, setProfile__current__state] = useState(initial__profile__state);

    function update__input(e){
        let new__state= {...profile__current__state};
        new__state[e.target.getAttribute('name')]= e.target.value;
        setProfile__current__state(new__state);
    }
    
    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };

    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    function abort(){
        //Abort the edition
        setSaveEdit(false);
        setProfile__current__state(ultimate__initial__first__state__which__doesnt__change);
    }

    const pop= useRef(null);
    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Fallback: Copying text command was ' + msg);
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
          console.log('Async: Copying to clipboard was successful!');
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }
    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }

    //image---
    const [imageMod, setImageMod] = useState(false);
    const showImage= ()=>{
        // console.log('stuff');
        setImageMod(true);
    };
    const closeImage= ()=>{
        setImageMod(false);
    };
    const imageDown= function(event){
        // event.preventDefault();
        if(isJpegFile(profile__current__state.KYC)){
            axios.get('/imageDownload/' + encodeURIComponent(profile__current__state.KYC), {
                responseType: "blob"
            })
            .then(response=> {
                // console.log(response);
                // console.log("fuck you");
                FileDownload(response.data, "image.jpg");
                console.log("downloaded: ", encodeURIComponent(profile__current__state.KYC));
            });
        }
    };


    return (
        <>
            <div className="my-account">
                <Header title="account" accountType="admin" user="Admin"></Header>
                <div className={ expanded ? "content expanded" : "content" }>

                    <Sidemenu active='acc' expandIt={ expander } classes= { menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <Stats></Stats>
                    <div className="profile-form">
                        {
                            loading?
                            <>
                                <div className="fullrows">
                                    <div className="one_">
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="one_one"><Bar></Bar><Bar></Bar></div>
                                    </div>
                                    <div className="two_">
                                        <div className="two_one"><Bar></Bar><Bar></Bar></div>
                                        <div className="two_one"><Bar></Bar><Bar></Bar></div>
                                    </div>
                                    <div className="thr_">
                                        <Bar></Bar>
                                        <Bar></Bar>
                                    </div>
                                    <div className="fou_">
                                        <Bar></Bar>
                                        <Bar></Bar>
                                    </div>
                                </div>
                            </>
                            :
                            <>
                                <div className="name-country">
                                    <div className="label__and__input">
                                        <label htmlFor="firstname">First name</label>
                                        { saveEdit ? <input type='text' className="" name="firstname" id="firstname"  value={ profile__current__state.firstname } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="firstname" id="firstname" value={ profile__current__state.firstname } onChange={ update__input } />
                                        }
                                        {/* <input type="text" className={ saveEdit ? "" : "disabled" } disabled={ !saveEdit } name="firstname" id='firstname' value="John"/> */}
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="lastname">Last name</label>
                                        { saveEdit ? <input type='text' className="" name="lastname" id="lastname"  value={ profile__current__state.lastname } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="lastname" id="lastname" value={ profile__current__state.lastname } onChange={ update__input } />
                                        }
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="country">Country</label>
                                        { saveEdit ? <input type='text' className="" name="country" id="country"  value={ profile__current__state.country } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="country" id="country" value={ profile__current__state.country } onChange={ update__input } />
                                        }
                                    </div>
                                </div>
                                <div className="email__phone">
                                    <div className="label__and__input">
                                        <label htmlFor="email">Email</label>
                                        { saveEdit ? <input type='text' className="" name="email" id="email"  value={ profile__current__state.email } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="email" id="email" value={ profile__current__state.email } onChange={ update__input } />
                                        }
                                    </div>
                                    <div className="label__and__input">
                                        <label htmlFor="phone">Phone number</label>
                                        { saveEdit ? <input type='text' className="" name="phone" id="phone"  value={ profile__current__state.phone } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="phone" id="phone" value={ profile__current__state.phone } onChange={ update__input } />
                                        }
                                    </div>
                                </div>
                                <div className="meta__key">
                                    <div className="label__and__input">
                                        <label htmlFor="metakey" className="label_copy">
                                            Metamask key
                                            <img
                                            src={ copy }
                                            alt="copy"
                                            title="Copy to clipboard"
                                            text-content={ metamask }
                                            onClick={ copyText__to__ }
                                            />
                                        </label>
                                        <span className="meta-key-disp">
                                            {/* <OnboardingButton></OnboardingButton> */}
                                            {(metamask!=="" && metamask!= "aucune") ? metamask : 
                                                metaRedux.hasMetamask? metaRedux.metamaskAccounts[0] : "Non disponible"
                                            }
                                        </span>
                                        {/* { saveEdit ? <input type='text' className="" name="metakey" id="metakey"  value={ profile__current__state.metakey } onChange={ update__input } /> :
                                            <input type='text' className="disabled" disabled name="metakey" id="metakey" value={ profile__current__state.metakey } onChange={ update__input } />
                                        } */}
                                    </div>
                                </div>
                                <div className="save-edit">
                                    <label htmlFor="">Documents</label>
                                    { saveEdit ?
                                        <div className="upload" id='upload'>
                                            <div className="input">
                                                <label htmlFor="uploader">
                                                    <img src={ Upload } alt="" id='up-im' />
                                                    <div className="hoverableImg" id="jpeg-up" >
                                                        <img src={ jpeg } alt="" />
                                                        <img src={ jpegcol } alt="" className='floatty' />
                                                    </div>
                                                    <div className="hoverableImg" id="pdf-up">
                                                        <img src={ pdf } alt="" />
                                                        <img src={ pdfcol } alt="" className='floatty' />
                                                    </div>
                                                </label>
                                                <input type="file" name="file" id="uploader"/>
                                            </div>
                                        </div> :
                                        <div className="download" id='download'>
                                            <div className="output">
                                                {/* replace it with a link to the files */}
                                                <label htmlFor="">
                                                    <img src={ Download } alt="" id='down-im' />
                                                    <div className="hoverableImg" id="jpeg-down" onClick={ showImage }>
                                                        <img src={ jpeg } alt="" />
                                                        <img src={ jpegcol } alt="" className='floatty' />
                                                    </div>
                                                    <div className="hoverableImg" id="pdf-down">
                                                        <img src={ pdf } alt="" />
                                                        <img src={ pdfcol } alt="" className='floatty'/>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    }
                                    
                                </div>
                                <div className="save__or__edit__button" onClick={switchto} title={ saveEdit ? "Save changes" : "Edit profile info" }>
                                    <div className="button-inner">
                                        <div className="text-sliding">{ saveEdit ? <>Sauvegarder</> : <>Editer</>}</div>
                                        { saveEdit ? <img src={ save } alt="" /> :
                                            <img src={ edit } alt="" />
                                        }
                                    </div>
                                </div>
                                <div className="pop-up-text invisible" ref={ pop }>
                                    Texte copié dans le presse-papiers.
                                </div>
                                { saveEdit && 
                                    <div className="abort-save__" onClick={ abort }>
                                    <div className="abort-inner">
                                    <div className="abort-crosses">
                                    <div className="abort"></div>
                                    <div className="abort"></div>
                                    </div>
                                    <div className="text-sliding">Annuler</div>
                                        </div>
                                        </div>
                                }
                            
                            </>
                        }
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                    <div className={ imageMod? "imageModal" : "imageModal invisible"}>
                        <div className="im-mod-inner">
                            {
                                !loading && <>{isJpegFile(profile__current__state.KYC)?
                                <img src={ "../" + profile__current__state.KYC } alt="no image?" />
                                :
                                <img src={ user } alt="" />}</>
                            }
                            <button onClick={ imageDown } style={ {cursor: "pointer"} }>
                                <img src={ Download } alt="" />
                                <span>Download Image</span>
                            </button>
                            <CloseButton right="0" transform="translate(calc(100% + .5em))" width="2em" onClick={ closeImage }/>
                        </div>
                    </div>
                    <MetamaskHandler class="invisible"></MetamaskHandler>
                </div>
            </div>
        </>
    );
}
