import React, { useState, useEffect } from 'react';
import './Wallet.css';
import { Link } from 'react-router-dom';
import { useRef } from 'react';
import { getAccounts } from "../dmi";


export default function Wallet() {

    const [to_do, setTo_do] = useState([]);
    const [in_gress, setIn_gress] = useState([]);
    const [done, setDone] = useState([]);
    const [req_done, setReq_done] = useState(false);
    const [done_done, setDone_done] = useState(false);

    useEffect(() => {
        const affichResult= async () =>{
            const resultats = await window.fetch('/afficheDacmi');
            const result= await resultats.json();
            let to_do_= result.filter(el => el.statu== false);
            let done_= result.filter(el => el.statu== true);
            setTo_do(to_do_);
            setReq_done(true);
            setDone(done_);
            setDone_done(true);
            // console.log(result);
            // console.log(to_do_);
            // console.log(done_);
        }
        affichResult();
    }, [])

    const [key, setKey] = useState("");
    const none__= function(){};
    
    (async () => {
        let wal__= await getAccounts();
        setKey(wal__);
    })();
    const go__to__tra= useRef(null);
    const go__to__pnd= useRef(null);
    const go__to__fns= useRef(null);
    function transaction__(){
        go__to__tra.current.click();
    }
    function pending__(){
        go__to__pnd.current.click();
    }
    function finished__(){
        go__to__fns.current.click();
    }

    return (
        <div className="wallet-admin-content">
            <div className="shadowed-box one-line">
                <div className="s-label">
                    Wallet DACMI
                </div>
                <div className="s-value">
                    <div className="value-container">
                        {/* { wallet.key } */}
                        { key }
                    </div>
                </div>
            </div>
            <div className="hyper-things">
                <div className="hyper-title">Transfer to do</div>
                <div className="hyper-value">
                    <div className={ to_do.length> 0 ? "shadowed-box tr-to-do" : "shadowed-box tr-to-do one-thing" } onClick={ transaction__ } title="Transactions en attente de validation. Cliquez pour voir la liste complète" id="waiting">
                        { to_do.map((cli) => (
                            <>
                                <div className="s-label">
                                    { cli.firstname + " " + cli.lastname }
                                </div>
                                <div className="s-value">
                                    <div className="value-container">
                                        { cli.adressMetamask }
                                    </div>
                                </div>
                            </>
                        )) }
                        { ( to_do.length<=0 && req_done ) ? <>La liste est vide.</> : !req_done ? <>Chargement...</> : <></>}
                    </div>
                        <Link className="invisible" to={ '/admin/requests' } ref={ go__to__tra }></Link>
                        <Link className="invisible" to={ '/admin/pending' } ref={ go__to__pnd }></Link>
                        <Link className="invisible" to={ '/admin/finished' } ref={ go__to__fns }></Link>
                </div>
                <div className="hyper-title">Transfer in progress</div>
                <div className="hyper-value">
                    <div className={ in_gress.length> 0 ? "shadowed-box" : "shadowed-box one-thing" } onClick={ in_gress.length> 0 ? pending__ : none__ } title="Transactions en cours. Cliquez pour voir la liste complète" id="pending" >
                        { in_gress.map((cli) => (
                            <>
                                <div className="s-label">
                                    { cli.firstname + cli.lastname }
                                </div>
                                <div className="s-value">
                                    <div className="value-container">
                                        { cli.adressMetamask }
                                    </div>
                                </div>
                            </>
                        )) }
                        { in_gress.length<=0 && <>La liste est vide.</> }
                    </div>
                </div>
                <div className="hyper-title">Validated transfer</div>
                <div className="hyper-value">
                    <div className={ done.length> 0 ? "shadowed-box" : "shadowed-box one-thing" } onClick={ finished__ } title="Transactions terminées. Cliquez pour voir la liste complète" id="finishing" >
                        { done.map((cli) => (
                            <>
                                <div className="s-label">
                                    { cli.firstname + cli.lastname }
                                </div>
                                <div className="s-value">
                                    <div className="value-container">
                                        { cli.adressMetamask }
                                    </div>
                                </div>
                            </>
                        )) }
                        { ( done.length<=0 && done_done) ? <>La liste est vide.</> : !done_done ? <>Chargement...</> : <></> }
                    </div>
                </div>
            </div>

            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
