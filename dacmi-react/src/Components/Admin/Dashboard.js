import React, { useState,useEffect } from 'react';
import './Dashboard.css';
import Header from './Header';
import Customer from './Customer';
import Wallet from './Wallet';
import Dstats from './Dstats';
import Values from './Values';
import Stats from './Stats';
import Sidemenu from './Sidemenu';
// import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { ReactComponent as User} from '../../media/user.svg';
import Loading from '../Loading/Loading';
import CloseButton from '../CloseButton/CloseButton';
import isJpegFile from '../fileExtension';

export default function Dashboard() {
    
    const showModal= useSelector(state => state.modalsReducer);
    // console.log(showModal);

    const [dispUser, setDispUser] = useState(null);
    const [dataReady, setDataReady]= useState(false);//data ready or not ??

    useEffect(() => {
        // console.log(showModal);
        if(showModal.id!== "" && showModal.showSignIn){
            //stuff
            window.fetch(`/afficheuser/${showModal.id}`)
            .then(data => data.json())
            .then(user => {
                setDispUser(user[0]);
                setDataReady(true);
            });
        }
    }, [showModal]);

    // useEffect(() => {
    //     console.log("wadafa---", dataReady);
    // }, [dataReady]);
    const dispatch= useDispatch();

    const closeModal= () => {
        setDataReady(false);
        // console.log(dataReady);
        dispatch({
            type: 'CLOSEMODAL'
        });
    };

    const [activeTab, setActiveTab] = useState(0); //'buy' and 'wallet'
    function setTab(event){
        let data_index= event.target.getAttribute('data-tab');
        setActiveTab(parseInt(data_index));
    }

    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }
    
	// function lastThree(someStr){
	// 	// console.log((someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]));
	// 	return(someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]);
	// }
    return (
        <>
            <div className="my-account">
                <Header title="dashboard" accountType="admin" user="Admin"></Header>
                <div className={ expanded ? "content expanded" : "content" }>
                    <Sidemenu active='dsh' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    { activeTab === 2 ? <Stats></Stats> : <Values></Values>}

                    <div className="tabulable">
                        <div className="tabs">
                            {/* <div className="tab">CURRENT</div> */}
                            <div className={ activeTab === 0 ? 'tab active' : 'tab'} onClick={setTab} data-tab='0'>CUSTOMERS</div>
                            <div className={ activeTab === 1 ? 'tab active' : 'tab'} onClick={setTab} data-tab='1'>WALLET</div>
                            <div className={ activeTab === 2 ? 'tab active' : 'tab'} onClick={setTab} data-tab='2'>DACMI STATS</div>
                        </div>
                        { activeTab === 0 && <Customer></Customer> }
                        { activeTab === 1 && <Wallet></Wallet> }
                        { activeTab === 2 && <Dstats></Dstats> }
                        {/* <Wallet></Wallet> */}
                        {/* <Buy></Buy> */}
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                    <div className={ showModal.showSignIn ? "modal-thing" :"invisible" }>
                        <div className="modal-overlay" onClick={ closeModal }></div>
                        {
                        dataReady ?
                            <div className="modal-content">
                                <div className="modal-title">
                                    {
                                        // lastThree(dispUser.KYC).match(/[Jj][Pp][Gg]/) ?
                                        isJpegFile(dispUser.KYC) ?
                                        // lastThree(dispUser.KYC)=== "jpg" ?
                                        <img src={ '../' + dispUser.KYC } alt="userlogo"  className="imgUser" className='modal-im'/>
                                        :
                                        <User className='modal-im'></User>
                                    }
                                    { dispUser.firstname + " " + dispUser.lastname }
                                </div>
                                <div className="modal-body">
                                    <div className="m-label">id</div>
                                    <div className="m-value">{ showModal.id }</div>
                                    <hr /><hr />
                                    <div className="m-label">email</div>
                                    <div className="m-value">{ dispUser.email }</div>
                                    <hr /><hr />
                                    <div className="m-label">adresse</div>
                                    <div className="m-value">{ dispUser.address }</div>
                                    <hr /><hr />
                                    <div className="m-label">ville</div>
                                    <div className="m-value">{ dispUser.city }</div>
                                    <hr /><hr />
                                    <div className="m-label">Pays</div>
                                    <div className="m-value">{ dispUser.country }</div>
                                    <hr /><hr />
                                    <div className="m-label">nationalité</div>
                                    <div className="m-value">{ dispUser.nationality }</div>
                                    <hr /><hr />
                                    <div className="m-label">abonné(e) newsletter</div>
                                    <div className="m-value">{ dispUser.news ? <>oui</> : <>non</> }</div>
                                    <hr /><hr />
                                    <div className="m-label">rôle</div>
                                    <div className="m-value">{ dispUser.role }</div>
                                    <hr /><hr />
                                    <div className="m-label">code postal</div>
                                    <div className="m-value">{ dispUser.postalCode }</div>
                                    <hr /><hr />
                                    <div className="m-label">Phone</div>
                                    <div className="m-value">{ dispUser.phone }</div>
                                    <hr /><hr />
                                    <div className="m-label">naissance</div>
                                    <div className="m-value">{ dispUser.birthday }</div>
                                </div>
                                <CloseButton onClick={ closeModal } width="2em" height="2em" ></CloseButton>
                            </div>
                        :
                            <div className="modal-content fe">
                                <div className="t">
                                    Récupération des données...
                                </div>
                                <Loading></Loading>
                                <CloseButton onClick={ closeModal } width="2em" height="2em" ></CloseButton>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </>
    );
}
