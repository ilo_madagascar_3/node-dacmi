import React, { useState, useRef, useEffect } from 'react';
import Header from './Header';
import Sidemenu from './Sidemenu';
import './TransactionRequests.css';
import copy from '../../media/copy-w.svg';

import '../thing.css';

export default function TransactionFinished() {
    document.title= "Admin - Transactions terminées";
    const [expanded, setExpanded]= useState(false);

    const [requesters, setrequesters]= useState([]);

    useEffect(() => {
        const affichResult= async () =>{
            const resultats = await window.fetch('/afficheDacmi');
            const result= await resultats.json();
            setrequesters(result);
            console.log(result);
        };
        affichResult();
    },[]);
    // console.log(requesters);

    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    const pop= useRef(null);

    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
            if(pop.current!== null){
                pop.current.classList.remove('invisible');
            }
            setTimeout(() => {
                if(pop.current!== null){
                    pop.current.classList.add('invisible');
                }
            }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
            console.log('Async: Copying to clipboard was successful!');
            if(pop.current!== null){
                pop.current.classList.remove('invisible');
            }
            setTimeout(() => {
                if(pop.current!== null){
                    pop.current.classList.add('invisible');
                }
            }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }

    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }


    return (
        <>
            <div className="my-re-account">
                <Header title="terminés" accountType="admin"></Header>
                <div className={ expanded ? "content expanded" : "content" }>
                    <Sidemenu active='' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <div className="content-of-things">
                        <div className="inner-of-things">
                            <table className="requests">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Metamask</th>
                                        <th>Date</th>
                                        <th>Dacmi</th>
                                        <th>Euro</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {requesters.filter( e => e.statu== true).map((req) => (
                                        <tr
                                          data-name={ req.firstname + " " + req.lastname }
                                          data-key={ req.adressMetamask }
                                          data-euro={ req.valeurEur }
                                          data-dacmi={ req.valeurDacmi }
                                        >
                                            <td>{ req.firstname } { req.lastname } { req.connected ? <span className="green-dot" title="en ligne"></span> : <span className="grey-dot" title="hors ligne"></span> } </td>
                                            <td className="c"><span className="meta">{ req.adressMetamask } <img src={ copy } alt="copy" title="Copy to clipboard" text-content={ req.adressMetamask } onClick={ copyText__to__ } /></span></td>
                                            <td className="c">{ req.date }</td>
                                            <td className="r">{ req.valeurDacmi }</td>
                                            <td className="r">{ req.valeurEur }</td>
                                        </tr>
                                    ))}
                                    { requesters.filter( e => e.statu== true).length == 0 ? <tr><td colSpan="5">L'historique est vide.</td></tr> : <></> }
                                </tbody>
                            </table>
                        </div>
                        <div className="pop-up-text invisible" ref={ pop }>
                            Texte copié dans le presse-papiers.
                        </div>
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                </div>
            </div>
        </>
    )
}
