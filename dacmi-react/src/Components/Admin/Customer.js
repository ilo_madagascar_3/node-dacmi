import React,  { useState, useEffect }  from 'react';
import './Customer.css';
import user from '../../media/user-c.svg';
import download from '../../media/download.png';
import Bar from '../Loading/Bar';

import pdf from '../../media/pdf-des.png';
import jpeg from '../../media/jpeg-des.png';
import pdfcol from '../../media/pdf-col.png';
import jpegcol from '../../media/jpeg-col.png';

import Loading from '../Loading/Loading';

import { useDispatch } from 'react-redux';

// let customers= [
//   {
//     name: "John Do",
//     metakey: "ox2bdfrg2rg5rg2163z32gr65z2gfe3r5g213s5gf3rg135erg435rg3er5g3er5",
//     pdfurl: "url1",
//     jpegurl: "url2"
//   }
// ];


export default function Customer() {

	const dispatch = useDispatch();

	const toggleSignIn= (event) => {
		console.log(event.currentTarget.getAttribute('data-id'));
		dispatch({
			type: "TOGGLEIN",
			id: event.currentTarget.getAttribute('data-id')
		});
	};

	const [customers, setCustomers]= useState([]);

    useEffect(() => {
          const affichUsers= async () =>{
              const userlists = await window.fetch('/afficheAlluser');
              const resultuser= await userlists.json();
              setCustomers(resultuser);
              console.log(resultuser);
			  setListReady(true);
          };
          affichUsers();
      },[]);

	function thing(e){
		console.log(e.currentTarget.getAttribute('data-file'));
	}

	const [listReady, setListReady] = useState(false);

    return (
        <div className="customers-list">
			<div className="list-of">

				{ customers.map(customer => (
					<div className="customer-line" onClick={ toggleSignIn } data-id={ customer._id } key={ customer._id }>
						<div className="customer-info">
							<img src={ user } alt="user" />
							<span className="userSname">
								{ customer.firstname + " " + customer.lastname }
							</span>
							<span className={ ( customer.adressMetamask!= undefined && customer.adressMetamask!= "aucune" && customer.adressMetamask!= "" ) ? "userSkey" : "userSkey b" } >
								<div className="ellipsis" title={ ( customer.adressMetamask!= undefined && customer.adressMetamask!= "aucune" && customer.adressMetamask!= ""  )? "Adresse Metamask" : "Aucune adresse Metamask enregistrée dans la base de données" }>
									{ ( customer.adressMetamask!= undefined && customer.adressMetamask!= "aucune" && customer.adressMetamask!= ""  )? customer.adressMetamask : "- - - -" }
									{/* {
										( customer. )
									} */}
								</div>
							</span>
						</div>
						<div className="downs">
							<span className="down">
								<img src={ download } alt="" />
								<div className="hoverableImg" data-file={ customer.KYC } onClick={ thing }>
									<img src={ pdfcol } alt="" className="floatty"/>
									<img src={ pdf } alt="" />
								</div>
							</span>
							<span className="down">
								<img src={ download } alt="" />
								<div className="hoverableImg">
									<img src={ jpegcol } alt="" className="floatty" />
									<img src={ jpeg } alt="" />
								</div>
							</span>
						</div>
					</div>
				))}
				<div className="end">
					{ (customers.length<=0 && listReady) ? <>Aucun utilisateur n'est enregistré dans la base de données</> :  listReady ? <>--- Fin de liste ---</> : <>
					{/* <Loading></Loading> */}
					<div className="bars">
						<Bar></Bar>
						<Bar></Bar>
						<Bar></Bar>
						<Bar></Bar>
						<Bar></Bar>
					</div>
					</>}
				</div>
			</div>

            <div className="refresher">
                <div className="triangle up"></div>
                <div className="triangle down"></div>
            </div>
        </div>
    )
}
