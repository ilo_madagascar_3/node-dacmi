import React, { useState, useRef,useEffect } from 'react';
import Header from './Header';
import Sidemenu from './Sidemenu';
import './TransactionRequests.css';
import copy from '../../media/copy-w.svg';

import MetaMaskOnboarding from '@metamask/onboarding';
import '../thing.css';
// import { Link } from 'react-router-dom';

import CloseButton from '../CloseButton/CloseButton';

const ONBOARD_TEXT = 'Click here to install MetaMask!';
const CONNECT_TEXT = 'Connecter Metamask';
const CONNECTED_TEXT = 'Votre compte';

export default function TransactionRequests() {
    document.title= "Admin - Transactions en attente";
    const [expanded, setExpanded]= useState(false);
    const [listacheteur, setListacheteur]= useState([]);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    // let requesters=[
    //     {
    //         nom: "Rakoto",
    //         prenom: "Jean",
    //         metamask: "0x5542133413545e546832s546876464768",
    //         date: "10-22-21",
    //         dacmi: "0.31",
    //         euro: "350",
    //         connected: true
    //     }
    // ]
    
    useEffect(() => {
        const affichResult= async () =>{
            const resultats = await window.fetch('/afficheDacmi');
            const result= await resultats.json();
            setListacheteur(result);
            // console.log(result);
        };
        affichResult();
    },[])
    // console.log(listacheteur);
    const pop= useRef(null);

    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
            if(pop.current!== null){
                pop.current.classList.remove('invisible');
            }
            setTimeout(() => {
                if(pop.current!== null){
                    pop.current.classList.add('invisible');
                }
            }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
            console.log('Async: Copying to clipboard was successful!');
            if(pop.current!== null){
                pop.current.classList.remove('invisible');
            }
            setTimeout(() => {
                if(pop.current!== null){
                    pop.current.classList.add('invisible');
                }
            }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }

    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }



    //---E T H   T R A N S A C T I O N S----------------------------------------------------------------------
    const [buttonText, setButtonText] = React.useState(ONBOARD_TEXT);
    const [isDisabled, setDisabled] = React.useState(false);
    const [accounts, setAccounts] = React.useState([]);
    const [compte, setCompte] = React.useState("");
    const [vola, setVola] = React.useState("");
    const [frai, setFrai] = React.useState("");
    const [balance, setBalance]= React.useState(0);
    const onboarding = React.useRef();

    React.useEffect(() => {
        if(MetaMaskOnboarding.isMetaMaskInstalled()){
            if (accounts.length > 0) {
            window.ethereum
            .request({ method: 'eth_getBalance' })
            .then((newBalance) => setBalance(newBalance));
            } else {
            setButtonText(CONNECT_TEXT);
            setDisabled(false);
            }
        } else {
            if (onboarding.current){

                onboarding.current.startOnboarding();
            }
        }
    }, [balance])


    React.useEffect(() => {
        if (!onboarding.current) {
        onboarding.current = new MetaMaskOnboarding();
        }
    }, []);

    React.useEffect(() => {
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
        if (accounts.length > 0) {
            setButtonText(CONNECTED_TEXT);
            setDisabled(true);
            onboarding.current.stopOnboarding();
        } else {
            setButtonText(CONNECT_TEXT);
            setDisabled(false);
        }
        }
    }, [accounts]);

    React.useEffect(() => {
        function handleNewAccounts(newAccounts) {
        setAccounts(newAccounts);
        }
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
        window.ethereum
            .request({ method: 'eth_requestAccounts' })
            .then(handleNewAccounts);
        window.ethereum.on('accountsChanged', handleNewAccounts);
        return () => {
            // window.ethereum.off('accountsChanged', handleNewAccounts);
        };
        }
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
        window.ethereum
            .request({ method: 'eth_request' })
            .then(handleNewAccounts);
        window.ethereum.on('accountsChanged', handleNewAccounts);
        return () => {
            // window.ethereum.off('accountsChanged', handleNewAccounts);
        };
        }
    }, []);

    const onClick = () => {
        if (MetaMaskOnboarding.isMetaMaskInstalled()) {
        window.ethereum
            .request({ method: 'eth_requestAccounts' })
            .then((newAccounts) => setAccounts(newAccounts));
            window.ethereum
            .request({ method: 'getBalance' })
            .then((newBalance) => setBalance(newBalance));
        } else {
        onboarding.current.startOnboarding();
        }
    };
    const sendEthButton = (() => {
        // console.log(receiver);
       let id= receiver.id;
    //    console.log("id",id);
        (async ()=> {
            
            let val={"statu":true};
            // console.log(val);
            await window.fetch(`/modiffier/${id}`,{
                    headers:{
                            "Accept":'application/json',
                            "Content-Type":'application/json'
                            }, 
                    method:'PATCH',
                    body: JSON.stringify(val)    
                })
        })();
        let item=compte;
        let value=vola;
        let gasPrice=frai;
        
        let GWEI = 1000000000;
        let WEI = 1000000000000000000;
        let numberToHexString = (value) => '0x' + parseInt(value).toString(16);

        // console.log(item);
        window.ethereum
        .request({
            method: 'eth_sendTransaction',
            params: [
            {
                from: accounts[0],
                to: item,
                value: numberToHexString(value * WEI),
                gasPrice: numberToHexString(gasPrice * GWEI),
                // gas: numberToHexString(gasLimit),
                //gasPrice: '0x4a817c80000',
                gas: '0xc350',
            },
            ],
        })
        .then((txHash) => console.log(txHash))
        .catch((error) => console.error);
        // console.log(value);
    });

    //--additionnal--
    const initialState={
        name: "",
        metakey: "",
        euro: "",
        dacmi: "",
        id:""
    }

    const [receiver, setReceiver] = useState(initialState);
    const [modalHere, setModalHere] = useState(false);

    function closeModal(){
        setModalHere(false);
        // console.log('heyhey');
    }

    function showModalPay(e){
        initialState.name= e.currentTarget.getAttribute('data-name');
        initialState.metakey= e.currentTarget.getAttribute('data-key');
        setCompte(initialState.metakey);
        initialState.euro= e.currentTarget.getAttribute('data-euro');
        initialState.dacmi= e.currentTarget.getAttribute('data-dacmi');
        initialState.id= e.currentTarget.getAttribute('data-id');
        // console.log("____", e.currentTarget.getAttribute('data-id'), "_____");
        console.log(initialState);
        setVola(initialState.dacmi);
        setReceiver(initialState);
        setModalHere(true);
    }

    //---E T H   E N D----------------------------------------------------------------------------




    return (
        <>
            <div className="my-re-account">
                <Header title="en attente" accountType="admin"></Header>
                <div className={ expanded ? "content expanded" : "content" }>
                    <Sidemenu active='' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <div className="content-of-things">
                        <div className="inner-of-things">
                            <table className="requests">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Metamask</th>
                                        <th>Date</th>
                                        <th>Dacmi</th>
                                        <th>Euro</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {listacheteur.filter( e => e.statu== false).map((req) => (
                                        <tr
                                          onClick={ showModalPay }
                                          data-name={ req.firstname + " " + req.lastname }
                                          data-key={ req.adressMetamask }
                                          data-euro={ req.valeurEur }
                                          data-dacmi={ req.valeurDacmi }
                                          data-id={ req._id }
                                        >
                                            <td>{ req.firstname } { req.lastname } { req.connected ? <span className="green-dot" title="en ligne"></span> : <span className="grey-dot" title="hors ligne"></span> } </td>
                                            <td className="c"><span className="meta">{ req.adressMetamask } <img src={ copy } alt="copy" title="Copy to clipboard" text-content={ req.adressMetamask } onClick={ copyText__to__ } /></span></td>
                                            <td className="c">{ req.date }</td>
                                            <td className="r">{ req.valeurDacmi }</td>
                                            <td className="r">{ req.valeurEur }</td>
                                        </tr>
                                    ))}
                                    { listacheteur.filter( e => e.statu== false).length == 0 ? <tr><td colSpan="5">L'historique des transactions est vide.</td></tr> : <></> }
                                </tbody>
                            </table>
                        </div>
                        <div className="pop-up-text invisible" ref={ pop }>
                            Texte copié dans le presse-papiers.
                        </div>
                    </div>
                    <div className={ modalHere ? "modal-transaction" : "modal-transaction invisible"}>
                        <div className="modal-inner">
                            <h3 className="transactitle">Valider l'achat DACMI de { receiver.name }</h3>
                            <div className="form-transaction">
                                <button disabled={isDisabled} onClick={onClick}>
                                {buttonText} : {accounts}
                                </button>
                                <label htmlFor="">Adresse du destinataire :</label>
                                <input type="text" value={compte} onChange={(e)=>setCompte(e.target.value)} className="form-control" placeholder="Adresse du destinataire"/> 
                                <label htmlFor="">Quantité à transférer :</label>
                                <input type="text" value={vola} onChange={(e)=>setVola(e.target.value)} className="form-control" placeholder="Valeur à transferer"/> 
                                <label htmlFor="">Frais :</label>
                                <input type="text" value={frai} onChange={(e)=>setFrai(e.target.value)} className="form-control" placeholder="Frais"/>
                                <button onClick={sendEthButton}>Transférer Eth</button>
                            </div>
                            <div className="back-to">
                                <p className="stuff">
                                    La quantité d'ETH indiquée sera transferé depuis votre portefeuille Metamask vers le portefeuille correspondant à l'adresse indiquée.
                                </p>
                                <CloseButton onClick={ closeModal } width='1.5em' height='1.5em'></CloseButton>
                            </div>
                        </div>
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                </div>
            </div>
        </>
    )
}
