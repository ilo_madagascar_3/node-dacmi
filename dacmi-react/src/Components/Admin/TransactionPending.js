import React, { useState, useRef } from 'react';
import Header from './Header';
import Sidemenu from './Sidemenu';
import './TransactionRequests.css';
import copy from '../../media/copy-w.svg';

import '../thing.css';

export default function TransactionPending() {
    document.title= "Admin - Transactions en cours";
    const [expanded, setExpanded]= useState(false);
    const expander= () =>{
        setExpanded(!expanded);
    };
    
    const [menuHere, setMenuHere]= useState(false);

    const toggleMenu= function(){
        setMenuHere(!menuHere);
    }

    let requesters=[
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350",
            connected: true
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x545e546832s5468764655421334134768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            metamask: "0x45e546832s54685542133413576464768",
            prenom: "Mahay",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20",
            connected: true
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350"
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20",
            connected: true
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350",
            connected: true
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220",
            connected: true
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350",
            connected: true
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250",
            connected: true
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350"
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350"
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350"
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        },
        {
            nom: "Rakoto",
            prenom: "Jean",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.31",
            euro: "350"
        },
        {
            nom: "Doe",
            prenom: "John",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.23",
            euro: "220"
        },
        {
            nom: "Randria",
            prenom: "Mahay",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.03",
            euro: "20"
        },
        {
            nom: "Kruger",
            prenom: "Max",
            metamask: "0x5542133413545e546832s546876464768",
            date: "10-22-21",
            dacmi: "0.13",
            euro: "250"
        }
    ]

    const pop= useRef(null);

    function fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
      
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
      
        try {
          var successful = document.execCommand('copy');
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Fallback: Copying text command was ' + msg);
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        } catch (err) {
          console.error('Fallback: Oops, unable to copy', err);
        }
      
        document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
        if (!navigator.clipboard) {
          fallbackCopyTextToClipboard(text);
          return;
        }
        navigator.clipboard.writeText(text).then(function() {
          console.log('Async: Copying to clipboard was successful!');
          if(pop.current!== null){
              pop.current.classList.remove('invisible');
          }
          setTimeout(() => {
              if(pop.current!== null){
                  pop.current.classList.add('invisible');
              }
          }, 5000);
        }, function(err) {
          console.error('Async: Could not copy text: ', err);
        });
    }

    const copyText__to__= (e) => {
        e.stopPropagation();
        copyTextToClipboard(e.currentTarget.getAttribute('text-content'));
    }


    return (
        <>
            <div className="my-re-account">
                <Header title="en cours" accountType="admin"></Header>
                <div className={ expanded ? "content expanded" : "content" }>
                    <Sidemenu active='' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                    <div className="content-of-things">
                        <div className="inner-of-things">
                            <table className="requests">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Metamask</th>
                                        <th>Date</th>
                                        <th>Dacmi</th>
                                        <th>Euro</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {requesters.map((req) => (
                                        <tr
                                          data-name={ req.prenom + " " + req.nom }
                                          data-key={ req.metamask }
                                          data-euro={ req.euro }
                                          data-dacmi={ req.dacmi }
                                        >
                                            <td>{ req.prenom } { req.nom } { req.connected ? <span className="green-dot" title="en ligne"></span> : <span className="grey-dot" title="hors ligne"></span> } </td>
                                            <td className="c"><span className="meta">{ req.metamask } <img src={ copy } alt="copy" title="Copy to clipboard" text-content={ req.metamask } onClick={ copyText__to__ } /></span></td>
                                            <td className="c">{ req.date }</td>
                                            <td className="r">{ req.dacmi }</td>
                                            <td className="r">{ req.euro }</td>
                                        </tr>
                                    ))}
                                    { requesters.length == 0 ? <tr><td colSpan="5">L'historique est vide.</td></tr> : <></> }
                                </tbody>
                            </table>
                        </div>
                        <div className="pop-up-text invisible" ref={ pop }>
                            Texte copié dans le presse-papiers.
                        </div>
                    </div>
                    <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                        <div className="hamburger"></div>
                    </div>
                </div>
            </div>
        </>
    )
}
