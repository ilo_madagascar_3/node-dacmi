import React, { useState, useEffect, useRef } from 'react';
import '../Dashboard/Messages.css';
import './Messages.css';
import Header from './Header';
import Sidemenu from './Sidemenu';
import Values from './Values';
import Loading from '../Loading/Loading';
import Bar from '../Loading/Bar';
import CloseButton from '../CloseButton/CloseButton';
import { ReactComponent as User } from '../../media/user-c.svg';
import env from '../../media/envellope-g.svg';

import userB from '../../media/user-b.svg';
import user from '../../media/user.svg';

import {io} from 'socket.io-client';
// import { addMessage } from '../../../../controllers/messageController';
const socket= io();//default to window.location

const userInfo = localStorage.getItem("userInfo");
let resultat = JSON.parse(userInfo);

// if(resultat!==null){
    // console.log(resultat.data._id);
    
    // socket.emit('joinRoom', {username: resultat.data.firstname + " " + resultat.data.lastname, room: '6167b2f23cdc9b21643e7d13'});
    
// }
socket.on('welcome', (msg) => console.log(msg));

export default function Messages() {


    const userInfo = localStorage.getItem("userInfo");
    let resultat = JSON.parse(userInfo);
    let inbox=[];
/**
 * Ny admin afaka misafidy ny olona hiresahany fa ny olona amin'ny admin ihany no afaka miresaka
 */
    const [converser, setConverser] = useState(null);//Ilay olona reasahin'ny admin
    const [converserFetched, setConverserFetched]= useState(false);//hijerena fotsiny hoe efa azo ve ilay user
    const [converser_online, setConverser_online] = useState(false);//raha connecté ilay olona hiresaka na tsia
    // const [conversationFetched, setConversationFetched] = useState(false);

    /**
     * Rehefa mankao amin'ny 'admin/messages' dia ny ecran hita voalohany dia
     *  - liste users (ahafahany misafidy hoe iza no resahany) : UI_type=== 'users'
     * rehefa avy misafidy user resahana izy izay vao tonga any amin'ny
     *  - ecran misy ny message miaraka amin'iny olona iny : UI_type=== 'messages'
     */
    const [UI_type, setUI_type]= useState('users');

    //alaina ny users rehetra
    const [all_u, setAll_u]= useState([]);//izy rehetra
    const [is_all_u, setIs_all_u]= useState(false);//vonona ve sa tsia izy rehetra ?
    useEffect(() => {
        window.fetch('/afficheAlluser')
        .then((response)=> response.json())
        .then((output) => {
            // console.log(output);
            setAll_u(output);
            setIs_all_u(true);//vonona izy rehetra
        });
    }, []);

    /**
     * Messages farany..
     */
    const [latests, setLatests]= useState([]);
    const [gotLatests, setGotLatests]= useState(false);
    useEffect(() => {//rehefa azo ny users rehetra dia alaina ny message nalefa tany farany..
        if(is_all_u){
            let arr=[];
            all_u.forEach((single_user, index)=> {
                window.fetch('/affich_limiteMP/' + single_user._id)
                .then(raw=> raw.json())
                .then((ripe)=> {
                    if(ripe.length>= 1){
                        let data={
                            text: ripe[0].text,
                            read: ripe[0].sender!==single_user._id? true: ripe[0].lu,
                            date: ripe[0].createdAt,
                            userid: single_user._id,
                            ismine: (ripe[0].sender=== resultat.data._id)? true : false
                        }
                        console.log(ripe[0].sender, resultat.data._id);
                        arr.push(data);
                        if(index=== all_u.length - 1){//tonga any amin'ny farany :(
                            setLatests(arr);
                            setGotLatests(true);
                        }
                    } else {
                        let data={
                            text: '',
                            read: true,
                            date: '',
                            userid: single_user._id,
                            ismine: true
                        }
                        arr.push(data);
                        if(index=== all_u.length - 1){//tonga any amin'ny farany :(
                            setLatests(arr);
                            setGotLatests(true);
                        }
                    }
                    // console.log(ripe);
                });
            });
        }
    }, [is_all_u]);

    // useEffect(() => {
    //     if(gotLatests){
    //         console.log("I have em all..");
    //         console.log(latests);
    //     }
    // }, [gotLatests]);

    function findLatestById(id){
        let index= latests.find((el)=> el.userid===id);
        // console.log('em all :' + latests);
        // console.log('em index :');
        // console.log(index);
        if(index=== undefined){
            return('');
        } else {
            return index.text;
            // return 0;
        }
    }
    
    function isRead(id){
        let index= latests.find((el)=> el.userid===id);
        if(index=== undefined){
            return(true);
        } else {
            return(index.read||index.ismine);
            // return 0;
        }
    }

    // rehefa misy clickena => mahazo id
    let the_id;//ho id ho an'izay user voasafidy

    function userClicked(event){
        the_id= event.currentTarget.getAttribute('data-id');
        //Rehefa azo ilay id dia jerena ny user mifanaraka amin'iny
        // setConverser(the_id);
        window.fetch(`/afficheuser/${the_id}`)
        .then(data => data.json())
        .then(user => {
            // console.log(user);
            setConverser(user[0]);
            setConverserFetched(true);
            socket.emit(
                'joinRoom',
                {
                    username: resultat.data.firstname + " " + resultat.data.lastname,
                    room: the_id,
                    id: resultat.data._id
                }
            );
            setUI_type('messages');//mivadika any amin'ilay ecran misy conversation
            //inbox=[
            //     {
            //         type: 'IN',
            //         date: '22-01-21',
            //         content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, facilis. Laborum, eos."
            //     }
            // ];
        });
    }

    function goBack(){//miverina any amin'ilay liste voalohany
        setUI_type('users');
        setRealTime([]);
        // setConversationFetched(false);
        setConverserFetched(false);
        setConverser(null);
        setInboxGathered(false);
    }

    const [expanded, setExpanded]= useState(false);//for the menu
    const expander= () =>{
        setExpanded(!expanded);
    };
    const [menuHere, setMenuHere]= useState(false);//for the menu
    const toggleMenu= function(){//for the menu
        setMenuHere(!menuHere);
    }

    //script for the chat
    const [textToSend, setTextToSend] = useState("");//ilay text ho alefa message
    const [realTime, setRealTime]= useState([]);//asiana an'izay message tonga sy lasa (ankoatran'izay efa any anaty base)

    const changeText= (event) => {//rehefa manoratra eo amin'ny input ilay olona
        setTextToSend(event.currentTarget.value);
    };

    // let endOf= useRef(null);
    function scrollDown(){//rehefa misy message vaovao tonga (na lasa) dia ampidinina
        let view3= document.querySelector('.the-messaging-app');
        // endOf.current.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });//meh..-_-

        view3.scrollTo({
            top: view3.scrollHeight,
            left: 0,
            behavior: 'smooth'
        });
    }

    function gimme(event){//rehefa mandefa an'ilay message
        event.preventDefault();//screw that..
        if(textToSend===""){
            return;
        }
        socket.emit('chatMessage', {
            user: {username: resultat.data.firstname + " " + resultat.data.lastname, admin: true},
            body: textToSend
        });
        let now= new Date();
        let newM= {
            type: 'OUT',
            date: now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes(),
            content: textToSend
        };
        let arr= [...realTime];
        arr.push(newM);
        setRealTime(arr);
        // console.log(arr);
        // console.log(realTime);
        setTextToSend('');

        //Alefa any amin'ny base :
        let the_to__send= {
            conversationId: converser._id,
            sender: resultat.data._id,
            text: textToSend
        };
        // console.log(converser._id);
        // console.log(resultat.data);
        window.fetch('/ajout_message', {
            method: 'POST',
            body:JSON.stringify(the_to__send),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }
        });
        // .then(thing => thing.json());//tokony tsy ilaina
        // // .then(res=> console.log(res));
        scrollDown();
    }
    
    useEffect(() => {
        socket.on('chatMessage', (thing) => {
            // console.log(thing.user + ' says : ' + thing.body);
            // console.log(thing);
            let newM= {
                type: thing.user.admin? 'OUT' : 'IN',
                /**
                 * Possible hoe admin 2 samihafa miaraka miresaka amin'ny olona 1 mitovy 
                 * dia raha admin ihany no nandefa an'ilay izy dia atao 'OUT' fa tsy 'IN'
                 */
                date: thing.date,
                content: thing.body
            };
            let arr= [...realTime];
            arr.push(newM);
            setRealTime(arr);
            scrollDown();
            // console.log(arr);
            // console.log(realTime);
        });
        return () => {
            socket.off('chatMessage');
        }
    }, [realTime]);

    useEffect(() => {
        if(realTime.length> 0){
            let data={
                text: realTime[realTime.length - 1].content,
                read: true,
                date: '',
                userid: converser._id,
                ismine: (realTime[realTime.length - 1].type=== 'IN')? false : true
            };
            let corresponding_index= latests.findIndex(el=> el.userid=== data.userid);
            let arr= [...latests];
            if(corresponding_index>= 0){
                arr[corresponding_index]= data;
            } else {
                arr.append(data);
            }
            setLatests(arr);
        }
    }, [realTime]);

    useEffect(() => {
        socket.on('roomW', (thing) => {
            console.log(thing.msg + " => " + thing.room);
        });
        return () => {
            socket.off('roomW');
        }
    }, []);

    useEffect(() => {
        // console.log(converserFetched);
        if(converserFetched){
            socket.emit('isOnline', converser._id);
            socket.on('online', (id) =>{
                if(id=== converser._id){
                    setConverser_online(true);
                }
            });
            socket.on('offline', id => {
                if(id=== converser._id){
                    setConverser_online(false);
                }
            });
            return () => {
                socket.off('online');
            }
        }
    }, [converserFetched]);
    useEffect(() => {
        socket.on('online', (id) =>{
            console.log('emited from somewhere');
            console.log(converser);
            if(converser){
                if(id=== converser._id){
                    setConverser_online(true);
                    console.log("received");
                }
            }
        });
        socket.on('offline', id => {
            if(converser){
                if(id=== converser._id){
                    setConverser_online(false);
                }
            }
        });
        if(converser_online){
            console.log('he is online');
        } else {
            console.log('he is offline');
        }
        return () => {
            socket.off('online');
            socket.off('offline');
        }
    }, []);

    //messagesStuff

    //let's fill the inbox !!
    const [inboxContent, setInboxContent]= useState([]);//empty first..
    const [inboxGathered, setInboxGathered]= useState(false);//if the inbox content is ready
    useEffect(()=> {//if there is a change on the converser state
        if(converser){//do something only when converser is not null
            // console.log(converser);
            // console.log(converser._id);
            window.fetch('/affich_messagee/' + converser._id, {
                method: 'GET',
                headers:{
                    "Content-Type":'application/json',
                    "Accept":'application/json'
                }
            }).then((result) => result.json())
            .then(output=> {
                // console.log(output);
                // inbox= [
                //     {
                //         type: 'IN',
                //         date: '22-01-21',
                //         content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, facilis. Laborum, eos."
                //     }
                // ];
                let messages= [];
                output.forEach((element) => {
                    let single= {
                        type: element.sender===converser._id? 'IN' : 'OUT',
                        date: element.createdAt,
                        content: element.text,
                        key: element._id
                    }
                    // console.log(single);
                    messages.push(single);
                });
                setInboxContent(messages);
                setInboxGathered(true);

                let req_body={
                    conversationId: converser._id,
                    sender: converser._id
                }
                window.fetch('modiffier_MP',{
                    method: 'PATCH',
                    headers:{
                        "Content-Type":'application/json',
                        "Accept":'application/json'
                    },
                    body: JSON.stringify(req_body)
                })
                .then(response=> {
                    let index= latests.findIndex(el=> el.userid=== converser._id);
                    if(index!= -1){
                        let thing={
                            ...latests[index],
                            read: true
                        };
                        let arr=[...latests];
                        arr[index]= thing;
                        setLatests(arr);
                    }
                });
                scrollDown();
            });
        }
    },[converser]);

	function lastThree(someStr){
		// console.log((someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]));
		return(someStr[someStr.length - 3] + someStr[someStr.length - 2] + someStr[someStr.length - 1]);
	}

    return (
        <>
        <div className="my-account">
            <Header title="Messages" accountType="user"></Header>
            <div className={ expanded ? "content expanded" : "content" }>
                <Sidemenu active='msg' expandIt={ expander } classes={ menuHere ? "set" : "unset" } toggleMenu={ toggleMenu } ></Sidemenu>
                <Values></Values>

                {
                    UI_type=== 'users' ?
                    <>
                    <div className="list-of-users__">
                        <div className="list-users-head_">
                            Utilisateurs inscrits
                        </div>
                        <div className="inner-list-of__">
                            <div className="inner-inner_">
                                {
                                    is_all_u?
                                    <>
                                        {
                                            all_u.map((one_user, index) => (
                                                <div className={ isRead(one_user._id)? "userLine" : "userLine unread" } onClick={ userClicked } data-id={ one_user._id } key={ one_user._id }>
                                                    {/* <User width={ '3em' } height={ '3em' }></User> */}
                                                    {
                                                        lastThree(one_user.KYC).match(/[Jj][Pp][Gg]/) ?
                                                        // lastThree(one_user.KYC)=== "jpg" ?
                                                        <img src={ '../' + one_user.KYC } alt="userlogo"  className="imgUser"/>
                                                        :
                                                        <img src={ userB } alt="userlogo"  className="imgUser"/>
                                                    }
                                                    <div className="username">
                                                        { one_user.firstname + " " + one_user.lastname }
                                                    </div>
                                                    <div className={ isRead(one_user._id)? "latest" : "latest unread" }>
                                                        {
                                                            gotLatests?
                                                            <>{ findLatestById(one_user._id)!==''? findLatestById(one_user._id) : "(Pas de messages)" }</>:
                                                            <><Bar/></>
                                                        }
                                                    </div>
                                                    <img src={ env } className='what' />
                                                </div>
                                            ))
                                        }
                                    </>
                                    :
                                    // <Loading></Loading>
                                    <div className="bars">
                                        <Bar></Bar>
                                        <Bar></Bar>
                                        <Bar></Bar>
                                        <Bar></Bar>
                                        <Bar></Bar>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                    </>
                    :
                    <div className="messages">
                        <CloseButton onClick={ goBack } width="2em" height="2em"></CloseButton>
                        <div className="messages-head">
                            {/* <img src={ user } alt="userlogo" /> */}
                            {
                                lastThree(converser.KYC).match(/[Jj][Pp][Gg]/) ?
                                // lastThree(one_user.KYC)=== "jpg" ?
                                <img src={ '../' + converser.KYC } alt="userlogo"  className="imgUser"/>
                                :
                                <img src={ userB } alt="userlogo"  className="imgUser"/>
                            }
                            <span className="user-name">
                                {
                                    converserFetched?
                                    converser.firstname + " " + converser.lastname
                                    :
                                    'loading username...'
                                }
                                <span className="green-dot"></span>
                            </span>
                        </div>
                        <div className="the-messaging-app">
                            <div className="the-inner">
                                <div className="message-logs">
                                    {
                                        converserFetched?
                                        <>
                                        {
                                            <>
                                                {
                                                    !inboxGathered ?
                                                    <>
                                                    <div className="center-box">
                                                        Chargement des anciens messages...<Loading/>
                                                    </div>
                                                    </>
                                                    :
                                                    (inboxContent.length<=0 && realTime.length<= 0) &&
                                                    <>
                                                    <div className="center-box">
                                                        Vous n'avez pas encore échangé des messages avec cet utilisateur.
                                                    </div>
                                                    </>
                                                }
                                                {
                                                    inboxContent.map(msg => (
                                                        <>
                                                        <div className={ msg.type=== "IN" ? "incoming" : msg.type=== 'OUT' ? "outcoming" : "invisible"} key={ msg.key }>
                                                            <div className="single-body">{msg.content}</div>
                                                            <div className="single-date">{msg.date}</div>
                                                        </div>
                                                    </>
                                                    ))
                                                }
                                            </>
                                        }
                                        {
                                            realTime.map((msg, index) => (
                                            <>
                                                <div className={ msg.type=== "IN" ? "incoming" : msg.type=== 'OUT' ? "outcoming" : "invisible"} key={ 'msg-real-' + index }>
                                                    <div className="single-body">{msg.content}</div>
                                                    <div className="single-date">{msg.date}</div>
                                                </div>
                                            </>
                                            ))
                                        }
                                        </>
                                        :
                                        <>
                                        loading conversation...
                                        </>
                                    }
                                    {/* <div className="endof-msg" ref={ endOf }></div> */}
                                </div>
                            </div>
                        </div>
                        <div className="text-form">
                            <form className="text-to-send" onSubmit={ gimme }>
                                <input type="text" value={ textToSend } onChange={ changeText } autoFocus/>
                                <button type="submit">Envoyer</button>
                            </form>
                        </div>
                    </div>
                }
                <div className={ menuHere ? "menu-activator deactivate" : "menu-activator" } onClick={ toggleMenu }>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                    <div className="hamburger"></div>
                </div>
            </div>
        </div>
        </>
    )
}
