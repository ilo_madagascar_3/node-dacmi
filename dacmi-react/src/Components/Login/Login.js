import React, {useEffect, useRef, useState} from 'react';
import jwt_decode from 'jwt-decode';
import axios from 'axios';
import './Login.css';
import { ReactComponent as User} from '../../media/user-c.svg';
import { ReactComponent as Lock} from '../../media/lock-c.svg';

import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';


export default function Login() {
    let linkToRegister= useRef(null);
    let linkToDashboard= useRef(null);
    let linkToDashboardAdmin= useRef(null);
    let message= useRef(null);
    const[user, setUser]= useState("");
    const dispatch = useDispatch();

    const [messageshow, setmessageshow]= useState(false);
    // console.log("tko",user)
    // const dispatche = useDispatch();

    useEffect(()=>{
        if(user!= ""){
            dispatch({
                type: "TOKENVALID",
                data: user
            });
        }
    }, [user]);

    function addClass(event){
        setmessageshow(false);
    }

    function registerFunc(e){
        linkToRegister.current.click();
    }
    const [email, setEmail]=useState("")
    const [password, setPassword]=useState("")

     async function Clogin(event){
         event.preventDefault();
         
         let item={email,password}
         
         let res  = await window.fetch(`/api/login`,{
            method:'POST',
            body:JSON.stringify(item),
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            }, 
            email,
            password
            });
        const resulta = await res.json();
        setUser(resulta.data);
        //console.log(resulta.data.email);
        if(resulta.status==="ok" && item.email === resulta.data.email){
            localStorage.setItem("userInfo", JSON.stringify(resulta));
            dispatch({
                type: "LOGIN"
            });

            const userInfo = localStorage.getItem("userInfo");
            if(userInfo && resulta.data.role==="admin"){
            linkToDashboardAdmin.current.click();
            }else{
                linkToDashboard.current.click();
            }
        }else{
            message.current.innerHTML= resulta.data.message ;
            setmessageshow(true);
        }
    }
    
  
    // const refreshToken = async () => {
    //     try{
    //         const resu= await axios.post(`/api/refreshtoken`,{token:user.refreshToken});
    //         setUser({
    //             ...user,
    //             accessToken: resu.data.accessToken,
    //             refreshToken: resu.data.refreshToken
    //         });
    //         return resu.data
    //     }catch(err){
    //         console.log(err)
    //     }
    // }
    // const axiosJWT = axios.create();
    // // console.log("rrrr",refreshToken())
    // axiosJWT.interceptors.request.use(async (config)=>{
    //     let currentDate= new Date();
    //     const decodeToken = jwt_decode(user.accessToken);
    //     console.log("rrrr",decodeToken)
    //     if(decodeToken.exp *1000 < currentDate.getTime()){
    //         const data = await refreshToken();
    //         config.headers["authorization"] = "Bearer " +data.accessToken
    //     }
    //     return config;
    // },
    // (error) => {
    //     return Promise.reject(error);
    // });

    return (
        <div className="page-connection container-single">
            <div className="logo-dacmi">
                <img src={ process.env.PUBLIC_URL + '/dacmi-center.png' }/>
            </div>
            <div className="connection-form-container">
                <form action="" className="rel">
                    <div className="label-input" id="username">
                        <div>
                            <label htmlFor='userName'><User width={ '80%' } height={ '80%' }></User></label>
                        </div>
                        <input type="text" name="email" value={email} onChange={(e)=>setEmail(e.target.value)} placeholder="Username" onClick= { addClass }/>
                    </div>
                    <div className="label-input" id="pass">
                        <div>
                            <label htmlFor='password'><Lock width={ '90%' } height={ '90%' }></Lock></label>
                        </div>
                        <input type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} onClick= { addClass }/>
                    </div>
                    <button type='submit' onClick={ Clogin } className='form-button'>Login</button>
                    <button className="form-button" onClick={ registerFunc }>Get registered</button>
                    <Link className='invisible' to='/create-account' ref={ linkToRegister }></Link>
                    <Link className='invisible' to='/dashboard' ref={ linkToDashboard }></Link>
                     <Link className='invisible' to='/admin/dashboard' ref={ linkToDashboardAdmin }></Link>
                    <div className={ messageshow ? "message" : "message invisible" } ref={ message }>
                    </div>
                </form>
            </div>
        </div>
    )
}
