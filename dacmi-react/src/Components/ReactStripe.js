import React, { useState, useRef, useEffect } from "react";
import "./ReactStripe.css";
import Header from "./Header/Header";
import { Link } from "react-router-dom";
import './ReactPayPal.css';
import '../pages/PaypalPage.css';
import Loading from "./Loading/Loading";
import axios from 'axios';
import { getAccounts } from './dmi';
import {showDcmiNumber,DACMI } from './conversion/conversion';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';
import StripeCheckout from "react-stripe-checkout";
require("dotenv").config();

export default function ReactStripe() {
  const [meta__key, setmeta__key]= useState("");
  const [pay_done, setPay_done]= useState(false);
  const [pay_fail, setPay_fail]= useState(false);
  const [is_processing, setIs_processing]= useState(false);
  const [donnemail, setDonnemail]= useState("");
  const [donneid, setDonneid]= useState("");
  const [donnepaye, setDonnepaye]= useState("");


  const [product, setProduct] = useState({
    name: "DACMI",
    price: 0,
    productBy: "DACMI",
  });
  useEffect(async ()=> {
    let key_state= await getAccounts();
    setmeta__key(key_state);
  }, []);
  const infoUser={
    lastname: "",
    firstname: "",
    phone: "",
    adressMetamask:"",
    valeurDacmi:"",
    idacheteur:"",
    email:"",
    idclient:""
  };
  const userInfo = localStorage.getItem("userInfo");
  let resultat = JSON.parse(userInfo);

  if(resultat){
    infoUser.firstname= resultat.data.firstname;
    infoUser.lastname= resultat.data.lastname;
    infoUser.phone= resultat.data.phone;
    infoUser.idacheteur= resultat.data._id;
    infoUser.email= resultat.data.email;
    infoUser.idclient= resultat.data._id;  
  }

  const [buttonState, setButtonState] = useState(0);
  //debut code ajout info
  async function ajouinfo(){
    if(buttonState== 0){
      setButtonState(1);
    let date_jour= new Date();
    function addZeros(n) {
      if (n <= 9) {
        return "0" + n;
      }
      return n
    }
    let formatted_date =  addZeros(date_jour.getDate()) + "-" +  addZeros(date_jour.getMonth() + 1) + "-" + date_jour.getFullYear() + " " +  addZeros(date_jour.getHours()) + ":" +  addZeros(date_jour.getMinutes()) + ":" +  addZeros(date_jour.getSeconds());
     
    let valeurDacmi= showDcmiNumber(transactionValue,9);
    
    let metamask= meta__key[0];
    console.log("Fetched metamask adress : ",metamask);
    // let item={
    //   emailpaypal: mailpay, 
    //   firstname: infoUser.firstname,
    //   lastname: infoUser.lastname,
    //   phone: infoUser.phone,
    //   adressMetamask: metamask,
    //   valeurDacmi: infoUser.valeurDacmi,
    //   idacheteur:infoUser.idacheteur,
    //   email:infoUser.email,
    //   valeurEur: props.valueToPass,  
    // };
    const formData = new FormData();

    formData.append('emailpaypal', donnemail)
    formData.append('firstname', infoUser.firstname)
    formData.append('lastname', infoUser.lastname)
    formData.append('phone', infoUser.phone)
    formData.append('adressMetamask', metamask)
    formData.append('valeurDacmi', valeurDacmi)
    formData.append('idacheteur', infoUser.idacheteur)
    formData.append('email', infoUser.email)
    formData.append('valeurEur', transactionValue)
    formData.append('idtransaction', donneid)
            
    const input = document.querySelector('#logo_');
    const pdf = new jsPDF();
    
    let canvas= await html2canvas(input);
    const imgData = canvas.toDataURL('image/png');
    pdf.addImage(imgData, 'JPEG', -18, 10, 110, 20);

    pdf.autoTable({//client and bill info
        margin: { left: 120 },
        theme: 'grid',
        headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
        bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
        styles: { font: 'monospace' },
        columnStyles: {
            2:{ halign: 'right' }
        },
        body:[
          [["Transaction ID"],[":"],[donneid ]],
          [["Client ID"],[":"],[ infoUser.idclient]],
          [["Nom client"],[":"],[ infoUser.firstname + " " + infoUser.lastname]]
        ]
    });
        
    pdf.autoTable({//DACMI info
      startY: 30,
      margin: { right: 120, left: 20 },
      theme: 'grid',
      headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
      bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
      styles: { font: 'monospace' },
      columnStyles: {
          2:{ halign: 'right' }
      },
      body: [
          ["NEGOCE international UA"],
          ["a.gerno@negoce-int-ua.com"],
          ["+212 707 7109 27"]
      ],
      bodyStyles: {
          cellPadding: 0,
          lineWidth: 0,
          textColor: [0,0,0]
      }
    });
    pdf.autoTable({//the actual bill
      // startY: 40,
      theme: 'grid',
      headStyles: { lineWidth: 0.1, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap', halign: 'center' },
      bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 'wrap', halign: "right" },
      styles: { font: 'monospace' },
      head:[["Valeur DACMI (€)","DACMI demandé","Montant transféré (€)"]],
      body: [
        [DACMI,
        valeurDacmi,
        donnepaye]
      ]
    });

    pdf.autoTable({//bill status
      theme: 'grid',
      headStyles: {
          lineWidth: 0.1,
          lineColor: [0,0,0],
          fillColor: [255,255,255],
          textColor: [0,0,0],
          cellWidth: 40
      },
      bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 40, textColor: [0,0,0] },
      columnStyles: {
          0:{ fontStyle: 'bold' }
      },
      styles: { font: 'monospace' },//doesn't work :(
      body: [
        [["Etat du paiement"],["Effectué"]],
        [["Mode de paiement"],["Stripe"]],
        [["Date de paiement"],[formatted_date]],
      ]
    });
          // pdf.save("download.pdf");
    var file =  pdf.output('blob');
    let blobe="forme";
    formData.append('file', file);

    const res = await axios.post(`/achatDacmi`, formData,{
    headers:{
      'Content-Type': 'multipart/form-data',
      "Accept": "application/ld+json",
      "type": "formData"
      }
    })
    const result = await res.data;
    console.warn("Data successfully sent, results :", result);
    setButtonState(2);
    }
  };
  //fin code

  const enCours= function(){
    setIs_processing(true);
  }

  const makePayment = (token) => {
    // setIs_processing(true);
    // this token is auto received when you write token as key
   
    const body = {
      token,
      product
    };
    // console.log(product.price);
    const headers = {
      "Content-Type": "application/json",
    };
    setDonnemail(body.token.email);
    setDonneid(body.token.created);
    setDonnepaye(body.product.price)
     let request = fetch("/payment", {
      method: "POST",
      headers,
      body: JSON.stringify(body),
    })
    .then((response) => {
      // console.log("RESPONSE ", response);
      const { status } = response;
      if(status === 200){
        setIs_processing(false);
        setPay_done(true);
      }
      // console.log("STATUS ", status);
      // console.log("STATUS ", response);
    })
    .catch((err) => {
      console.log(err);
      setIs_processing(false);
      setPay_fail(true);
    });
     return request;
  };

  const value= useRef(null);

  const [transactionValue, setTransactionValue] = useState(0);
  
  //trimmer -----------
  function removeZeroFrom__(someStringValue){
    let len= someStringValue.length;
    while(someStringValue[0]== '0' && someStringValue[1] != '.'){
      someStringValue= someStringValue.slice(1);
    }
    return(someStringValue);//new
  }
  //trimmer END -------

  useEffect(() => {
    let product_={
      name: 'DACMI',
      price: removeZeroFrom__(transactionValue),
      productBy: 'DACMI'
    };
    setProduct(product_);
    // console.log(product);
  }, [transactionValue])

  const setValue= function(event){
    setTransactionValue(event.currentTarget.value);
    // // console.log(value.value);
    // // console.log(value.value);
    // console.log(transactionValue);
  }


  function reload(){
    window.location.reload();
  }

  return (
    <>
      <div className="Page">
        <Header title="Acheter DACMI" accountType="user" title="Acheter DACMI"></Header>
        <header className="Page-header">
          {
            (pay_done && !is_processing) ?
            <div className="payment-div">
              <div>
                <div className="mid-thing">
                  <h3>
                    Le paiement a été effectué avec succès !
                  </h3>
                  <p className="success-explain">
                    Vous venez de payer { product.price }€ pour l'achat de DACMI. Vous pouvez maintenant envoyer votre requête pour que la quantité de DACMI équivalente soit transférée dans votre portefeuille Metamask.
                  </p>
                </div><br/>
                <button onClick={ajouinfo} className={ buttonState== 0? "buttonActive" : buttonState== 1? "buttonWait" : "buttonDone" } >
                  { buttonState== 0 && <>Envoyer la requête</> }
                  { buttonState== 1 && <Loading></Loading> }
                  { buttonState== 2 && <>Requête envoyée</> }
                </button>
                { buttonState== 2 &&
                <>
                  <p>
                    L'historique de vos transaction est visible dans la section Wallet de votre
                    <Link to={'/dashboard'} >
                      Tableau de bord.
                    </Link>
                  </p>
                </>
                }
              </div>
            </div>
            :
            (!is_processing)?
            <>
            <div>
              <h3>Acheter Dacmi via Stripe</h3>
              <div className="form-holder">
                <div className="for-the-inp">
                  <input type="number" ref={value} className="payInp" placeholder="Votre prix" onInput={ setValue } value={ transactionValue }/><span className="unit">€</span>
                </div>
                <StripeCheckout
                  stripeKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}
                  token={makePayment}
                  name="Buy Dacmi"
                  price={product.price * 100}
                  shippingAddress
                  billingAddress
                >
                  <button onClick={ enCours }>
                    {/* Acheter {product.price} € de DACMI */}
                    Proceder au paiement
                  </button>
                </StripeCheckout>
              </div>
            </div>
            <div className="back-to">
              <p className="stuff">
                Une fois la transaction terminée, un de nos administrateurs va transférer l'équivalent de { removeZeroFrom__(transactionValue) }€ (moins les frais de transfert) dans votre portefeuille Metamask.
              </p>
              <Link to={ '/dashboard' }>Retourner au tableau de bord.</Link>
            </div>
            </>
            :
            <>
              <div className="stuff-in_pro">
                En attente d'une réponse de Stripe...
              </div>
              <Loading></Loading>
            </>
          }
          {
            pay_fail &&
            <div className="not-connected">
              Une erreur est survenue.
              <button className="centerL" onClick={ reload }>Réessayer</button>
            </div>
          }
        </header>
      </div>
    </>
  );

}