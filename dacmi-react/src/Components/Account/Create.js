import React, { useState,useRef} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './Create.css';

import Enveloppe from '../../media/circle-enveloppe.png';
import Lock from '../../media/circle-lock.png';
import Lockre from '../../media/circle-lock-re.png';
import Phone from '../../media/circle-phone.png';
import User from '../../media/circle-user.png';

export default function Create() {
    let linkToComplet= useRef(null);
    let message= useRef(null);

    const [firstname, setFirstname]=useState("")
    const [lastname, setLastname]=useState("")
    const [email, setEmail]=useState("")
    const [password, setPassword]=useState("")
    const [passwordre,setPasswordre]=useState("")
    const [phone, setPhone]=useState("")
    const [remember, setRemember]=useState(false)
    const [news, setNews]=useState(false)
    const [messageshow, setmessageshow]= useState(false);

    
    function removeRed(event){
        event.currentTarget.classList.remove('redish');
        setmessageshow(false);
    }

   const handleChange = (value) => {
     return !value;
  };
  
    async function ajout(event){
        event.preventDefault();
        const emailRegex = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

        let item={firstname,lastname,email,password,passwordre,phone,news,remember}
       /// console.warn(item.email)
        if((item.firstname!=="") && (item.lastname!=="") && (item.email!=="") &&(item.password!=="") && (item.passwordre!=="") &&(item.phone!=="")){
          
            let res  = await axios.post(`api/check-mail`,{
                method:'POST',
                body:JSON.stringify(item.email),
                headers:{
                    "Content-Type":'application/json',
                    "Accept":'application/json'
                }, 
                email
            })
            const resulta=await res.data 
            console.warn("valiny",resulta.status) 
          
            if(emailRegex.test(item.email)){
                if(resulta.status==="ok"){
                    if (item.password === passwordre) {
                        console.warn(item)
                        localStorage.setItem("toComplete", JSON.stringify(item))
                    
                        const complete = localStorage.getItem("toComplete");
                        if(complete){
                        linkToComplet.current.click();
                        }
                    }else{
                        document.querySelector("input[name='passwordre']").classList.add('redish'); 
                    }
                }else{
                     message.current.innerHTML= resulta.message ;
                     setmessageshow(true);
                     document.querySelector("input[name='email']").classList.add('redish');
                }  
            }else{
                 document.querySelector("input[name='email']").classList.add('redish');
            } 
    } else{
        console.warn("Remplir votre champ");
        if(item.firstname== ""){
            document.querySelector("input[name='firstname']").classList.add('redish');
        }
        if(item.lastname== ""){
            document.querySelector("input[name='lastname']").classList.add('redish');
        }
        if(item.email== ""){
            document.querySelector("input[name='email']").classList.add('redish');
        }
        if(item.password== ""){
            document.querySelector("input[name='password']").classList.add('redish');
        }
        if(item.passwordre== ""){
            document.querySelector("input[name='passwordre']").classList.add('redish');
        }
        if(item.phone== ""){
            document.querySelector("input[name='phone']").classList.add('redish');
        }
    }
    }
    return (
        <div className="creation-compte">
            <div className="head-banner">
                <img src={ process.env.PUBLIC_URL + '/header-banner.jpg' }/>
            </div>
            <div className="modal-form">
                <div className="modal-content">
                    <h1 className="form-title">Create New Account</h1>
                    <form action="" className="rel">
                        <div className="form-line">
                            <div className="label-input half">
                                <div className="label">
                                    <label htmlFor="first-name">
                                        <img src={ User } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="text" name="firstname" value={firstname} onChange={(e)=>setFirstname(e.target.value)} placeholder="First Name" onClick= { removeRed }/>
                            </div>
                            <div className="spacer"></div>
                            <div className="input half">
                                <input type="text" name="lastname" value={lastname} onChange={(e)=>setLastname(e.target.value)} placeholder="Last Name" onClick= { removeRed }/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label htmlFor="email">
                                        <img src={ Enveloppe } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="email" name="email" value={email} onChange={(e)=>setEmail(e.target.value)} placeholder="Email" onClick= { removeRed }/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label htmlFor="password">
                                        <img src={ Lock } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} placeholder="Password" onClick= { removeRed }/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label htmlFor="password-re">
                                        <img src={ Lockre } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="password" name="passwordre" value={passwordre} onChange={(e)=>setPasswordre(e.target.value)} placeholder="Confirm Password" onClick= { removeRed }/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="label-input">
                                <div className="label">
                                    <label htmlFor="phone">
                                        <img src={ Phone } alt="" width={'60%'}/>
                                    </label>
                                </div>
                                <input type="text" name="phone" value={phone} onChange={(e)=>setPhone(e.target.value)} placeholder="Phone Number" onClick= { removeRed }/>
                            </div>
                        </div>

                        <div className="form-line">
                            <div className="half">
                                <input type="checkbox" checked={remember} onChange={() => setRemember(handleChange)} name="remember"/>
                                <label htmlFor="remember">Remember me</label>
                            </div>
                            <div className="half">
                                <input type="checkbox" checked={news} onChange={() => setNews(handleChange)} name="news"/>
                                <label htmlFor="news">Subscribe to Newsletter</label>
                            </div>
                        </div>

                        <button onClick={ajout} className="form-button" type="submit">Register</button>
                        <Link className='invisible' to='/complete-account' ref={ linkToComplet }></Link>
                        <div className={ messageshow ? "message" : "message invisible" } ref={ message }></div>
                    </form>
                </div>
                <img src={ process.env.PUBLIC_URL + '/geometry.jpg' } alt="geom" className='bottom-im'/>
            </div>
            
        </div>
    )
}
