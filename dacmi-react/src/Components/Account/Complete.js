import React, { useRef, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './Complete.css';

import Calendar from '../../media/circle-calendar.png';
import Nation from '../../media/circle-nation.png';
import Locate from '../../media/circle-locate.png';
import House from '../../media/circle-house.png';
import World from '../../media/circle-world.png';
import Upload from '../../media/upload.png';
import calendar from '../../media/calendar-white.png';


const isLeap= function(year){
    
    if(year % 4 == 0){
        if(year % 100 == 0){
            if(year % 400 == 0){
                return true;
            } else {
                return false
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function number_days_in_month(month, year){
    if([1, 3, 5, 7, 8, 10, 12].includes(month)){
        return 31;
    } else {
        if(month != 2){
            return 30;
        } else {
            if(isLeap(year)){
                return 29;
            } else {
                return 28;
            }
        }
    }
}

export default function Complete() {
    let linkToLogin= useRef(null);
    const [day, setDay]=useState("")
    const [month, setMonth]=useState("")
    const [year, setYear]=useState("")
    const [nationalite, setNationalite]=useState("")
    const [address, setAddress]=useState("")
    const [city, setCity]=useState("")
    const [postalCode,setPostalCode]=useState("")
    const [country, setCountry]=useState("")
    const [file, setFile]=useState("")
    const [filename, setFilename]=useState("Choose File");
    const [uploadedFile, setUploadedFile]=useState({});
    
    let select_country= useRef(null);
    const getData=()=>{
        fetch('country.json'
        ,{
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           }
        }
        )
          .then(function(response){
            return response.json();
          })
          .then(function(countries) {
            for(let i=0; i< countries.length; i++){
                let el= document.createElement('option');
                el.innerHTML= countries[i].name;
                el.setAttribute('nom',countries[i].name);
                el.setAttribute('value', countries[i].alpha3Code);
                el.setAttribute('continent', countries[i].region);
                el.setAttribute('subregion', countries[i].subregion);
                select_country.current.appendChild(el);
                   
            }
        })
      }
      useEffect(()=>{
          getData();
      },[]);

      const [countryRefused, setCountryRefused] = useState(false);

      function refuseCountry(e){
          let list__of__denied= ['DZA', 'MAR'];
          if (list__of__denied.includes(select_country.current.value)){
              setCountryRefused(true);
          } else {
              if(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute('continent') == "Asia"){
                  setCountryRefused(true);
              }else{
                  if(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute('continent') == "Americas" && e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute('subregion') != 	"Northern America" ){
                      setCountryRefused(true);
                  } else {
                      setCountryRefused(false);
                      setCountry(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute('nom'));   
                  }
              }
          }
      }
    // for datepickerthing------------------------------------------------

    let dd= useRef(null);
    let mm= useRef(null);
    let yy= useRef(null);
    let dpk= useRef(null);

    const birth__initial={
        day: 1,
        month: 1,
        year: 1600,
        day_max: 31
    }
    
    const [birthDate, setBirthDate] = useState(birth__initial);

    useEffect(() => {
        console.log(birthDate);
    }, [birthDate]);

    const updateDate=function(event){
        let the= event.target;
        let new_date= {...birthDate};
        if(the.name == "day"){
            if(the.value > new_date.day_max){
                the.value= new_date.day_max;
                new_date.day= new_date.day_max;
            } else {
                if(the.value < 1){
                    the.value= 1;
                    new_date.day= 1;
                } else {
                     if(the.value < 10){
                         the.value= "0" + the.value;
                       new_date.day= the.value;
                    } else {
                      new_date.day= the.value;
                    }
                }
            }
            mm.current.value= mm.current.value != "" ? new_date.month : "" ;
            yy.current.value= yy.current.value != "" ? new_date.year : "" ;

            dpk.current.value= [toString(new_date.year), toString(new_date.month), toString(new_date.day)].join("-");
            setBirthDate(new_date);
        }
        if(the.name == "month"){
            if(the.value > 12){
                the.value= 12;
                new_date.month= 12;
            } else {
                if(the.value < 1){
                    the.value= 1;
                    new_date.month= 1;
                } else {
                    if(the.value < 10){
                         the.value= "0" + the.value;
                       new_date.month= the.value;
                    } else {
                      new_date.month= the.value;
                    }
                }
            }
            new_date.day_max= number_days_in_month(new_date.month, new_date.year);
            if(new_date.day > number_days_in_month(new_date.month, new_date.year)){
                new_date.day= number_days_in_month(new_date.month, new_date.year);
                new_date.day_max= number_days_in_month(new_date.month, new_date.year);
            }
            dd.current.value= dd.current.value != "" ? new_date.day : "" ;
            yy.current.value= yy.current.value != "" ? new_date.year : "" ;
            
            dpk.current.value= [toString(new_date.year), toString(new_date.month), toString(new_date.day)].join("-");
            setBirthDate(new_date);
        }
        if(the.name == "year"){
            let today= new Date();
            let currentYear= today.getFullYear();
            if(the.value > currentYear){
                the.value= currentYear;
                new_date.year= currentYear;
            } else {
                if(the.value < 1){
                    the.value= 1;
                    new_date.year= 1600;
                } else {
                    new_date.year= the.value;
                }
            }
            new_date.day_max= number_days_in_month(new_date.month, new_date.year);
            if(new_date.day > number_days_in_month(new_date.month, new_date.year)){
                new_date.day= number_days_in_month(new_date.month, new_date.year);
                new_date.day_max= number_days_in_month(new_date.month, new_date.year);
            }
            
            dd.current.value= dd.current.value != "" ? new_date.day : "" ;
            mm.current.value= mm.current.value != "" ? new_date.month : "" ;
            
            yy.current.value= new_date.year;
            dpk.current.value= [toString(new_date.year), toString(new_date.month), toString(new_date.day)].join("-");

            setBirthDate(new_date);
        }
        if(the.name == "datepicker"){
            let values= the.value.split("-");
            new_date.day= values[2];
            new_date.year= values[0];
            new_date.month= values[1];
            new_date.day_max= number_days_in_month(values[1], values[0]);

            dd.current.value= new_date.day;
            mm.current.value= new_date.month;
            yy.current.value= new_date.year;
            setBirthDate(new_date);
        }
    }


    // for datepickerthing  E N D ----------------------------------------

    let uploadLogo= useRef(null);
    function triggerUpload(){
        uploadLogo.current.click();
    }

    const onChange = e => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    }

    async function ajoutComplete(e){
        e.preventDefault();
        
        let item={day:birthDate.day,month:birthDate.month,year:birthDate.year,nationalite,address,city,postalCode,country}
         if((item.day!=="") && (item.month!=="") && (item.year!=="") &&(item.nationalite!=="") && (item.address!=="") &&(item.city!=="") &&(item.postalCode!=="") &&(item.country!=="")){
            const complete = localStorage.getItem("toComplete");
            let resultat = JSON.parse(complete)
            let info={...resultat, ...item}
            console.log(info)
            const formData = new FormData();
            
            formData.append('firstname', info.firstname)
            formData.append('lastname', info.lastname)
            formData.append('email', info.email)
            formData.append('password', info.password)
            formData.append('passwordre', info.passwordre)
            formData.append('phone', info.phone)
            formData.append('remember', info.remember)
            formData.append('news', info.news)
            formData.append('day', info.day)
            formData.append('month', info.month)
            formData.append('year', info.year)
            formData.append('nationalite', info.nationalite)
            formData.append('address', info.address)
            formData.append('city', info.city)
            formData.append('postalCode', info.postalCode)
            formData.append('country', info.country)
            formData.append('file', file)
        
            console.error("valiny.........");
        
            console.log('info image', formData.getAll('file'))
            console.log('info inscription',formData.getAll('email'))

             const res = await axios.post(`/api/register`, formData,{
                 headers:{
                     'Content-Type': 'multipart/form-data',
                     "Accept": "application/ld+json",
                     "type": "formData"
                 }
             })
             const resultats = await res.data
             console.log(resultats);
        
        //     console.error("valiny.........",result)
            const {fileName, filePath} = res.data
            setUploadedFile({fileName, filePath})
             console.log(res.data);

              if(res.data.data.status === "ok"){
                  linkToLogin.current.click();
                  localStorage.removeItem('toComplete'); 
              }else{
                 console.error("tsy lasa");
              }  
        }
    }

    return (
	<div className="completion-compte">
        <div className="head-banner">
                <img src={ process.env.PUBLIC_URL + '/header-banner.jpg' }/>
        </div>
        <div className="modal-form">
            <div className="modal-content">
                <h1 className="form-title">Complete your Account</h1>
                <form action=""  >
                    <div className="form-grid" id="dater">
                        <div className="label">
                            <img src={ Calendar } alt="" width={'20%'}/>
                            <label htmlFor="naissance">
                                Naissance
                            </label>
                        </div>
                        <div className="input" name="naissance">
                            <input type="number" name="day" placeholder="dd" onChange={ updateDate } min="1" max={ birthDate.day_max } ref={ dd }/>
                            <div className="spacer"></div>
                            <input type="number" name="month" placeholder="mm" onChange={ updateDate } min="1" max="12" ref={ mm }/>
                            <div className="spacer"></div>
                            <input type="number" name="year" placeholder="yyyy" onChange={ updateDate } min="0" max="9999" ref={ yy }/>
                        </div>
                        <div className="input-date">
                            <label id='dpk'>
                                <input type="date" id="dpk" ref={ dpk } onChange={ updateDate } name="datepicker" />
                                <button id="calendar_text"><img src={ calendar } alt="" /></button>
                            </label>
                            </div>
                    </div>

                    <div className="form-grid">
                        <div className="label">
                            <img src={ Nation } alt="" width={'20%'}/>
                            <label htmlFor="nationalite">
                                Nationalité
                            </label>
                        </div>
                        <div className="input">
                            <input type="text" name="nationalite" value={nationalite} onChange={(e)=>setNationalite(e.target.value)}/>

                        </div>
                    </div>

                    <div className="form-grid">
                        <div className="label">
                            <img src={ Locate } alt="" width={'20%'}/>
                            <label htmlFor="address">
                                Address
                            </label>
                        </div>
                        <div className="input">
                            <input type="text" name="address"value={address} onChange={(e)=>setAddress(e.target.value)}/>
                        </div>
                    </div>

                    <div className="form-grid">
                        <div className="label">
                            <img src={ House } alt="" width={'20%'}/>
                            <label htmlFor="city">
                                City
                            </label>
                        </div>
                        <div className="input" name="city">
                            <input type="text" name="city"value={city} onChange={(e)=>setCity(e.target.value)}/>
                            <div className="spacer"></div>
                            <input type="text" name="postalCode" value={postalCode} onChange={(e)=>setPostalCode(e.target.value)} placeholder="postalCode"/>
                        </div>
                    </div>

                    <div className="form-grid">
                        <div className="label">
                            <img src={ World } alt="" width={'20%'}/>
                            <label htmlFor="country">
                                Country
                            </label>
                        </div>
                        <div className="input">
                            <select name="country" ref={ select_country } onChange={ refuseCountry } defaultValue="">
                                <option disabled value=""></option>
                            </select>
                        </div>
                    </div>

                    <div className="label-input uploader">
                        <div className="label">
                            <label onClick= {triggerUpload} >
                            <img src={ Upload } alt="" width={'50%'}/>
                            </label>
                        </div>
                        <div className="input">
                            <label htmlFor="file-upload" className='label-upload' ref={ uploadLogo }>Upload your DOC,<br/> PDF or JPEG</label>
                            <input type="file" name="file" onChange={onChange} id='file-upload'/>
                        </div>
                    </div>

                    <button className={ countryRefused ? "form-button refused" : "form-button" } onClick={ajoutComplete} >Register</button>
                    <Link className='invisible' to='/login' ref={ linkToLogin }></Link>
                    <div className={ countryRefused ? "denied" : "denied invisible" }>
                        Nous n'acceptons pas les clients de votre région.<br/>
                        Veuillez contacter le service client
                    </div>
                </form>
            </div>
                <img src={ process.env.PUBLIC_URL + '/geometry.jpg' } alt="geom" className='bottom-im'/>
        </div>        
    </div>
    )
}