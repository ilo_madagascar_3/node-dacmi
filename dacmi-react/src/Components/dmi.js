import MetaMaskOnboarding from '@metamask/onboarding';
import React from 'react';
import './thing.css';
import Header from './Admin/Header';
import { Link } from 'react-router-dom';

const ONBOARD_TEXT = 'Click here to install MetaMask!';
const CONNECT_TEXT = 'Connecter Metamask';
const CONNECTED_TEXT = 'Votre compte';

const OnboardingButton= () => {
  const [buttonText, setButtonText] = React.useState(ONBOARD_TEXT);
  const [isDisabled, setDisabled] = React.useState(false);
  const [accounts, setAccounts] = React.useState([]);
  const [compte, setCompte] = React.useState("");
  const [vola, setVola] = React.useState("");
  const [frai, setFrai] = React.useState("");
  const [balance, setBalance]= React.useState(0);
  const onboarding = React.useRef();

  React.useEffect(() => {
    if(MetaMaskOnboarding.isMetaMaskInstalled()){
        if (accounts.length > 0) {
        window.ethereum
          .request({ method: 'eth_getBalance' })
          .then((newBalance) => setBalance(newBalance));
        } else {
          setButtonText(CONNECT_TEXT);
          setDisabled(false);
        }
    } else {
      onboarding.current.startOnboarding();
    }
  }, [balance])
  

  React.useEffect(() => {
    if (!onboarding.current) {
      onboarding.current = new MetaMaskOnboarding();
    }
  }, []);

  React.useEffect(() => {
    if (MetaMaskOnboarding.isMetaMaskInstalled()) {
      if (accounts.length > 0) {
        setButtonText(CONNECTED_TEXT);
        setDisabled(true);
        onboarding.current.stopOnboarding();
      } else {
        setButtonText(CONNECT_TEXT);
        setDisabled(false);
      }
    }
  }, [accounts]);

  React.useEffect(() => {
    function handleNewAccounts(newAccounts) {
      setAccounts(newAccounts);
    }
    if (MetaMaskOnboarding.isMetaMaskInstalled()) {
      window.ethereum
        .request({ method: 'eth_requestAccounts' })
        .then(handleNewAccounts);
      window.ethereum.on('accountsChanged', handleNewAccounts);
      return () => {
        // window.ethereum.off('accountsChanged', handleNewAccounts);
      };
    }
     if (MetaMaskOnboarding.isMetaMaskInstalled()) {
      window.ethereum
        .request({ method: 'eth_request' })
        .then(handleNewAccounts);
      window.ethereum.on('accountsChanged', handleNewAccounts);
      return () => {
        // window.ethereum.off('accountsChanged', handleNewAccounts);
      };
    }
  }, []);

  const onClick = () => {
    if (MetaMaskOnboarding.isMetaMaskInstalled()) {
      window.ethereum
        .request({ method: 'eth_requestAccounts' })
        .then((newAccounts) => setAccounts(newAccounts));
        window.ethereum
        .request({ method: 'eth_getBalance' })
        .then((newBalance) => setBalance(newBalance));
    } else {
      onboarding.current.startOnboarding();
    }
  };

//Sending Ethereum to an address
  const sendEthButton = () => {
    let item=compte;
    let value=vola;
    let gasPrice=frai;
    
    let GWEI = 1000000000;
    let WEI = 1000000000000000000;
    let numberToHexString = (value) => '0x' + parseInt(value).toString(16);

    console.log(item);
    window.ethereum
      .request({
        method: 'eth_sendTransaction',
        params: [
          {
            from: accounts[0],
            to: item,
            value: numberToHexString(value * WEI),
            gasPrice: numberToHexString(gasPrice * GWEI),
            // gas: numberToHexString(gasLimit),
            //gasPrice: '0x4a817c80000',
            gas: '0xc350',
          },
        ],
      })
      .then((txHash) => console.log(txHash))
      .catch((error) => console.error);
      console.log(value);
  };

  return (
    <>
    <Header title="Transfert"></Header>
    <div className="thing">

        <button disabled={isDisabled} onClick={onClick}>
        {buttonText} : {accounts}
        </button>
        <input type="text" value={compte} onChange={(e)=>setCompte(e.target.value)} className="form-control" placeholder="Adresse du destinataire"/> 
        <input type="text" value={vola} onChange={(e)=>setVola(e.target.value)} className="form-control" placeholder="Valeur à transferer"/> 
        <input type="text" value={frai} onChange={(e)=>setFrai(e.target.value)} className="form-control" placeholder="Frais"/>
        <button onClick={sendEthButton}>Send Eth</button>
    </div>
    <div className="back-to">
      <p className="stuff">
        La quantité d'ETH indiquée sera transferé depuis votre portefeuille Metamask vers le portefeuille correspondant à l'adresse indiquée.
      </p>
      <Link to={ '/admin/dashboard' }>Retourner au tableau de bord.</Link>
    </div>
    </>
    
  );
}

export function getAccounts(){
  let accounts= null;
  if (MetaMaskOnboarding.isMetaMaskInstalled()) {
    return window.ethereum.request({ method: 'eth_requestAccounts' }).then((output) => {return output;});
  }
}
export async function getBalace(){
  let accounts= await getAccounts();
  console.log(accounts);
  if (MetaMaskOnboarding.isMetaMaskInstalled()) {
    let params=[accounts[0], 'latest'];
    return window.ethereum.request({ method: 'eth_getBalance', params }).then((output) => {console.log(output); return output;});
  }
}

export default OnboardingButton;

