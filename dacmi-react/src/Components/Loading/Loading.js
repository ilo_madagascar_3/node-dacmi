import React from 'react';
import './Loading.css';

export default function Loading() {
    return (
        <div className="text-loading">
            <div className="loading-screen">
                <div className="o">
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                <div className="i">
                    <div className="r"></div>
                </div>
                </div>
            </div>
        </div>
    )
}
