import { PromiseProvider } from "mongoose";
import React, { useEffect, useState } from "react";
import axios from 'axios';
import { getAccounts } from './dmi';
import {showDcmiNumber,DACMI } from './conversion/conversion';
import './ReactPayPal.css';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable';
import { Link } from "react-router-dom";
import Loading from "./Loading/Loading";

export default function ReactPayPal(props) {
  const [meta__key, setmeta__key]= useState("");

  const [paid, setPaid] = React.useState(0);
  const [error, setError] = React.useState(null);
  const [adressMetamask, setAdressMetamask] = React.useState("");
  const [valeurDacmi, setValeurDacmi] = React.useState("");
  let checking__=false;

  useEffect(async ()=> {
    let key_state= await getAccounts();
    setmeta__key(key_state);
  }, []);

  const infoUser={
    lastname: "",
    firstname: "",
    phone: "",
    adressMetamask:"",
    valeurDacmi:"",
    idacheteur:"",
    email:"",
    idclient:""
  };
  
  const userInfo = localStorage.getItem("userInfo");
  let resultat = JSON.parse(userInfo);
   console.log("__vvv__",resultat);
  
  if(resultat){
    infoUser.firstname= resultat.data.firstname;
    infoUser.lastname= resultat.data.lastname;
    infoUser.phone= resultat.data.phone;
    infoUser.idacheteur= resultat.data._id;
    infoUser.email= resultat.data.email;
    infoUser.idclient= resultat.data._id;  
  }
  
  /////////////////

  const [buttonState, setButtonState] = useState(0);
  /*
  *  0 is active (not clicked yet)
  *  1 is inactive (clicked but action note finished)
  *  2 is inactive
  */

//   let date_jour=  data.date;
//     function addZeros(n) {
//       if (n <= 9) {
//         return "0" + n;
//       }
//       return n
//     }
//     let formatted_date =  addZeros(date_jour.getDate()) + "-" +  addZeros(date_jour.getMonth() + 1) + "-" + date_jour.getFullYear() + " " +  addZeros(date_jour.getHours()) + ":" +  addZeros(date_jour.getMinutes()) + ":" +  addZeros(date_jour.getSeconds());
// console.log("---hhhh--",formatted_date);
   let data={
        dacmi: 5.5,
        one_dacmi: 3,// one DACMI costs 3€
    }

  async function ajouinfo(){
    if(buttonState== 0){
      setButtonState(1);
    let date_jour= new Date();
    function addZeros(n) {
      if (n <= 9) {
        return "0" + n;
      }
      return n
    }
    let formatted_date =  addZeros(date_jour.getDate()) + "-" +  addZeros(date_jour.getMonth() + 1) + "-" + date_jour.getFullYear() + " " +  addZeros(date_jour.getHours()) + ":" +  addZeros(date_jour.getMinutes()) + ":" +  addZeros(date_jour.getSeconds());
     
    let valeurDacmi= showDcmiNumber(paiement_total,9);
    
    let metamask= meta__key[0];
    console.log("Fetched metamask adress : ",metamask);
    // let item={
    //   emailpaypal: mailpay, 
    //   firstname: infoUser.firstname,
    //   lastname: infoUser.lastname,
    //   phone: infoUser.phone,
    //   adressMetamask: metamask,
    //   valeurDacmi: infoUser.valeurDacmi,
    //   idacheteur:infoUser.idacheteur,
    //   email:infoUser.email,
    //   valeurEur: props.valueToPass,  
    // };
    const formData = new FormData();

    formData.append('emailpaypal', mailpay)
    formData.append('firstname', infoUser.firstname)
    formData.append('lastname', infoUser.lastname)
    formData.append('phone', infoUser.phone)
    formData.append('adressMetamask', metamask)
    formData.append('valeurDacmi', valeurDacmi)
    formData.append('idacheteur', infoUser.idacheteur)
    formData.append('email', infoUser.email)
    formData.append('valeurEur', props.valueToPass)
    formData.append('idtransaction', idtransaction)
            
    // console.log("val",valeurDacmi);
    // console.log("Data to be sent :",item);
    // let res =await window.fetch(`/achatDacmi`,{
    //   method:'POST',
    //   body:JSON.stringify(item),
    //   headers:{
    //     "Content-Type":'application/json',
    //     "Accept":'application/json'
    //   }
    // });

      

    const input = document.querySelector('#logo_');
    const pdf = new jsPDF();
    
    let canvas= await html2canvas(input);
    const imgData = canvas.toDataURL('image/png');
    pdf.addImage(imgData, 'JPEG', -18, 10, 110, 20);

    pdf.autoTable({//client and bill info
        margin: { left: 120 },
        theme: 'grid',
        headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
        bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
        styles: { font: 'monospace' },
        columnStyles: {
            2:{ halign: 'right' }
        },
        body:[
          [["Transaction ID"],[":"],[idtransaction ]],
          [["Client ID"],[":"],[ infoUser.idclient]],
          [["Nom client"],[":"],[ infoUser.firstname + " " + infoUser.lastname]]
        ]
    });
        
    pdf.autoTable({//DACMI info
      startY: 30,
      margin: { right: 120, left: 20 },
      theme: 'grid',
      headStyles: { lineWidth: 0, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap' },
      bodyStyles: { lineWidth: 0, lineColor: [0,0,0], cellWidth: 'wrap', textColor: [0,0,0] },
      styles: { font: 'monospace' },
      columnStyles: {
          2:{ halign: 'right' }
      },
      body: [
          ["NEGOCE international UA"],
          ["a.gerno@negoce-int-ua.com"],
          ["+212 707 7109 27"]
      ],
      bodyStyles: {
          cellPadding: 0,
          lineWidth: 0,
          textColor: [0,0,0]
      }
    });
    // pdf.text(20, 20, "NEGOCE international UA");
    // pdf.text(20, 30, "a.gerno@negoce-int-ua.com");
    // pdf.text(20, 40, "+212 707 7109 27");

    pdf.autoTable({//the actual bill
      // startY: 40,
      theme: 'grid',
      headStyles: { lineWidth: 0.1, lineColor: [0,0,0], fillColor: [255,255,255], textColor: [0,0,0], cellWidth: 'wrap', halign: 'center' },
      bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 'wrap', halign: "right" },
      styles: { font: 'monospace' },
      head:[["Valeur DACMI (€)","DACMI demandé","Montant transféré (€)"]],
      body: [
        [DACMI,
        valeurDacmi,
        paiement_total]
      ]
    });

    pdf.autoTable({//bill status
      theme: 'grid',
      headStyles: {
          lineWidth: 0.1,
          lineColor: [0,0,0],
          fillColor: [255,255,255],
          textColor: [0,0,0],
          cellWidth: 40
      },
      bodyStyles: { lineWidth: 0.1, lineColor: [0,0,0], cellWidth: 40, textColor: [0,0,0] },
      columnStyles: {
          0:{ fontStyle: 'bold' }
      },
      styles: { font: 'monospace' },//doesn't work :(
      body: [
        [["Etat du paiement"],["Effectué"]],
        [["Mode de paiement"],["Paypal"]],
        [["Date de paiement"],[formatted_date]],
      ]
    });
          // pdf.save("download.pdf");
    var file =  pdf.output('blob');
    let blobe="forme";
    formData.append('file', file);

    const res = await axios.post(`/achatDacmi`, formData,{
    headers:{
      'Content-Type': 'multipart/form-data',
      "Accept": "application/ld+json",
      "type": "formData"
      }
    })
    const result = await res.data;
    console.warn("Data successfully sent, results :", result);
    setButtonState(2);
    }
  };
  ////////////////
 
  const paypalRef = React.useRef();
  const [mailpay , setMailpay]= React.useState("");
  const [idtransaction , setIdtransaction]= React.useState("");
  const [datepay , setDatepay]= React.useState("");
  const [paiement_total , setPaiement_total]= React.useState("");
  const [nomAdmin , setNomAdmin]= React.useState("");
  // console.log(mailpay);
  // To show PayPal buttons once the component loads
  React.useEffect(() => {
    // console.log(props);
    // console.log(props.valueToPass);
    // console.log(typeof(props.valueToPass));
    window.paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
              {
                description: "Your description",
                amount: {
                  currency_code: "EUR",
                  value: props.valueToPass,
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
           setPaid(1);
           console.log("Order details : ", order);
          //  console.log("aziza",order.purchase_units[0].amount.value);
          //  console.log("ppp",order.purchase_units[0].shipping.name.full_name);
          setDatepay(order.create_time);
          setIdtransaction(order.id);
          setMailpay(order.payer.email_address);
          setPaiement_total(order.purchase_units[0].amount.value);
          setNomAdmin(order.purchase_units[0].shipping.name.full_name);
          // console.log(data);
        },
        onError: (err) => {
        //   setError(err),
          console.error(err);
        },
      })
      .render(paypalRef.current);
  }, []);

  // If the payment has been made
  if (paid==1) {
    return (
      <div>
        <div className="mid-thing">
          <h3>
            Le paiement a été effectué avec succès !
          </h3>
          <p className="success-explain">
            Vous venez de payer { paiement_total }€ pour l'achat de DACMI. Vous pouvez maintenant envoyer votre requête pour que la quantité de DACMI équivalente soit transférée dans votre portefeuille Metamask.
          </p>
          {/* <table>
            <tbody>
              <tr>
                <td>Montant total</td>
                <td>{paiement_total} €</td>
              </tr>
            </tbody>
          </table> */}
        </div><br/>
        <button onClick={ajouinfo} className={ buttonState== 0? "buttonActive" : buttonState== 1? "buttonWait" : "buttonDone" } >
          { buttonState== 0 && <>Envoyer la requête</> }
          { buttonState== 1 && <Loading></Loading> }
          { buttonState== 2 && <>Requête envoyée</> }
        </button>
        { buttonState== 2 &&
        <>
          <p>
            L'historique de vos transaction est visible dans la section Wallet de votre
            <Link to={'/dashboard'} >
              Tableau de bord.
            </Link>
          </p>
        </>
        }
      </div>
        
    );
  }
        

  //  affich();
  // If any error occurs
    if (error) {
      return <div className="mid-thing">Une erreur est survenue lors du paiement. Veuillez réessayer.</div>;
    } else {
      // Default Render
      return (
        <div>
          <h4>Acheter l'équivalent en DACMI de { props.removeZeroFrom__(props.valueToPass) } €</h4>
          <div ref={paypalRef} />
        </div>
      );

    }
  

}

