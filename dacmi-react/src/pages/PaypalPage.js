import React from 'react';
import './PaypalPage.css';
import ReactPayPal from '../Components/ReactPayPal';
import Header from '../Components/Header/Header';
import { useRef, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function PaypalPage() {
  const [checkout, setCheckout] = React.useState(false);
  const value= useRef(null);

  const [transactionValue, setTransactionValue] = useState(0);

  const setValue= function(event){
    setTransactionValue(event.currentTarget.value);
    // // console.log(value.value);
    // // console.log(value.value);
    // console.log(transactionValue);
  }

  //trimmer -----------
  function removeZeroFrom__(someStringValue){
    let len= someStringValue.length;
    while(someStringValue[0]== '0' && someStringValue[1] != '.'){
      someStringValue= someStringValue.slice(1);
    }
    return(someStringValue);
  }
  //trimmer END -------


  return (
    <div className="Page">
    <Header title="dashboard" accountType="user" title="Acheter DACMI"></Header>
      <header className="Page-header">
        {(checkout === true) 
          ?
          <div className="payment-div">
            <ReactPayPal valueToPass= { transactionValue } removeZeroFrom__={ removeZeroFrom__ }/>
          </div>
          :
          <>
            <div>
              <h3>Acheter Dacmi via Paypal</h3>
              <div className="form-holder">
                <div className="for-the-inp">
                  <input type="number" ref={value} className="payInp" placeholder="Votre prix" onInput={ setValue } value={ transactionValue }/><span className="unit">€</span>
                </div>
                <button onClick={() => {setCheckout(true)}} className="checkout-button">Proceder au paiement</button>
              </div>
            </div>
            <div className="back-to">
              <p className="stuff">
                Une fois la transaction terminée, un de nos administrateurs va transférer l'équivalent de { removeZeroFrom__(transactionValue) }€ (moins les frais de transfert) dans votre portefeuille Metamask.
              </p>
              <Link to={ '/dashboard' }>Retourner au tableau de bord.</Link>
            </div>
          </>
        }
      </header>
    </div>
  );
}