import React from 'react';

export default function Maintenance() {
    return (
        <>
        <div className="maintenance">
            Le site est en cours de maintenance, veuillez revenir ultérieurement.
        </div>
        </>
    )
}
