import './fonts.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

import Login from './Components/Login/Login';
import Create from './Components/Account/Create';
import Complete from './Components/Account/Complete';
import Dashboard from './Components/Dashboard/Dashboard';
import Account from './Components/Dashboard/Account/Account';
import Guide from './Components/Dashboard/Guide/Guide';

import Assistance from './Components/Dashboard/Assistance';

import AdminDashboard from './Components/Admin/Dashboard';
import AdminAccount from './Components/Admin/Account/Account';
import AdminGuide from './Components/Admin/Guide/Guide';

import OnboardingButton, { getAccounts } from './Components/dmi';
import PaypalPage from './pages/PaypalPage';
import TransactionRequests from './Components/Admin/TransactionRequests';
import TransactionPending from './Components/Admin/TransactionPending';
import TransactionFinished from './Components/Admin/TransactionFinished';
import Bill from './Components/Bill/Bill';
import Messages from './Components/Dashboard/Messages';
import MessagesAdmin from './Components/Admin/Messages';
import ReactStripe from './Components/ReactStripe';
import axios from 'axios';


import Chat from './Components/Chat/Chat';

import CloseButton from './Components/CloseButton/CloseButton';

// import { getAccounts } from './Components/dmi';
// x= await getAccounts();


function App() {

  const [isUserLoggedIn, setUserLoggedIn]= useState(false);
  const [userInfo, setUserInfo]= useState(null);
  const [contentReady, setContentReady] = useState(false);
  const [useThis, setUseThis] = useState(false);
  

  
  const isLogged= useSelector(state => state.signInReducer);
  //const showToken= useSelector(state => state.logReducer);
  const listinfo= localStorage.getItem("userInfo");
   //console.log("++++",listinfo);
  const[tokennew,setTokennew]= useState(listinfo);
  // console.log("newT",tokennew);
  // let token=showToken.data.acsessToken;
  // console.log("tafita",token);
  // (async () => {
  //   console.log(await getAccounts())
  // })();
  // git checkout --theirs -- path/to/conflicted-file.txt
  // git checkout --ours -- path/to/conflicted-file.txt
  // useEffect(()=> {
  // },[]);

  const refreshToken = async () => {
        try{
            const resu= await axios.post(`/api/refreshtoken`,{token:tokennew.refreshToken});
            setTokennew({
                ...tokennew,
                accessToken: resu.data.accessToken,
                refreshToken: resu.data.refreshToken
            });
            
            return resu.data
        }catch(err){
            console.log(err)
        }
    }
   console.log("newaaaa",tokennew); 
    const axiosJWT = axios.create();
    //  console.log("ttt",axiosJWT)
   axiosJWT.interceptors.request.use(async (config)=>{
        let currentDate= new Date();
         console.log("ddddd",tokennew)
        const decodeToken = jwt_decode(tokennew.data.acsessToken);
        console.log("rrrr",decodeToken)
        if(decodeToken.exp *1000 < currentDate.getTime()){
            const data = await refreshToken();
            config.headers["authorization"] = "Bearer " +data.acsessToken
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    });


  useEffect(()=> {
    let info= localStorage.getItem("userInfo");
    let parsedInfo= JSON.parse(info);
    if(parsedInfo!== null){
      setUserInfo(parsedInfo.data);
      setUseThis(true);
    }
  },[]);
  
  useEffect(()=> {
    if(isLogged.isSignedIn){
      setUserLoggedIn(true);
      setUseThis(true);
    } else {
      setUserLoggedIn(false);
      setContentReady(true);
    }
  },[isLogged]);

  useEffect(()=> {
    if(userInfo!== null){
      setUserLoggedIn(true);
      setUseThis(true);
    } else {
      setUserLoggedIn(false);
      setContentReady(true);
    }
  }, [userInfo]);
  
  useEffect(() => {
    // console.log('content should be ready..');
    if(isUserLoggedIn){
      // console.log(isUserLoggedIn);
      // let info= localStorage.getItem("userInfo");
      // let parsedInfo= JSON.parse(info);
      // if(parsedInfo){
      setUseThis(true);
      // console.log(useThis);
      // console.log(contentReady);
      setContentReady(true);
      // console.log(useThis);
      // console.log(contentReady);
      // }
    }
  }, [isUserLoggedIn]);

  if(isUserLoggedIn){
    return (
      <>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
  
          <Route path='/' exact component={ Login }>
          </Route>
  
          <Route path='/login' exact component={ Login }>
          </Route>
  
          <Route path='/guide' exact component={ Guide }>
          </Route>
  
          <Route path='/dashboard' exact component={ Dashboard }>
          </Route>
  
          <Route path='/create-account' exact component={ Create }>
          </Route>
  
          <Route path='/complete-account'  exact component={ Complete }>
          </Route>
  
          <Route path='/account'  exact component={ Account }>
          </Route>
          <Route path='/admin/account'  exact component={ AdminAccount }>
          </Route>
  
          <Route path='/admin/dashboard'  exact component={ AdminDashboard }>
          </Route>
  
          <Route path='/admin/guide'  exact component={ AdminGuide }>
          </Route>
  
          <Route path='/transaction'  exact component={ OnboardingButton }>
          </Route>
  
          <Route path='/paypal'  exact component={ PaypalPage }>
          </Route>
          <Route path='/stripe'  exact component={ ReactStripe }>
          </Route>
  
          <Route path='/assist'  exact component={ Assistance }>
          </Route>
  
          <Route path='/admin/requests'  exact component={ TransactionRequests }>
          </Route>
  
          <Route path='/admin/pending'  exact component={ TransactionPending }>
          </Route>
  
          <Route path='/admin/finished'  exact component={ TransactionFinished }>
          </Route>
  
          <Route path='/bill'  exact component={ Bill }>
          </Route>
  
          <Route path='/messages' exact component={ Messages }>
          </Route>
  
          <Route path='/admin/messages' exact component={ MessagesAdmin }>
          </Route>
  
        </Switch>
      </Router>
      </>
    );
  } else {
    return (
      <>
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
  
          <Route path='/' exact component={ Login }>
          </Route>
  
          <Route path='/login' exact component={ Login }>
          </Route>
  
          <Route path='/create-account' exact component={ Create }>
          </Route>

          <Route path='/complete-account'  exact component={ Complete }>
          </Route>

          <Route path='/*'>
            {/* {(contentReady && !isUserLoggedIn ) && 
              <div className="not-connected">
                  Vous devez être connecté(e) avant d'aller sur cette page.
                  <Link to="/login" className="centerL">Page de connexion</Link>
              </div>
            } */}
            {
              contentReady ? ( !useThis && <>
                <div className="not-connected">
                    Vous devez être connecté(e) avant d'aller sur cette page.
                    <Link to="/login" className="centerL">Page de connexion</Link>
                </div>
                </>)
                : <></>
            }
          </Route>
  
          {/* <Route path='/guide' exact component={ Guide }>
          </Route>
  
          <Route path='/dashboard' exact component={ Dashboard }>
          </Route>
  
          <Route path='/create-account' exact component={ Create }>
          </Route>
  
          <Route path='/complete-account'  exact component={ Complete }>
          </Route>
  
          <Route path='/account'  exact component={ Account }>
          </Route>
          <Route path='/admin/account'  exact component={ AdminAccount }>
          </Route>
  
          <Route path='/admin/dashboard'  exact component={ AdminDashboard }>
          </Route>
  
          <Route path='/admin/guide'  exact component={ AdminGuide }>
          </Route>
  
          <Route path='/transaction'  exact component={ OnboardingButton }>
          </Route>
  
          <Route path='/paypal'  exact component={ PaypalPage }>
          </Route>
  
          <Route path='/assist'  exact component={ Assistance }>
          </Route>
  
          <Route path='/admin/requests'  exact component={ TransactionRequests }>
          </Route>
  
          <Route path='/admin/pending'  exact component={ TransactionPending }>
          </Route>
  
          <Route path='/admin/finished'  exact component={ TransactionFinished }>
          </Route>
  
          <Route path='/bill'  exact component={ Bill }>
          </Route>
  
          <Route path='/messages' exact component={ Messages }>
          </Route>
  
          <Route path='/admin/messages' exact component={ MessagesAdmin }>
          </Route> */}
  
        </Switch>
      </Router>
      </>
    );
  }
}
export default App;
