import fs from 'fs';

export default function eraseFile(filePath_){
    console.log('attempting to delete ' + filePath_);
    fs.unlinkSync(filePath_);
};