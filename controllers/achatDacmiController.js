import express from 'express';
import achatDacmiModel from '../model/achatDacmiModel.js';
import User from '../model/user.js';
import crypto from 'crypto';
import  nodemailer from 'nodemailer';
import eraseFile from './files.js';
import fs from 'fs';

export const addAchatDacmi = async (req, res) => {

    let{firstname, lastname,emailpaypal, adressMetamask, phone, valeurEur, valeurDacmi,idacheteur, email} = req.body;
     let filename='';

    if (req.files) {
        const file = req.files.file;
     
        if (file.mimetype === "application/pdf") {
            
            let nom=req.body.idtransaction;
            //let cryptename = crypto.createHash('md5').update(nom).digest("hex");
             filename=nom+'.pdf';
            // console.log('The file name : ');
            // console.log(filename);
            // console.log("vvv",filename)
            file.mv(`./uploads/image/${filename}`, (error) => {
                // console.log(error);
    
                if (error) {
                    res.send(error);
                } 
            });
        } else {
            return res.json({status:"error", error:"Le format de la pièce-jointe est incorrecte !!!"})
        }
    } else {
        return res.json({status:"error", error:"Vous devez envoyer une pièce-jointe !!!"})
    }
    const transporter = nodemailer.createTransport({
        port: 465,               // true for 465, false for other ports
        host: "smtp.gmail.com",
        auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASSWORD,
            },
        secure: true,
    });
    // const { to, subject, text } = req.body;
    const mailData ={
        from: 'andrianjafynantenaina2@gmail.com',
        to: email,
        subject: 'Achat dacmi',
        text: 'Bonjour',
        html: '<b>Bonjour! </b><br> Merci d\'avoir choisi DACMI crypto-monnaie, l\'achat dacmi que vous avez effectué a bien été reçu. <br/>Veiller patientez pendant le transfert de la quantité de dacmi demandé à vos comptes Metamask <br/>',
        attachments: [
            {
                filename: filename,
                path: `./uploads/image/${filename}`
            }
        ]
    };
    transporter.sendMail(mailData, (error,info) =>{
        if(error){
            console.log(error);
            return;
        }
        console.log("reachable code");
        // fs.closeSync(file);
        console.log("unreachable code-1");
        fs.unlink("./uploads/image/" + filename, (err)=> {
            if(err){
                console.log(err);
            } else {
                console.log("file deleted");
            }
        });
        console.log("unreachable code-2");
        res.status(200).send({message:"Mail send", message_id: info.messageId});
        
        // eraseFile("./uploads/image/" + filename);
    });
    // .then(response=>{
    //     console.log(response);
        //to do when mail is sent
        //...
        //...
        //'./uploads/image/' + filename
    // });
    
    // console.log("valeur",req.body.idacheteur);
    let nomfacture= filename;
    

    let date_jour=new Date();
    function addZeros(n) {
      if (n <= 9) {
        return "0" + n;
      }
      return n
    }
    let formatted_date =  addZeros(date_jour.getDate()) + "-" +  addZeros(date_jour.getMonth() + 1) + "-" + date_jour.getFullYear() + " " +  addZeros(date_jour.getHours()) + ":" +  addZeros(date_jour.getMinutes()) + ":" +  addZeros(date_jour.getSeconds());
    let init=false;
    let userconect= await User.find({ _id: idacheteur })
    //console.log("valiny",userconect);
    if(userconect==""){
        // console.log('Vous êtes un utilisateur anonyme');
         return res.json({status: 400, mesageerror: 'Vous êtes un utilisateur anonyme'});
    }else{
        try {
            const resultats = await achatDacmiModel.create({
                firstname,
                lastname, 
                email,
                emailpaypal,
                adressMetamask,
                phone,
                valeurEur,
                valeurDacmi,
                statu:init,
                idacheteur,
                nomfacture,
                date: formatted_date
            
                
            });
            // console.log('achat Dacmi successfully');
            return res.json({data:{ status: 'ok',
            _id: resultats.id,
            firstname:resultats.firstname,
            email:resultats.email,
            emailpaypal:resultats.emailpaypal,
            adressMetamask:resultats.adressMetamask,
            phone:resultats.phone,
            valeurDacmi:resultats.valeurDacmi,
            valeurEur:resultats.valeurEur,
            statu:resultats.statu,
            date:resultats.date,
            nomfacture:resultats.nomfacture,
            idacheteur:resultats.idacheteur
            }});

        } catch (error) {
            throw error;
        }    
    }
}
export const getAchatDacmi = async (_, res) => {
    const AchatDacmi = await achatDacmiModel.find({})
    res.send(AchatDacmi)
}

export const getADacmi_id = async (req, res) => {
    const achatDacmi= await achatDacmiModel.find({ idacheteur: req.params.id })
    res.send(achatDacmi)
}

export const updateDacmi = async (req, res) => {
    const achatDacmi= await achatDacmiModel.findByIdAndUpdate( req.params.id, req.body)
    await achatDacmi.save()
    res.send(achatDacmi)
}

////get tous users
export const getAllUser = async (_, res) => {
    const Userall = await User.find({})
    res.send(Userall)
}
export const updateUsers= async (req, res) => {
    const modifUser= await User.findByIdAndUpdate( req.params.id, req.body)
    await modifUser.save()
    res.send(modifUser)
}
export const getUser_id = async (req, res) => {
    const userid= await User.find({ _id: req.params.id })
    res.send(userid)
}
// export const getTest = async (_, res) => {
//     const AchatDacmi = await achatDacmiModel.find({})
//     res.send(AchatDacmi)
// }