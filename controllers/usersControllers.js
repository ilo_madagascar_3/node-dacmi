import User from '../model/user.js'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import path from 'path'
import { response } from 'express';
import fetch from 'node-fetch';

const JWT_SECRET ='thesecretilodacmijwtsecret';

const catchErrors = asyncFunction => (...args) => asyncFunction(...args).catch(console.error);
const getCountry = catchErrors(async (country) => {
    const res = await fetch(`https://restcountries.eu/rest/v2/name/${country}?fullText=true`);
    const json = await res.json();

    console.log(json);
    return json;
});

/**
 * Returns wether or not there is a user related to an email
 * @returns response
 */
export const  verifyUserExistence = async (req, res) => {
    const { email } = req.body;

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailVerification = re.test(String(email).toLowerCase());
    
    //Data validation
    if (emailVerification !== true) {
        return res.json({status: 'error', error: 'Vous devez rentrer un format d\'e-mail valide'});
    }

    if (!email) {
        return res.json({status: "error", error:"Aucun e-mail n'a été envoyé !!!"});
    }

    const userRelated = await User.findOne({email});
        
    if (userRelated) {
        return res.json({status:'error', message:"Cet e-mail est déjà relié à un utilisateur !!!"});
    }

    return res.json({status:'ok', error:"Pas d'utilisateur relié au mail"});

}

/**
 * 
 * Register Function
 * @param {*} req 
 * @param {*} res 
 * @returns 
 * 
 */


export const registerFunction = async (req, res) => {
    
    let
    {
        firstname, 
        lastname, 
        email, 
        password:plainTextPassword, 
        passwordre:confirmpassword, 
        phone, 
        remember, 
        news,
        day, 
        month, 
        year, 
        nationalite, 
        address, 
        city, 
        postalCode, 
        country,
        role,
        adressMetamask
    } = req.body;
    
    console.log(req.body)
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailVerification = re.test(String(email).toLowerCase());
    
    //console.log(renemberme, subscribenews);
    
    //Data validation
    if (emailVerification !== true) {
        return res.json({status: 'error', error: 'Vous devez rentrer un format d\'e-mail valide'});
    }

    if (!email || typeof email !== 'string') {
        return res.json({status: 'error', error: 'Invalid username'});
    }

    const userRelated = await User.findOne({email});
        
    if (userRelated) {
        return res.json({status:'error', error:"Cet e-mail est déjà relié à un utilisateur !!!"});
    }
    
    if (!plainTextPassword || typeof plainTextPassword !== 'string') {
        return res.json({status: 'error', error: 'Invalid password'});
    }

    if (plainTextPassword.length < 5) {
        return res.json({status: 'error', error: 'The password is too short : the password should be at least 6 characters'});
    }

    if (!confirmpassword || typeof confirmpassword !== 'string') {
        return res.json({status: 'error', error: 'Veuillez confirmer votre mot de passe !!!'});
    }

    if (confirmpassword  !== plainTextPassword) {
        return res.json({status: 'error', error: 'Le mot de passe et la confirmation du mot de passe doivent être les mêmes !!!'});
    }

    if(remember == 'true'){
        remember = true;
    }else {
        remember = false;
    }

    if(news == 'true'){
        news = true;
    }else {
        news = false;
    }

    //let {_id, email, day, month, year, nationalite, address, city, postalCode, country} = req.body; //console.log("id", _id);

    /* if (!_id || typeof _id !== 'string') {
        return res.json({status:'error', error: "L'id de l'utilisateur n'a pas été envoyé. "})
    } */
    //tests sur les données envoyées !!!
    if (!day) {
        return res.json({status: 'error', error: 'Vous devez spécifier votre jour de naissance'});
    }

    if (!month ) {
        return res.json({status: 'error', error: 'Vous devez spécifier votre mois de naissance'});
    }

    if (!year) {
        return res.json({status: 'error', error: 'Vous devez spécifier votre année de naissance'});
    }

    if (!nationalite || typeof nationalite !== 'string') {
        return res.json({status: 'error', error: 'Vous devez impérativement indiquer votre nationalité !!!'});
    }

    if (!address || typeof address !== 'string') {
        return res.json({status: 'error', error: 'Vous devez impérativement indiquer votre addresse !!!'});
    }

    if (!city || typeof city !== 'string') {
        return res.json({status: 'error', error: 'Vous devez impérativement indiquer votre ville de résidence !!!'});
    }

    if (!postalCode || typeof postalCode !== 'string') {
        return res.json({status: 'error', error: 'Vous devez impérativement indiquer votre code postal !!!'});
    }

    //country operations
    if (!country || typeof country !== 'string') {
        return res.json({status: 'error', error: 'Vous devez impérativement indiquer votre pays de résidence, c\'est très important !!!'});
    }
    
    if(country === 'Algeria'  || country === 'algeria'|| country === 'Morocco' || country === 'morocco')
    {
        return res.json({status: 'error', error: "This country is blacklisted"});
    }

    // let country_informations = await getCountry(country);
    
    // if (country_informations[0].subregion == 'South America' || country_informations[0] == 'Central America') {
    //     return res.json({status: 'error', error: "This country is blacklisted"});
    // }

    //Upload
    let filename='';
    
    if (req.files) {

        const file = req.files.file;
        console.log(file);
        
        console.log("avadik",file.mimetype);
        if (file.mimetype === "image/jpeg" || file.mimetype === "application/pdf") {
            
            filename = file.name;
            console.log('The file name : ');
            console.log(filename);
    
            file.mv(`./uploads/${filename}`, (error) => {
                console.log(error);
    
                if (error) {
                    res.send(error);
                } 
            });
        } else {
            return res.json({status:"error", error:"Le format de la pièce-jointe est incorrecte !!!"})
        }

    } else {
        return res.json({status:"error", error:"Vous devez envoyer une pièce-jointe !!!"})
    }

    //Password hash
    const password = await bcrypt.hashSync(plainTextPassword, 10);
    let user="client";
    let metamask="aucune";
    try {
        const response = await User.create({
            email,
            firstname,
            lastname, 
            password,
            phone,
            remember,
            news,
            birthday: new Date(year, month, day),
            nationality: nationalite,
            address,
            city,
            postalCode,
            country,
            KYC: filename,
            role: user,
            adressMetamask: metamask

        });

        console.log(response);

        console.log('User created successfully');
        return res.json({data:{ status: 'ok', _id: response.id,firstname:response.firstname, email:response.email,role:response.role }});

    } catch (error) {
        if (error.code === 11000) {
            
            console.log(JSON.stringify( error));
            return res.json({status:'error', data:{message:'E-mail déjà utilisé !!!'}});
        }
        throw error;
    }
}
/**
 * RefreshTokens
 */

let refreshTokens = [];

export const refresh = async (req, res) => {
         const refreshToken =req.body.token
         console.log("oooo",refreshToken);
         if(!refreshToken) return res.status(405).send("Vous êtes déconnecté");
         if(!refreshTokens.includes(refreshToken)){
             return res.status(405).send("Vous refresh Token est non vallidé");
         }  
         jwt.verify(refreshToken,"myRefreshSecretKey", (err, Usercon) => {
            err && console.log(err);
            refreshTokens = refreshTokens.filter((token) => token !== refreshToken);

            const newAcsessToken = generatAcsessToken(Usercon)
            const newRefreshToken = generatRefreshToken(Usercon)
            refreshTokens.push(newRefreshToken);

            res.json({
                stautus:200,
                acsessToken: newAcsessToken,
                refreshToken: newRefreshToken
            })
         })
     }

    const generatAcsessToken = (Usercon) =>{
        return jwt.sign({ id: Usercon.id, email: Usercon.email },
                    "mySecretKey", 
                    {expiresIn:"100s"}
        )
    }
    const generatRefreshToken = (Usercon) =>{
        return jwt.sign({ id: Usercon.id, email: Usercon.email },
                    "myRefreshSecretKey"
        )
    }
/**
 *
 * Login function 
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const loginFunction = async (req, res) => {
    const {email, password} = req.body;
    //Modification user par Usercon......
    const Usercon = await User.findOne({email}).lean();

    if (!Usercon) {
        return res.json({status:'error', data: {message:'Invalid email/password'}});    
    }

    if (!Usercon.birthday || !Usercon.nationality || !Usercon.address || !Usercon.city || !Usercon.postalCode || !Usercon.country) {
        return res.json({status:'error', error: 'Vous n\'avez pas encore complété votre profil', user_id: Usercon._id, email: Usercon.email });
    }

    if (await bcrypt.compare(password,Usercon.password)) {
        //the email password combination is successful
        // const token = jwt.sign({
        //     id:Usercon._id, 
        //     email:Usercon.email
        // }, JWT_SECRET);
        const acsessToken = generatAcsessToken(Usercon)
        const refreshToken = generatRefreshToken(Usercon)
        refreshTokens.push(refreshToken);

        const _id =Usercon._id;
        const firstname =Usercon.firstname;
        const lastname =Usercon.lastname;
        const role =Usercon.role;
        const phone =Usercon.phone;
        const email=Usercon.email;

        console.log(_id);

        return res.json({status:'ok', data:{_id,role, email, acsessToken,refreshToken, firstname, lastname, phone,email}});
    }

    res.json({status: 'error', data:{message:'Invalid credentials'}});
}

/**
 * Verification si token valide
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const verify = (req, res, next) => {
    const autHeader = req.headers.authorization;
    if(autHeader){
        const token = autHeader.split(" ")[1];
        jwt.verify(token, "mySecretKey", (err, Usercon) =>{
            if(err){
              return res.json({status:403 , message:"Token non validé"})  
            }
            req.Usercon = Usercon;
            next();
        })
    }
    else{
      return res.json({status:401 , message:"Vous êtes pas authentifié"})
  
    }
}
/**
 * Change password function
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const changePasswordFunction = async (req, res) => 
{
    const { token, newpassword } = req.body;

    console.log(newpassword);
    
    if (!newpassword || typeof newpassword !== 'string') {
        return res.json({status: 'error', error: 'Invalid password'});
    }

    if (newpassword.length < 5) {
        return res.json({status: 'error', error: 'The password is too short : the password should be at least 6 characters'});
    }

    try {
        const user = jwt.verify( token, JWT_SECRET );

        console.log('user : ', user);
        console.log(newpassword);
        const _id = user.id;
        console.log(_id);

        const hashedPassword = await bcrypt.hash(newpassword,10);
        
        console.log(hashedPassword);

        await User.updateOne({_id}, {
            $set: {
                password:hashedPassword
            }
        });

        res.json({status:'ok'});

    } catch (error) {
        console.log(error);

        return res.json({status: 'error', error: 'Requête non autorisée !!!'});        
    } 
}

export const secondStepRegistration = async (req, res) => {  
    //Country operations
    const { country } = req.body;
    let country_operations = await getCountry(country);
    // console.log('country_operations : ');
    // console.log(country_operations);

    console.log('country_operations[0] : ');
    console.log(country_operations[0]);

    if (country_operations[0].subregion == 'South America' || country_operations[0].subregion == 'Central America') {
        return res.json({status: 'error', error: "This country is blacklisted"});
    }

    res.json({ status: 'ok', message:"Data got by the controller !!", data: country_operations[0] });
}

export const getUser = async (req, res) => {
    const user = await User.find({ _id: req.params.id });
    res.send(user[0]);
}
 export const deconnexion= (req,res) => {
        const refreshToken = req.body.token;
        refreshTokens = refreshTokens.filter((token)=>token !== refreshToken);
        res.json({stautus:200, message:"Vous êtes deconnecté"})
}