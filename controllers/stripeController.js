 import loadStripe  from 'stripe';//(process.env.NODE_APP_STRIPE_SECRETKEY);
//const stripe = require("stripe")(process.env.NODE_APP_STRIPE_SECRETKEY);
import { v4 as uuid } from 'uuid';


export const getStripeid = async (req, res) => {
  res.send(`Test Stripe Secret Key - ${process.env.NODE_APP_STRIPE_SECRETKEY}`);
};

export const postStripe = async (req, res) => {
  //console.log("valiny",req.body);

  const stripe =await loadStripe(process.env.NODE_APP_STRIPE_SECRETKEY);
  const { product, token } = req.body;
  console.log("PRODUCT ", product);
  console.log("PRICE ", product.price);
  const idempotencyKey = uuid(); // this key is used so that you do not double charge in case of error

  let request = stripe.customers
    .create({
      email: token.email,
      source: token.id,
    })
    .then((customer) => {
      stripe.charges.create(
        {
          amount: product.price * 100,
          currency: "EUR",
          customer: customer.id,
          receipt_email: token.email, //in case you want mail
          description: `purchase of ${product.name}`,
          shipping: {
            name: token.card.name,
            address: {
              country: token.card.address_country,
            },
          },
        },
        { idempotencyKey }
      )
      .then((result) => {
        res.status(200).json(result);
        console.log("----", result);
      })
    })
    .then((result) => {
      res.status(200).json(result);
      console.log("----", result);
    })
    .catch((err) => console.log(err));
    
  return request;
};
export const postinfo= async (req,res) => {
    
}
