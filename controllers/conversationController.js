import express from 'express'
import ConversationModel from '../model/conversationModel.js'

export const addConversation = async (req, res) => {
    const newconversation= new ConversationModel({
        membres:[req.body.senderId, req.body.receiverId],
    });
    try{
        const savedConversation = await newconversation.save();
        res.status(200).json(savedConversation)
    }catch(err){
        res.status(500).json(err)
    }
}

export const getConversation = async (req, res) => {

    try{
        const conversation = await ConversationModel.find({
            membres:{$in: [req.params.userId]}
        });
        res.status(200).json(conversation)
    }catch(err){
        res.status(500).json(err)
    }
}