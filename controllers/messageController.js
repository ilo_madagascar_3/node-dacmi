import express from 'express'
import MessageModel from '../model/messageModel.js'

export const addMessage = async (req, res) => {
    let{conversationId,sender,text} = req.body;

    try{
        const saveresultats = await MessageModel.create({
            conversationId,
            sender, 
            text,
            lu:false 
        });
        res.status(200).json({data:{ status: 'ok',
            _id: saveresultats.id,
            conversationId: saveresultats.conversationId,
            sender: saveresultats.sender,
            text: saveresultats.text,
            lu: saveresultats.lu
        }})
    }catch(err){
        res.status(500).json(err)
    }
}
export const updatestatuMP = async (req, res) => {
    const valeur=[{conversationId:req.body.conversationId},{sender: req.body.sender}]

    try {
    const thing = await MessageModel.updateMany({
        $and:valeur
    }, {
        "$set": {
            lu: true
        }
    }, {
        "multi": true
    });
    res.json(thing)

} catch (err) {
    console.log(err.message)
    res.status(500).json({
        message: err.message
    })
}

}

export const updatestatuMultiple = async (req, res) => {
    const valeur={conversationId:req.body.conversationId}
    const sender=req.body.sender
    
     for(var i=0; i<=sender.length;i++){
         var senderune={sender:sender[i]};
     
         var table=[valeur,senderune];
         console.log("yyy",table)
         try {
        const thing = await MessageModel.updateMany({
            $and:table
        }, {
            "$set": {
                lu: false
            }
        }, {
            "multi": true
        });
        res.json(thing)

        } catch (err) {
            console.log(err.message)
            res.status(500).json({
            message: err.message
            })
        }
       return;
     }
   

}

export const getMessage = async (req, res) => {
    try{
        const messages = await MessageModel.find({ conversationId: req.params.conversationId });
        res.status(200).json(messages)
    }catch(err){
        res.status(500).json(err)
    }
}
export const getLimitMp = async (req, res) => {
    try{
        const messages = await MessageModel.find({ conversationId: req.params.conversationId }).sort({'_id':-1}).limit(1);
        res.status(200).json(messages)
    }catch(err){
        res.status(500).json(err)
    }
}