import express from "express";
// const express= require('express');
import path from 'path';
import http from 'http';

// import { Socket } from "socket.io";
import * as _io from "socket.io";


const __dirname= path.resolve();
// console.log(__dirname);
// console.log(path.join(__dirname, 'public'));

const app= express();
const server= http.createServer(app);

const io= new _io.Server(server);

app.use('/', express.static(path.join(__dirname, 'public')));

io.on('connection', socket => { console.log('new connection..'); });

const PORT= 8000 || process.env.PORT;

server.listen(
    PORT,
    () => console.log("--__listening on port " + PORT + "__--")
);