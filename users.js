const users= [];

//user joins
function userJoin(id, username, room, s_id){
    const user= {
        id,//user dacmi id
        username,
        room,
        s_id: s_id//socket id
    };
    // if(!users.find(u => u.id=== user.id)){
        users.push(user);
    // }
    // console.log(users);
    return user;
}

//find an user
function findUser(id){
    console.log(users);
    return users.find(user => user.id === id);
}

//user leaves
function userLeaves(id){
    let index= users.findIndex(user => user.s_id=== id);
    if(index !== -1){
        return users.splice(index, 1)[0];
    }
}

//get the users in a room
function getRoomUsers(room){
    return(users.filter(user => user.room=== room));
}

export {userJoin, findUser, getRoomUsers, userLeaves};