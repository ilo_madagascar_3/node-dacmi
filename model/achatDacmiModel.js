import  Mongoose  from "mongoose"

const AchatDacmiSchema = new Mongoose.Schema({
    
    firstname: {type:String,required: true},
    lastname: {type:String,required: true},
    email: {type:String},
    emailpaypal: {type:String},
    adressMetamask: {type:String},
    phone: {type:String},
    valeurEur: {type:String},
    valeurDacmi: {type:String},
    statu: {type:Boolean},
    idacheteur: {type:String},
    nomfacture: {type:String},
    date: {type: String}
     
})
const AchatDacmiModel = Mongoose.model("achatDacmi", AchatDacmiSchema)
export default AchatDacmiModel