//const mongoose = require('mongoose');
import { json } from 'express';
import mongoose from 'mongoose'
const UserSchema = new mongoose.Schema(
{
    email: {type:String, required:true, unique:true},
    firstname: {type:String, required:true},
    lastname: {type:String, required:true},
    password: {type:String, required:true},
    phone: {type:String, required:true},
    remember: {type:Boolean, required:true},
    news: {type:Boolean, required:true},
    birthday: { type: Date },
    nationality: {type:String},
    address: {type:String},
    city: {type:String},
    postalCode: {type:String},
    country: {type:String},
    role: {type:String},
    adressMetamask: {type:String},
    KYC: {type:String}
}, 
{ 
    collection: 'users'
});

const model = mongoose.model('UserSchema', UserSchema);

//module.exports = model;

export default model;