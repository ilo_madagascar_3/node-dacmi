import  Mongoose  from "mongoose"

const MessageSchema = new Mongoose.Schema(
{
    conversationId:{
        type: String
    },
    sender:{
        type: String
    },
    text:{
        type: String
    },
    lu:{
        type: Boolean
    },

}, 
    {timestamps:true}
   
);
const MessageModel = Mongoose.model("message", MessageSchema)
export default MessageModel